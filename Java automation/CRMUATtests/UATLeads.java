package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Test;
	//import org.junit.After;
	import static org.junit.Assert.*;
	import static org.hamcrest.CoreMatchers.is;
	import static org.hamcrest.core.IsNot.not;
	import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class UATLeads {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM UAT Leads";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log does not Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			//WebDriver driver = new ChromeDriver();		
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		  
		    
		    //click-on Login-button
			System.out.println("click Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> wh = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandle = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    vars.put(currentWindowHandle, waitForWindow(2000));
		 
		   
		    Calendar c = Calendar.getInstance();
		    int nmonth = c.get(Calendar.MONTH);
		    int nday = c.get(Calendar.DAY_OF_MONTH);
		    int nhour = c.get(Calendar.HOUR_OF_DAY);
		    int nmin = c.get(Calendar.MINUTE);
		    nmonth=nmonth+1;
		    
		    //click Add Customer
		    System.out.println("click Add Customer");
		    
		    //click-on Customers TAB
			
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
		    try {
			   Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("#mycontroller > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(7) > a")).click();
		    driver.switchTo().defaultContent();
		    
		    //Enter Customer information
		    System.out.println("Enter Customer information");
		    
		    	   
		    driver.findElement(By.id("txtFirstName")).click();
		    driver.findElement(By.id("txtFirstName")).sendKeys("Test");
		    driver.findElement(By.id("txtLastName")).click();
		    driver.findElement(By.id("txtLastName")).sendKeys("Leads"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("txtEmailAddress")).click();
		    driver.findElement(By.id("txtEmailAddress")).sendKeys("leads"+nmonth+nday+nhour+nmin+"@yopmail.com");
		    driver.findElement(By.xpath("/html/body/div[11]/div/div[3]/button[1]")).click();
		    
		    try {
				   Thread.sleep(8000);
				    } catch (InterruptedException e) {
				      e.printStackTrace();
				    }
			    
		    
		    
		    driver.switchTo().defaultContent();
		    
		    
		    //create lead
		    System.out.println("click Create Lead");
		    
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    
		    //click recent user
		    System.out.println("click Recent User");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/div/div/a[1]")).click();
		    
            //click new customer add lead
		    System.out.println("Add Lead Type");
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.linkText("Select an action to perform")).click();
		    driver.findElement(By.linkText("Add Contact to Opportunity")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("selAddToCampaign")).click();
		    driver.findElement(By.xpath("//*[@id=\"selAddToCampaign\"]")).click();
		    driver.findElement(By.id("selAddToCampaign")).click();
		    driver.findElement(By.cssSelector("#selAddToCampaign > option:nth-child(2)")).click();
		    driver.findElement(By.cssSelector("#modalAddOpportunity .btn-primary")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		   
		    
		    //click Lead Summary
		    System.out.println("Verify Lead Created");
		    
		    //driver.findElement(By.xpath("//button[contains(.,\'  Update\')]")).click();
		    
		    //error in opening lead, resolves itself in five seconds
		    Thread.sleep(15000);
			
		    driver.findElement(By.cssSelector(".bc-current")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    System.out.println("Modify Lead Info");
		    driver.switchTo().defaultContent();
		    
		    driver.findElement(By.id("radMCSU_Completed")).click();
		    driver.findElement(By.id("chkMCSU_AddCalendarEntry")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("txtMCSU_AddCalendarSubject")).sendKeys("leadcalendarentry");
		    driver.findElement(By.id("chkMCSU_AddQuickCall")).click();
		    driver.findElement(By.id("selMCSU_AddQuickCall")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("selMCSU_AddQuickCall"));
		      dropdown.findElement(By.xpath("//option[. = 'Called and left message']")).click();
		    }
		    
		    driver.findElement(By.id("chkMCSU_AddSomeDetails")).click();
		    driver.findElement(By.id("txtMCSU_AddSomeDetails")).sendKeys("leaddetails");
		    
		    
		    driver.findElement(By.cssSelector(".btn-group:nth-child(3) > .btn-success")).click();
		    driver.findElement(By.cssSelector("div.btn-group:nth-child(3) > button:nth-child(1)")).click();
		    driver.findElement(By.cssSelector("div.btn-group:nth-child(3) > button:nth-child(1)")).click();
		    driver.close();
		    
		  //Adding-in Headless Chrome Options
			//ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    
		    //click-on Login-button
			System.out.println("click Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //Lead Management
		    System.out.println("click Leads Tab / Lead Management");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector("#dashSubMenu_1 > a:nth-child(2)")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.switchTo().frame(0);
		    
		    driver.findElement(By.id("searchSourceLO")).click();
		    driver.findElement(By.id("searchBtn")).click();
		    driver.findElement(By.id("StartDt")).click();
		    driver.findElement(By.id("EndDt")).click();
		    driver.findElement(By.id("leadTypes")).click();
		    driver.findElement(By.id("contactTypes")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    {
		      WebElement dropdown = driver.findElement(By.id("contactTypes"));
		      dropdown.findElement(By.xpath("//option[. = 'All Contacts']")).click();
		    }
		    
		    driver.findElement(By.id("bdoPipeline")).click();
		    driver.findElement(By.cssSelector(".le-search-checkbox:nth-child(1) > .le-search-checkbox-label")).click();
		    driver.findElement(By.id("completeHierarchy")).click();
		    driver.findElement(By.id("searchBtn")).click();
		    driver.switchTo().defaultContent();
		    
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    
		    driver.findElement(By.id("completeHierarchy")).click();
		    driver.findElement(By.id("searchBtn")).click();
		    
		    //driver.findElement(By.xpath("//li[contains(.,'JourneyBank Home Division 2')]")).click();
		    //driver.findElement(By.cssSelector(".dataGridRowOdd > div:nth-child(1) > a:nth-child(1)")).click();

		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //driver.findElement(By.xpath("//li[contains(.,'Northeast')]")).click();
		    //driver.findElement(By.cssSelector("div.le-listview-row:nth-child(1) > div:nth-child(1) > a:nth-child(1)")).click();
		    
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //driver.findElement(By.xpath("//li[contains(.,'Northeast_CRM Boston')]")).click();
		    //driver.findElement(By.cssSelector("div.le-listview-row:nth-child(1) > div:nth-child(1) > a:nth-child(1)")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		  
		    //driver.findElement(By.xpath("//li[contains(.,'Beck(CM)')]")).click();
		    //driver.findElement(By.cssSelector("div.le-listview-row:nth-child(8) > div:nth-child(1) > a:nth-child(1)")).click();
		    
		  
		    System.out.println("Lead list");
		    
		    //try {
			  //    Thread.sleep(5000);
			    //} catch (InterruptedException e) {
			      //e.printStackTrace();
			    //}
		    
		    //driver.findElement(By.cssSelector("#lv-items-2 > div:nth-child(1) > div:nth-child(1) > input:nth-child(1)")).click();
		    
		    //try {
			  //    Thread.sleep(5000);
			    //} catch (InterruptedException e) {
			      //e.printStackTrace();
			    //}
		    
		    
		    //driver.findElement(By.id("btnReassign")).click();
		    
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    //driver.findElement(By.cssSelector("#lv-items-3 > div:nth-child(1) > div:nth-child(7) > button:nth-child(1)")).click(); 
		    //try {
			  //    Thread.sleep(5000);
			    //} catch (InterruptedException e) {
			      //e.printStackTrace();
			    //}

		    //driver.findElement(By.cssSelector("#crumbs > ul:nth-child(1) > li:nth-child(3) > a:nth-child(1) > div:nth-child(1)")).click();
		    
		    //try {
			  //    Thread.sleep(5000);
			    //} catch (InterruptedException e) {
			      //e.printStackTrace();
			    //}
		    
		    //driver.findElement(By.id("btnComplete")).click();
		    //System.out.println("click-on Reassignment Complete");
		    driver.close();
		    
		  //Lead Management
		    System.out.println("click Leads Tab / Lead Summary");
		    
		    options.addArguments("window-size=1280,800");
		    driver = new ChromeDriver(options);
			
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    
		    //click-on Login-button
			System.out.println("click Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/button")).click();
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.linkText("Lead Summary")).click();
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);

		    
		    //click Lead Summary
		    System.out.println("click Lead Summary");
		    
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Lead Summary"));
		    driver.findElement(By.id("StartDt")).click();
		    driver.findElement(By.id("StartDt")).click();
		    driver.findElement(By.id("EndDt")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.cssSelector("#FilterTypeCd1 > option:nth-child(2)")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'New']")).click();
		    }
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.cssSelector("#FilterTypeCd1 > option:nth-child(3)")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'Open']")).click();
		    }
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'Late']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd1 > option:nth-child(5)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'Closed']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd1 > option:nth-child(6)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'No Disposition']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd1 > option:nth-child(8)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'Open - Hot Leads']")).click();
		    }
		    driver.findElement(By.cssSelector("option:nth-child(9)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'Open - Warm Leads']")).click();
		    }
		    driver.findElement(By.cssSelector("option:nth-child(10)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'Open - Cold Leads']")).click();
		    }
		    driver.findElement(By.cssSelector("option:nth-child(11)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd1")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd1"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.id("LastName")).click();
		    driver.findElement(By.id("LastName")).sendKeys("Leads");
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("LastName")).sendKeys(" ");
		    driver.findElement(By.cssSelector("#FilterTypeCd1 > option:nth-child(2)")).click();
		    driver.findElement(By.id("CampaignId")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("CampaignId"));
		      dropdown.findElement(By.xpath("//option[. = 'Purch-SelfGen']")).click();
		    }
		    driver.findElement(By.cssSelector("#CampaignId > option:nth-child(2)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("ContactTypeId")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("ContactTypeId"));
		      dropdown.findElement(By.xpath("//option[. = 'Customers']")).click();
		    }
		    driver.findElement(By.cssSelector("#ContactTypeId > option:nth-child(2)")).click();
		    driver.findElement(By.id("CampaignId")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("CampaignId"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.cssSelector("#CampaignId > option:nth-child(1)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("ContactTypeId")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("ContactTypeId"));
		      dropdown.findElement(By.xpath("//option[. = 'Partners']")).click();
		    }
		    driver.findElement(By.cssSelector("#ContactTypeId > option:nth-child(3)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("ContactTypeId")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("ContactTypeId"));
		      dropdown.findElement(By.xpath("//option[. = 'Prospects']")).click();
		    }
		    driver.findElement(By.cssSelector("#ContactTypeId > option:nth-child(4)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("ContactTypeId")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("ContactTypeId"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.cssSelector("#ContactTypeId > option:nth-child(1)")).click();
		    driver.findElement(By.id("FilterTypeCd2")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd2"));
		      dropdown.findElement(By.xpath("//option[. = 'Atlantic Time Zone (UTC-4:00)']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd2 > option:nth-child(2)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd2")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd2"));
		      dropdown.findElement(By.xpath("//option[. = 'Eastern Time Zone (UTC-5:00)']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd2 > option:nth-child(3)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd2")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd2"));
		      dropdown.findElement(By.xpath("//option[. = 'Central Time Zone (UTC-6:00)']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd2 > option:nth-child(4)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd2")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd2"));
		      dropdown.findElement(By.xpath("//option[. = 'Mountain Time Zone (UTC-7:00)']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd2 > option:nth-child(5)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd2")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd2"));
		      dropdown.findElement(By.xpath("//option[. = 'Pacific Time Zone (UTC-8:00)']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd2 > option:nth-child(6)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd2")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd2"));
		      dropdown.findElement(By.xpath("//option[. = 'Alaska Time Zone (UTC-9:00)']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd2 > option:nth-child(7)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd2")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd2"));
		      dropdown.findElement(By.xpath("//option[. = 'Hawaiian Time Zone (UTC-10:00)']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd2 > option:nth-child(8)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    driver.findElement(By.id("FilterTypeCd2")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("FilterTypeCd2"));
		      dropdown.findElement(By.xpath("//option[. = 'Eastern Time Zone (UTC-5:00)']")).click();
		    }
		    driver.findElement(By.cssSelector("#FilterTypeCd2 > option:nth-child(3)")).click();
		    driver.findElement(By.id("lead-summary-search-btn")).click();
		    
		  
		    //update lead
		    System.out.println("Update Lead");
            
		    //driver.findElement(By.xpath("//button[contains(.,\'  Update\')]")).click();
		    

		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //sort by recent leads
		    driver.findElement(By.cssSelector("div.le-listview-col-header:nth-child(2) > div:nth-child(1) > div:nth-child(1)")).click();
		   
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.cssSelector("div.le-listview-col-header:nth-child(2) > div:nth-child(1) > div:nth-child(1)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
            driver.findElement(By.cssSelector("div.le-listview-row:nth-child(1) > div:nth-child(11) > button:nth-child(1)")).click();
            try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
            
           
		    driver.switchTo().defaultContent();
            driver.findElement(By.cssSelector("div.btn-group:nth-child(3) > button:nth-child(2)")).click();
            driver.switchTo().defaultContent();
		  
		    //click-on Logout TAB
            if (myLog.exists()) {
		    	System.out.println("Log Exists, so opening log for appending.");
		    	//logWriter.close();
		    	FileWriter logWriter = new FileWriter(myLog, true);
		    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
		    logWriter.write(logWithNewLine2);
		    logWriter.close();
		    }
	    	
			System.out.println("click-on Logout button - Leads Test PASS");
		    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/a")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.close();
		    driver.quit();
	}
		    

		@AfterTest
		public void tearDownTest() {
			
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}

