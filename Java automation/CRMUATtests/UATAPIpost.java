package vollygroupid.Mavenjava;

	import org.testng.Assert;

import java.io.BufferedReader;
import java.io.BufferedWriter;

//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
//import org.junit.Test;
	//import org.junit.After;
	import java.time.Duration;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
    import java.util.Optional;
    import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

//import javax.net.ssl.HttpsURLConnection;

import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;
	//import org.openqa.selenium.devtools.DevTools;
    //import org.openqa.selenium.devtools.v96.network.*;	
    //import org.openqa.selenium.devtools.v96.fetch.Fetch;

    //import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

import com.google.gson.JsonArray;

import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class UATAPIpost {

		
		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

	    
	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM UAT API post tests";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log does not Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");	
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			
		  //Adding-in Headless Chrome Options
		    boolean headless = true;
		    
			//ChromeOptions options = new ChromeOptions();
			//ChromeDriver driver = new ChromeDriver();
			
		    WebDriver driver = null;
			if (headless) {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--headless");
				options.addArguments("window-size=1920,1080");
				driver = new ChromeDriver(options);
				driver.manage().window().maximize();
			}
			else {
				driver = new ChromeDriver();
				driver.manage().window().maximize();
			}
			
			
			//Send token example from roytuts.com
			//https://roytuts.com/httpsurlconnection-rest-api-call-auth-token-and-proxy-server/
			 //POST tests 
			//tests based on https://api.vollycrm.com/cm/swagger/index.html
		    System.out.println("Test CRM POST:\n");
		    //System.out.println("Generic Campaign Feed:\n");

		    
			
			
			String token = "Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGFpbSI6IlUyRnNkR1ZrWDErSUxBNmRkWXZFRXJZYWRpSUhCNjdHaktlZlV1YS9MbXdjT0xJU3Vnd2g4eVk5MW5DQTQxUzByYURaV2tNR0JzT09nb1NYZ3N6d3NnTi9nZHdnSGxOTFA3d25lcWZleURPSUZTUGlXRHZFcHVXaFhFWjhnMzNRQVJZTzlQcWJJTWkvUXprVDl0WkJJVjNLQUtLODRqKy9GbXZMU0RNaTQyeDVsVVlCRHpJU21wU2ttTGw1YkIwV3E5NFUrckRjcHBERjFVVm4zTzgxeWJqbUxpNGdwdUY4SlBZR1psK3E2aWQyOTIrTms2U1NHazR3bVBoTlh5Y2luajlKbjJnRFRETHVMRmZCMzl0RnhHdFNTUmZROExydnR0T2piLzJxNFZZM3VPYXFVM0F1T1YvNTZCRGJYRTBuZDZjUUZIL0tjMzd3Lzl4QytrWnYrZjlPVElIUy8rSjR4UytqZFE1MkpFcz0iLCJpYXQiOjE2MzI3Njk1NTMsImV4cCI6MTYzMjc3MzE1M30.jsTuZJUTTQ4iiJt21I8hu43OkEkfjS9SBwh8Weyk7-8";
			
			/*
			URL postUrl = new URL("http://api.uat.vollycrm.com/Generic/CampaignFeed");
			
			HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection();

			// Set POST as request method
			connection.setRequestMethod("POST");

			// Setting Header Parameters
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Authorization", "Bearer " + token);
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			
			// Adding Body payload for POST request
			String params = "[{\\\"LclContactCd\\\":\\\"alantest\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"LclLoginId\\\":\\\"A028793\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"LclCampaignCd\\\":\\\"RMPPREQ\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"ContactTypeCd\\\":\\\"C\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"ContactCategoryTypeCd\\\":\\\"1\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"FirstName\\\":\\\"Alan\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"LastName\\\":\\\"Test\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"MiddleInitial\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"Email\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"HomeStreetAddress1\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"HomeStreetAddress2\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"HomeCity\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"HomeZipCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"HomeStateCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"HomeCountryCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"HomePhoneNbr\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"CellPhoneNbr\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkStreetAddress1\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkStreetAddress2\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkCity\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkZipCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkStateCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkCountryCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkPhoneNbr\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"FaxNbr\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkCompanyName\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkTitle\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"WorkMailStop\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SpouseLastName\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SpouseFirstName\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"DOB\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"Comment\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"LOB\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"MarketingOptionTypeCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"FICO\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"Salutation\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"Flex1\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"Flex2\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"DoNotCallCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"DoNotMailCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"DoNotEMailCd\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"Flex3\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"Flex4\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"Flex5\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"Flex6\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyDataExistsIndicator\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal1\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal3\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal4\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal5\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal6\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal7\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal15\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal16\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal17\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal18\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal19\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal20\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal21\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal22\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal23\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal24\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal25\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal26\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal27\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal28\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal29\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal30\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal31\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal32\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal33\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal34\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal35\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal36\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal37\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal38\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal39\\\":\\\"\\\",\\r\\n\"\r\n"
					+ "	    		+ \"  \\\"SurveyFieldVal40\\\":\\\"\\\"\\r\\n\"\r\n"
					+ "	    		+ \"	}]\\r\\n\"\r\n";

			BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
			wr.write(params);
			wr.close();

			connection.connect();

			// Getting Response
			int statusCode = connection.getResponseCode();

			// Checking http status code for 201 (Created)
			if (statusCode == HttpURLConnection.HTTP_CREATED) {
				StringBuffer jsonResponseData = new StringBuffer();
				String readLine = null;
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				while ((readLine = bufferedReader.readLine()) != null) {
					jsonResponseData.append(readLine + "\n");
				}

				bufferedReader.close();
				System.out.println(jsonResponseData.toString());

			} else {
				System.out.println("Status code: "+statusCode+"\n");
			}
		        
			*/
            //Example: https://reqbin.com/req/java/v0crmky0/rest-api-post-example
		    System.out.println("Production Integration Pos Activity:\n");
    
			URL postUrl = new URL("https://api.vollycrm.com/cm/integration/pos/activity?corpCode=ABC");
			HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection();

			// Set POST as request method
			connection.setRequestMethod("POST");

			// Setting Header Parameters
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Authorization", "Bearer " + token);
			connection.setUseCaches(false);
			connection.setDoOutput(true);

			String params ="{\r\n"
					+ "  \"activityAmortizationTypeLclCd\": \"string\",\r\n"
					+ "  \"activityAmt\": \"string\",\r\n"
					+ "  \"activityDt\": \"string\",\r\n"
					+ "  \"activityInterestRatePct\": \"string\",\r\n"
					+ "  \"activityLclId\": \"string\",\r\n"
					+ "  \"activityLienPositionTypeLclCd\": \"string\",\r\n"
					+ "  \"activityProductTypeLclCd\": \"string\",\r\n"
					+ "  \"activitySellOffDt\": \"string\",\r\n"
					+ "  \"activityStatusTypeCd\": \"string\",\r\n"
					+ "  \"activityTermInMonths\": \"string\",\r\n"
					+ "  \"activityTypeLclCd\": \"string\",\r\n"
					+ "  \"altALoanCd\": \"A\",\r\n"
					+ "  \"armResetDt\": \"string\",\r\n"
					+ "  \"batchInterfaceId\": 0,\r\n"
					+ "  \"borrowerDOB\": \"string\",\r\n"
					+ "  \"borrowerFirstName\": \"string\",\r\n"
					+ "  \"borrowerLastName\": \"string\",\r\n"
					+ "  \"borrowerPhoneNbr\": \"string\",\r\n"
					+ "  \"campaignStateLclCd\": \"string\",\r\n"
					+ "  \"cellPhoneNbr\": \"string\",\r\n"
					+ "  \"coBorrowerDOB\": \"string\",\r\n"
					+ "  \"coBorrowerEmail\": \"string\",\r\n"
					+ "  \"coBorrowerFirstName\": \"string\",\r\n"
					+ "  \"coBorrowerLastName\": \"string\",\r\n"
					+ "  \"coBorrowerPhoneNbr\": \"string\",\r\n"
					+ "  \"comment\": \"string\",\r\n"
					+ "  \"constructionLoanCd\": \"S\",\r\n"
					+ "  \"email\": \"string\",\r\n"
					+ "  \"faxNbr\": \"string\",\r\n"
					+ "  \"ficoScore\": \"777\",\r\n"
					+ "  \"flexFieldVal1\": \"string\",\r\n"
					+ "  \"flexFieldVal10\": \"string\",\r\n"
					+ "  \"flexFieldVal11\": \"string\",\r\n"
					+ "  \"flexFieldVal12\": \"string\",\r\n"
					+ "  \"flexFieldVal13\": \"string\",\r\n"
					+ "  \"flexFieldVal14\": \"string\",\r\n"
					+ "  \"flexFieldVal15\": \"string\",\r\n"
					+ "  \"flexFieldVal16\": \"string\",\r\n"
					+ "  \"flexFieldVal17\": \"string\",\r\n"
					+ "  \"flexFieldVal18\": \"string\",\r\n"
					+ "  \"flexFieldVal19\": \"string\",\r\n"
					+ "  \"flexFieldVal2\": \"string\",\r\n"
					+ "  \"flexFieldVal20\": \"string\",\r\n"
					+ "  \"flexFieldVal3\": \"string\",\r\n"
					+ "  \"flexFieldVal4\": \"string\",\r\n"
					+ "  \"flexFieldVal5\": \"string\",\r\n"
					+ "  \"flexFieldVal6\": \"string\",\r\n"
					+ "  \"flexFieldVal7\": \"string\",\r\n"
					+ "  \"flexFieldVal8\": \"string\",\r\n"
					+ "  \"flexFieldVal9\": \"string\",\r\n"
					+ "  \"heInitialDrawAmt\": \"string\",\r\n"
					+ "  \"heMonthsOnBook\": \"8\",\r\n"
					+ "  \"heUtilizationRate\": \"string\",\r\n"
					+ "  \"interestOnlyLoanCd\": \"A\",\r\n"
					+ "  \"jumboFlagCd\": \"D\",\r\n"
					+ "  \"lclBorrowerCd\": \"string\",\r\n"
					+ "  \"lclCategoryCd1\": \"string\",\r\n"
					+ "  \"lclCategoryCd2\": \"string\",\r\n"
					+ "  \"lclCategoryCd3\": \"string\",\r\n"
					+ "  \"lclCategoryCd4\": \"string\",\r\n"
					+ "  \"lclCategoryCd5\": \"string\",\r\n"
					+ "  \"lclCategoryCd6\": \"string\",\r\n"
					+ "  \"lclCategoryCd7\": \"string\",\r\n"
					+ "  \"lclCategoryCd8\": \"string\",\r\n"
					+ "  \"lclCoBorrowerCd\": \"string\",\r\n"
					+ "  \"lclLoginId\": \"string\",\r\n"
					+ "  \"lclPartner1Cd\": \"string\",\r\n"
					+ "  \"lclPartner2Cd\": \"string\",\r\n"
					+ "  \"lclPartner3Cd\": \"string\",\r\n"
					+ "  \"lclPartner4Cd\": \"string\",\r\n"
					+ "  \"lclPartnerCd\": \"string\",\r\n"
					+ "  \"lclReferalCd\": \"string\",\r\n"
					+ "  \"legacyDataCd\": \"C\",\r\n"
					+ "  \"loadStatusTxt\": \"string\",\r\n"
					+ "  \"mailingCity\": \"string\",\r\n"
					+ "  \"mailingCountryCd\": \"string\",\r\n"
					+ "  \"mailingStateCd\": \"MA\",\r\n"
					+ "  \"mailingStreetAddress1\": \"string\",\r\n"
					+ "  \"mailingStreetAddress2\": \"string\",\r\n"
					+ "  \"mailingZipCd\": \"string\",\r\n"
					+ "  \"middleInitial\": \"A\",\r\n"
					+ "  \"originalLTV\": \"123\",\r\n"
					+ "  \"partner1CategoryCd\": \"string\",\r\n"
					+ "  \"partner1City\": \"string\",\r\n"
					+ "  \"partner1CompanyName\": \"string\",\r\n"
					+ "  \"partner1CountryCd\": \"string\",\r\n"
					+ "  \"partner1Email\": \"string\",\r\n"
					+ "  \"partner1FirstName\": \"string\",\r\n"
					+ "  \"partner1LastName\": \"string\",\r\n"
					+ "  \"partner1PhoneNbr\": \"string\",\r\n"
					+ "  \"partner1StateCd\": \"MA\",\r\n"
					+ "  \"partner1StreetAddress\": \"string\",\r\n"
					+ "  \"partner1ZipCd\": \"string\",\r\n"
					+ "  \"partner2CategoryCd\": \"string\",\r\n"
					+ "  \"partner2City\": \"string\",\r\n"
					+ "  \"partner2CompanyName\": \"string\",\r\n"
					+ "  \"partner2CountryCd\": \"string\",\r\n"
					+ "  \"partner2Email\": \"string\",\r\n"
					+ "  \"partner2FirstName\": \"string\",\r\n"
					+ "  \"partner2LastName\": \"string\",\r\n"
					+ "  \"partner2PhoneNbr\": \"string\",\r\n"
					+ "  \"partner2StateCd\": \"MA\",\r\n"
					+ "  \"partner2StreetAddress\": \"string\",\r\n"
					+ "  \"partner2ZipCd\": \"string\",\r\n"
					+ "  \"partner3CategoryCd\": \"string\",\r\n"
					+ "  \"partner3City\": \"string\",\r\n"
					+ "  \"partner3CompanyName\": \"string\",\r\n"
					+ "  \"partner3CountryCd\": \"string\",\r\n"
					+ "  \"partner3Email\": \"string\",\r\n"
					+ "  \"partner3FirstName\": \"string\",\r\n"
					+ "  \"partner3LastName\": \"string\",\r\n"
					+ "  \"partner3PhoneNbr\": \"string\",\r\n"
					+ "  \"partner3StateCd\": \"MA\",\r\n"
					+ "  \"partner3StreetAddress\": \"string\",\r\n"
					+ "  \"partner3ZipCd\": \"string\",\r\n"
					+ "  \"partner4CategoryCd\": \"string\",\r\n"
					+ "  \"partner4City\": \"string\",\r\n"
					+ "  \"partner4CompanyName\": \"string\",\r\n"
					+ "  \"partner4CountryCd\": \"string\",\r\n"
					+ "  \"partner4Email\": \"string\",\r\n"
					+ "  \"partner4FirstName\": \"string\",\r\n"
					+ "  \"partner4LastName\": \"string\",\r\n"
					+ "  \"partner4PhoneNbr\": \"string\",\r\n"
					+ "  \"partner4StateCd\": \"MA\",\r\n"
					+ "  \"partner4StreetAddress\": \"string\",\r\n"
					+ "  \"partner4ZipCd\": \"string\",\r\n"
					+ "  \"partnerCompanyName\": \"string\",\r\n"
					+ "  \"partnerFirstName\": \"string\",\r\n"
					+ "  \"partnerLastName\": \"string\",\r\n"
					+ "  \"partnerPhoneNbr\": \"string\",\r\n"
					+ "  \"propertyCity\": \"string\",\r\n"
					+ "  \"propertyCountryCd\": \"string\",\r\n"
					+ "  \"propertyStateCd\": \"MA\",\r\n"
					+ "  \"propertyStreetAddress1\": \"string\",\r\n"
					+ "  \"propertyStreetAddress2\": \"string\",\r\n"
					+ "  \"propertyTypeLclCd\": \"string\",\r\n"
					+ "  \"propertyUseLclCd\": \"string\",\r\n"
					+ "  \"propertyZipCd\": \"string\",\r\n"
					+ "  \"referalFirstName\": \"string\",\r\n"
					+ "  \"referalLastName\": \"string\",\r\n"
					+ "  \"referalPhoneNbr\": \"string\",\r\n"
					+ "  \"referringPartnerCd\": \"string\",\r\n"
					+ "  \"suffix\": \"string\",\r\n"
					+ "  \"workCity\": \"string\",\r\n"
					+ "  \"workCompanyName\": \"string\",\r\n"
					+ "  \"workCountryCd\": \"string\",\r\n"
					+ "  \"workMailStop\": \"string\",\r\n"
					+ "  \"workPhoneNbr\": \"string\",\r\n"
					+ "  \"workStateCd\": \"MA\",\r\n"
					+ "  \"workStreetAddress1\": \"string\",\r\n"
					+ "  \"workStreetAddress2\": \"string\",\r\n"
					+ "  \"workTitle\": \"string\",\r\n"
					+ "  \"workZipCd\": \"string\"\r\n"
					+ "}";
			
			BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
			wr.write(params);
			wr.close();

			connection.connect();

			// Getting Response
			int statusCode = connection.getResponseCode();
			String statusResponse = connection.getResponseMessage();
			
			// Checking http status code for 201 (Created)
			if (statusCode == HttpURLConnection.HTTP_CREATED) {
				StringBuffer jsonResponseData = new StringBuffer();
				String readLine = null;
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				while ((readLine = bufferedReader.readLine()) != null) {
					jsonResponseData.append(readLine + "\n");
				}

				bufferedReader.close();
				System.out.println(jsonResponseData.toString());

			} else {
				System.out.println("Status code: "+statusCode+"\n");
				System.out.println("Reason: "+statusResponse+"\n\n");
				
			}
			
			//Test 2
            //Example: https://reqbin.com/req/java/v0crmky0/rest-api-post-example
		    System.out.println("Production Integration Activity:\n");
    
			URL postUrlb = new URL("https://api.vollycrm.com/cm/integration/activity?corpCode=ABC");
			HttpURLConnection connectionb = (HttpURLConnection) postUrlb.openConnection();

			// Set POST as request method
			connectionb.setRequestMethod("POST");

			// Setting Header Parameters
			connectionb.setRequestProperty("Content-Type", "application/json");
			connectionb.setRequestProperty("Authorization", "Bearer " + token);
			connectionb.setUseCaches(false);
			connectionb.setDoOutput(true);
    

	
			BufferedWriter wrb = new BufferedWriter(new OutputStreamWriter(connectionb.getOutputStream(), "UTF-8"));
			wrb.write(params);
			wrb.close();

			connectionb.connect();

			// Getting Response
			int statusCodeb = connectionb.getResponseCode();
			String statusResponseb = connectionb.getResponseMessage();
		
			// Checking http status code for 201 (Created)
			if (statusCodeb == HttpURLConnection.HTTP_CREATED) {
				StringBuffer jsonResponseDatab = new StringBuffer();
				String readLineb = null;
				BufferedReader bufferedReaderb = new BufferedReader(new InputStreamReader(connectionb.getInputStream()));

				while ((readLineb = bufferedReaderb.readLine()) != null) {
					jsonResponseDatab.append(readLineb + "\n");
				}

				bufferedReaderb.close();
				System.out.println(jsonResponseDatab.toString());

			} else {
				System.out.println("Status code: "+statusCodeb+"\n");
				System.out.println("Reason: "+statusResponseb+"\n\n");
		
		
		
			}
	

	
	System.out.println("API POST tests PASS");
    
    if (myLog.exists()) {
    	System.out.println("Log Exists, so opening log for appending.");
    	//logWriter.close();
    	FileWriter logWriter = new FileWriter(myLog, true);
    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
    logWriter.write(logWithNewLine2);
    logWriter.close();
    }
    driver.close();
    driver.quit();
}
		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			//driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}
