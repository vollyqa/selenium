package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Test;
	//import org.junit.After;
	import java.time.Duration;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class UATSalesforce {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		// JavascriptExecutor js;

		// js = (JavascriptExecutor) driver;

//		public static void main(String[] args) {

		/*
		 * @BeforeTest public void setUp() throws Exception {
		 * System.setProperty("webdriver.chrome.driver",
		 * "C:\\Users\\acronin\\eclipse\\chromedriver.exe"); WebDriver driver = new
		 * ChromeDriver(); }
		 */
	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "UAT Salesforce Create User";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						//logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log does not Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				//logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			//WebDriver driver = new ChromeDriver();		
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
		    
		    //click-on Login-button
			System.out.println("Login to Prod Salesforce");
		    
		    
			//Salesforce Prod login
			 driver.get("https://volly-sales-dev-ed.my.salesforce.com/");
			 driver.manage().window().setSize(new Dimension(1437, 905));
			 driver.findElement(By.name("username")).click();
			 driver.findElement(By.name("username")).sendKeys("abeck@myvolly.sales");
			 driver.findElement(By.id("password")).click();
			 driver.findElement(By.id("password")).sendKeys("Auggrid48");
			 driver.findElement(By.cssSelector("#Login")).click();
			 
			 
			 //two-factor auth
			 
			//click Contact section
			 System.out.println("Click Contact section");
			 driver.findElement(By.cssSelector(".slds-rich-text-editor__output > p:nth-child(1) > a:nth-child(2)")).click();
			 
			 
			 //click Marketing Portal link
			 System.out.println("Click Marketing Portal button");
			 driver.findElement(By.cssSelector(".slds-rich-text-editor__output > p:nth-child(1) > a:nth-child(2)")).click();
			 
				
			try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
			
			    
			driver.switchTo().frame(0);
			    
			    //click all categories
			    driver.findElement(By.id("id-0")).click();
			    try {
				      Thread.sleep(5000);
				    } catch (InterruptedException e) {
				      e.printStackTrace();
				    }
			    
			    driver.findElement(By.cssSelector(".button")).click();
			    
			    
			    
			    try {
				      Thread.sleep(5000);
				    } catch (InterruptedException e) {
				      e.printStackTrace();
				    }
			    //click about us flyer
			    driver.findElement(By.cssSelector(".product-item:nth-child(1) > .product-name > .LinesEllipsis > div")).click();
			    
		   
		    

		    //click-on Logout TAB
			    if (myLog.exists()) {
			    	System.out.println("Log Exists, so opening log for appending.");
			    	//logWriter.close();
			    	FileWriter logWriter = new FileWriter(myLog, true);
			    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
			    logWriter.write(logWithNewLine2);
			    logWriter.close();
			    }
			System.out.println("Logout of Salesforce");
		    driver.findElement(By.xpath("/html/body/div[4]/div[1]/section/header/div[2]/span/div[2]/ul/li[9]/span/button/div/span[1]/div")).click();
		    driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[4]/div[1]/div[1]/div/div[2]/div/a[2]")).click();

		    driver.close();
			driver.quit();
	}
	

		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			//driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}
