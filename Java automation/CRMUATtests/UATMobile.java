package vollygroupid.Mavenjava;

	import org.testng.Assert;
  
	import org.apache.poi.ss.usermodel.Cell;  
	//import org.apache.poi.ss.usermodel.CellType; 
	import org.apache.poi.ss.usermodel.Row;  
	import org.apache.poi.ss.usermodel.Sheet;  
	import org.apache.poi.ss.usermodel.Workbook; 
	import org.apache.poi.ss.usermodel.WorkbookFactory; 

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileInputStream;
	import java.io.FileOutputStream;
	import java.io.FileWriter;
	import java.io.IOException;
	import java.time.format.TextStyle;
	import java.text.DecimalFormat;
	//import org.junit.Before;
	//import org.junit.Test;
	import org.junit.After;
	import static org.junit.Assert.*;
	import static org.hamcrest.CoreMatchers.is;
	import static org.hamcrest.core.IsNot.not;
	import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
import java.util.Optional;
import java.util.Set;
	import java.util.Iterator;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;
	import org.openqa.selenium.Alert;
	

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.openqa.selenium.JavascriptExecutor;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

import com.google.common.collect.ImmutableList;

import io.github.selcukes.devtools.protocol.ChromeDevTools;
import io.github.selcukes.devtools.protocol.types.network.Response;

import java.io.FileNotFoundException; 
	import java.io.InputStream;  
	import java.io.OutputStream;  
 
	import org.testng.annotations.AfterTest;
	import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v85.emulation.Emulation;
import org.openqa.selenium.devtools.v85.network.Network;
import org.openqa.selenium.devtools.v85.security.Security;

	public class UATMobile {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

	//	  @Before
		  public void setUp() {
			  	ChromeDriver driver = new ChromeDriver();
				
		    //js = (JavascriptExecutor) driver;
		    vars = new HashMap<String, Object>();
		    
		    
		  }
	//	  @After
		  public void tearDown() {
				// close browser
				//driver.close();
				driver.quit();
				System.out.println("Test Completed Successfully!");
		  }
	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "UAT Mobile";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}


		//WebElement ele = driver.findElement(By.xpath("//*[@id='content']/div/div/div[2]/form/span/button"));
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("arguments[0].scrollIntoView();", ele);
		
		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
			
		    System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		    ChromeOptions options = new ChromeOptions();		
		    ChromeDriver driver = new ChromeDriver();
		    DevTools devTools = driver.getDevTools();
		    
		    devTools.createSession();
		    devTools.send(Security.enable());
		    devTools.send(Security.setIgnoreCertificateErrors(true));
		    
		    devTools.send(Emulation.setDeviceMetricsOverride(600, 1000, 50, true, Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()));
		    
		    //mobile testing
	        Map deviceMetrics = new HashMap()
	        {{
	            put("width", 600);
	            put("height", 1000);
	            put("mobile", true);
	            put("deviceScaleFactor", 50);
	        }};
	        driver.executeCdpCommand("Emulation.setDeviceMetricsOverride", deviceMetrics);
	        
	      
	        //location of testing, localization (Berlin)
	        
	        Map<String,Object>coordinates = new HashMap<String,Object>();
	        coordinates.put("Latitude", 52.5);
	        coordinates.put("Longitude", 13.4);
	        coordinates.put("accuracy", 1);
	        
	        driver.executeCdpCommand("Emulation.setGeolocationOverride", coordinates);
	        //driver.get("http://dw.com/de");
	        
	        
	        devTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));
	        
	        devTools.addListener(Network.loadingFailed(), loadingfailed ->
	        {
	        	System.out.println(loadingfailed.getErrorText());
	        	System.out.println(loadingfailed.getTimestamp());
	        	
	        });
	        
	        devTools.send(Network.setBlockedURLs(ImmutableList.of("*.jpg","*.css")));
	        
	        /*
	        devTools.addListener(Network.requestWillBeSent(), request ->
	        
	        		{
	        			if (request.getRequest().getUrl().contains("QA")){
	        			
	        			 Request req = request.getRequest();
	        			 System.out.println(req);
	   }
	        		});
	        
	        
	        devTools.addListener(Network.responseReceived(), response ->
	        
	        {
	        	org.openqa.selenium.devtools.v85.network.model.Response res = response.getResponse();
	        	//System.out.println(res.getURL());
	        	//System.out.println(res.getStatus());
	        	if(res.getStatus().toString().startsWith("4"))
	        	{
	        		System.out.println(res.getUrl()+" is failing with status code "+res.getStatus());
	        	}
	        });
	        */
	      
	    	//log network errors
	        devTools.addListener(Network.loadingFailed(), loadingFailed->
	        {
	        	System.out.println(loadingFailed.getErrorText());
	        	System.out.println(loadingFailed.getTimestamp());
	       
	        });
	        
	        //LogEntries entry=driver.manage().logs().get(LogType.BROWSER);
	         //List<LogEntry>logs = entry.getAll();
	         
	         //for(LogEntry e : logs)
	         //{
	        	// System.out.println(e.getMessage());
	         //}
	        
		    
			//ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			//options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			//driver = new ChromeDriver(options);
			
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> wh = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandle = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    vars.put(currentWindowHandle, waitForWindow(2000));
		    driver.executeCdpCommand("Emulation.setDeviceMetricsOverride", deviceMetrics);
		    
		    //#divCustomerManagerBody > div.site-menu > div > div:nth-child(3) > button
		    //click-on Customers TAB
			//System.out.println("click-on Customers TAB");
		    //driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //click-on Profile TAB
			System.out.println("click-on Profile TAB");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }

		    driver.switchTo().frame(0);
		    //driver.findElement(By.xpath("//*[@id=\"section0\"]")).click();
		    //driver.get("https://cit.vollycrm.com//site/login.asp?PassedUserCorp=ABC2");
		    //driver.manage().window().setSize(new Dimension(1437, 905));
		    //driver.findElement(By.id("idLogin")).click();
		    //driver.findElement(By.id("idLogin")).sendKeys("Abeck-CM");
		    //driver.findElement(By.id("idPassword")).sendKeys("Testpw1234");
		    //driver.findElement(By.id("idUserCorp")).sendKeys("ABC2");
		    //driver.findElement(By.xpath("//input[@value=\'Log In\']")).click();
		    //driver.findElement(By.xpath("//button[@onclick=\'document.getElementById(\"masterIframe\").src=\"LOProfile.asp\"\']")).click();
		    //driver.switchTo().frame(0);
		    //driver.findElement(By.cssSelector("table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(1) tr:nth-child(3) > td")).click();
		    //vars.put("nday", js.executeScript("return new Date().getDate();"));
		    //vars.put("nhour", js.executeScript("return new Date().getHours();"));
		    
		    //LocalDate currentDate = LocalDate.now();
		    //Date da = new Date();
		    //DayOfWeek ndate = currentDate.getDayOfWeek();
		    //int nmonth = currentDate.getMonthValue();
		    //int nday = currentDate.getDayOfMonth();
		    //int nhour = da.getHours();
		    //int nmin = da.getMinutes();
		    
		    Calendar c = Calendar.getInstance();
		    int nmonth = c.get(Calendar.MONTH);
		    int nday = c.get(Calendar.DAY_OF_MONTH);
		    int nhour = c.get(Calendar.HOUR_OF_DAY);
		    int nmin = c.get(Calendar.MINUTE);
		    
		    driver.findElement(By.id("idCardLine1")).click();
		    driver.findElement(By.id("idCardLine1")).clear();
		    driver.findElement(By.id("idCardLine1")).sendKeys("Alan"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("idCardLine2")).click();
		    driver.findElement(By.id("idCardLine2")).clear();
		    driver.findElement(By.id("idCardLine2")).sendKeys("QA"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("idCardLine3")).click();
		    driver.findElement(By.id("idCardLine3")).clear();
		    driver.findElement(By.id("idCardLine3")).sendKeys("53 Commerce Way");
		    driver.findElement(By.id("idCardLine4")).click();
		    driver.findElement(By.id("idCardLine4")).clear();
		    driver.findElement(By.id("idCardLine4")).sendKeys("Woburn, MA 01801");
		    driver.findElement(By.id("idCardLine5")).click();
		    driver.findElement(By.id("idCardLine5")).clear();
		    driver.findElement(By.id("idCardLine5")).sendKeys("none");
		    driver.findElement(By.id("idCardLine6")).click();
		    driver.findElement(By.id("idCardLine6")).clear();
		    driver.findElement(By.id("idCardLine6")).sendKeys("test"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("idCardLine7")).click();
		    driver.findElement(By.id("idCardLine7")).clear();
		    driver.findElement(By.id("idCardLine7")).sendKeys("none");
		    driver.findElement(By.id("idCardLine8")).click();
		    driver.findElement(By.id("idCardLine8")).clear();
		    driver.findElement(By.id("idCardLine8")).sendKeys("abeck@myvolly.com");
		    driver.findElement(By.id("idRAddrLine1")).click();
		    driver.findElement(By.id("idRAddrLine1")).clear();
		    driver.findElement(By.id("idRAddrLine1")).sendKeys("Alan Beck");
		    driver.findElement(By.id("idRAddrLine2")).click();
		    driver.findElement(By.id("idRAddrLine2")).clear();
		    driver.findElement(By.id("idRAddrLine2")).sendKeys("Volly");
		    driver.findElement(By.id("idRAddrLine3")).click();
		    driver.findElement(By.id("idRAddrLine3")).clear();
		    driver.findElement(By.id("idRAddrLine3")).sendKeys("53 Commerce Way");
		    driver.findElement(By.id("idRAddrLine4")).click();
		    driver.findElement(By.id("idRAddrLine4")).clear();
		    driver.findElement(By.id("idRAddrLine4")).sendKeys("Woburn, MA  01801");
		    driver.findElement(By.id("idRAddrLine5")).click();
		    driver.findElement(By.id("idRAddrLine5")).clear();
		    driver.findElement(By.id("idRAddrLine5")).sendKeys("test"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("idSignLine1")).click();
		    driver.findElement(By.id("idSignLine1")).clear();
		    driver.findElement(By.id("idSignLine1")).sendKeys("Alan");
		    driver.findElement(By.id("idSignLine2")).click();
		    driver.findElement(By.id("idSignLine2")).clear();
		    driver.findElement(By.id("idSignLine2")).sendKeys("Alan Beck");
		    driver.findElement(By.id("idSignLine3")).click();
		    driver.findElement(By.id("idSignLine3")).clear();
		    driver.findElement(By.id("idSignLine3")).sendKeys("Alan Beck");
		    driver.findElement(By.id("idSignLine4")).click();
		    driver.findElement(By.id("idSignLine4")).clear();
		    driver.findElement(By.id("idSignLine4")).sendKeys("781-938-1175");
		    driver.findElement(By.id("idCCType")).click();
		    driver.findElement(By.id("idCCType")).click();
		    driver.findElement(By.id("idCC")).click();
		    driver.findElement(By.id("idCC")).clear();
		    driver.findElement(By.id("idCC")).sendKeys("123123132123");
		    driver.findElement(By.id("idMM")).click();
		    driver.findElement(By.id("idMM")).clear();
		    driver.findElement(By.id("idMM")).sendKeys("03");
		    driver.findElement(By.id("idYY")).click();
		    driver.findElement(By.id("idYY")).clear();
		    driver.findElement(By.id("idYY")).sendKeys("20");
		    driver.findElement(By.xpath("(//button[@value=\'Save Profile\'])[2]")).click();
		    driver.findElement(By.id("idSMSTextEmail")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("idState_"));
		      dropdown.findElement(By.xpath("//option[. = 'Massachusetts']")).click();
		    }
		    driver.findElement(By.id("idZip_")).clear();
		    driver.findElement(By.id("idZip_")).sendKeys("02421");
		    driver.findElement(By.id("idSMSTextEmail")).clear();
		    driver.findElement(By.id("idSMSTextEmail")).sendKeys(nmonth+nday+nhour+nmin+"@yopmail.com");
		    driver.findElement(By.xpath("//input[@value=\'Save and Test\']")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(2) .btn")).click();
		    //driver.close();

		    //driver.switchTo().defaultContent();
		    
		  //click on Information icons
		    System.out.println("click-on Profile Information icons");
		    
		    driver.findElement(By.cssSelector("#idFormProfile > table:nth-child(12) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(3) > td:nth-child(1) > a:nth-child(2) > img:nth-child(1)")).click();
		    try {
			      Thread.sleep(500);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		  
		    
		    driver.findElement(By.cssSelector("#idFormProfile > table:nth-child(12) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(13) > td:nth-child(1) > a:nth-child(2) > img:nth-child(1)")).click();
		    try {
			      Thread.sleep(500);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector("#idFormProfile > table:nth-child(12) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(1) > a:nth-child(2) > img:nth-child(1)")).click();
		    try {
			      Thread.sleep(500);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector("#idFormProfile > table:nth-child(12) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(5) > td:nth-child(1) > a:nth-child(2) > img:nth-child(1)")).click();
		    try {
			      Thread.sleep(500);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector("#idFormProfile > table:nth-child(12) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(6) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > a:nth-child(1) > img:nth-child(1)")).click();
		    try {
			      Thread.sleep(500);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector("#idFormProfile > table:nth-child(12) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(7) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(3) > td:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > a:nth-child(1) > img:nth-child(1)")).click();
		    try {
			      Thread.sleep(500);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		   
		    String parent=driver.getWindowHandle();
		    
		    Set<String>s=driver.getWindowHandles();

		 // Now iterate using Iterator
		 Iterator<String> I1= s.iterator();

		 while(I1.hasNext())
		 {

		 String child_window=I1.next();


		 if(!parent.equals(child_window))
		 {
		 driver.switchTo().window(child_window);

		 System.out.println(driver.switchTo().window(child_window).getTitle());

		 driver.close();
		 }
		 }
		 //switch to the parent window
		 driver.switchTo().window(parent);
		 
		 driver.close();
		 
		 //driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.executeCdpCommand("Emulation.setDeviceMetricsOverride", deviceMetrics);
		    
		    Set<String> whc = driver.getWindowHandles();
			System.out.println("this is value of wh: " + whc);
			String currentWindowHandlec = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandlec);
		    vars.put(currentWindowHandlec, waitForWindow(2000));
		    

		    //Profile - Access Admin
		    System.out.println("click-on Access Admin");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/div/a[2]")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.switchTo().frame(0);
		    
		   
		    driver.findElement(By.linkText("Add Users")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("LastNameFilter")).click();
		    driver.findElement(By.id("LastNameFilter")).sendKeys("b");
		    driver.findElement(By.cssSelector(".btn-primary:nth-child(1)")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(1) > td:nth-child(1) > input")).click();
		    driver.findElement(By.id("addselected")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    System.out.println("click-on Delete Admin User");
		    driver.findElement(By.id("deleteUser")).click();
		    
		    //driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("a.btn-small:nth-child(1)")).click();
		    
		    
		    
		    driver.findElement(By.id("saveBtn")).click();
		    driver.switchTo().defaultContent();
		    
		    //Profile - Change Password
		    System.out.println("click-on Change Password");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/div/a[3]")).click();
		    
		    Thread.sleep(5000);
		    driver.switchTo().frame(0);
		    
		    assertThat(driver.findElement(By.cssSelector(".blurb-password > strong")).getText(), is("Please Change Your Password"));
		    driver.findElement(By.id("txtOldPassword")).click();
		    driver.findElement(By.id("txtNewPassword")).click();
		    driver.findElement(By.id("txtVerifyPassword")).click();
		    driver.findElement(By.cssSelector(".btn")).click();
		    
		    Alert alert = driver.switchTo().alert();
		    String alertMessage = driver.switchTo().alert().getText();
		    System.out.println(alertMessage);
		    //alert.accept();
		    
		    //assertThat(driver.switchTo().alert().getText(), is("The length of your password must be at least 9 characters."));
		    driver.switchTo().alert().accept();
		    driver.findElement(By.id("txtOldPassword")).click();
		    driver.findElement(By.id("txtOldPassword")).sendKeys("123456789");
		    driver.findElement(By.id("txtNewPassword")).click();
		    driver.findElement(By.id("txtNewPassword")).sendKeys("123456789");
		    driver.findElement(By.id("txtVerifyPassword")).click();
		    driver.findElement(By.id("txtVerifyPassword")).sendKeys("123456789");
		    driver.findElement(By.cssSelector(".btn")).click();
		    Alert alert2 = driver.switchTo().alert();
		    String alertMessage2 = driver.switchTo().alert().getText();
		    System.out.println(alertMessage2);
		    driver.switchTo().alert().accept();
		    
		    //assertThat(driver.switchTo().alert().getText(), is("Your new password is the same as your old password."));
		  
		    
		   driver.switchTo().defaultContent();
		    
		    //Profile - Help
		    System.out.println("click-on Help");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/div/a[4]")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.switchTo().frame(0);
		    
		    assertThat(driver.findElement(By.cssSelector("h4")).getText(), is("Online Help"));
		    assertThat(driver.findElement(By.linkText("VollyCare@myvolly.com")).getText(), is("VollyCare@myvolly.com"));
		    vars.put("window_handles", driver.getWindowHandles());
		    driver.findElement(By.linkText("Read More...")).click();
		    
		    vars.put("win5269", waitForWindow(2000));
		    vars.put("root", driver.getWindowHandle());
		    //driver.switchTo().window(vars.get("win5269").toString());
		    //driver.close();
		    driver.switchTo().window(vars.get("root").toString());
		    driver.findElement(By.cssSelector("body")).click();
		    driver.switchTo().frame(0);
		    
		    System.out.println("click-on Help - FAQ");
		    assertThat(driver.findElement(By.cssSelector("tr:nth-child(3) h2")).getText(), is("FAQs"));
		    vars.put("window_handles", driver.getWindowHandles());
		   
		    System.out.println("click-on Help - User Guide");
		    driver.findElement(By.cssSelector("tr:nth-child(3) a")).click();
		    vars.put("win3871", waitForWindow(2000));
		    //driver.switchTo().window(vars.get("win3871").toString());
		    //driver.close();
		    
		    //driver.switchTo().window(currentWindowHandle);
		    driver.close();
		    
		    //driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
		    driver.executeCdpCommand("Emulation.setDeviceMetricsOverride", deviceMetrics);
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
			 
		    driver.executeCdpCommand("Emulation.setDeviceMetricsOverride", deviceMetrics);
		
		    //Profile - Upload Contacts
		    System.out.println("click-on Upload Contacts");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/div/a[5]")).click(); 
		   		    
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    
		    assertThat(driver.findElement(By.cssSelector("#step1Tab > .step1-tab-content")).getText(), is("1. Upload Contacts"));
		    assertThat(driver.findElement(By.cssSelector("#listUploadStep1 > .upload-title")).getText(), is("Upload Contacts"));
		    driver.findElement(By.xpath("//img[@alt=\'View Detail\']")).click();
		    driver.findElement(By.cssSelector("#helpModal .btn")).click();
		    driver.findElement(By.id("uploadType")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("uploadType"));
		      dropdown.findElement(By.xpath("//option[. = 'Upload to my Book']")).click();
		    }
		    driver.findElement(By.id("contactType")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("contactType"));
		      dropdown.findElement(By.xpath("//option[. = 'Customer']")).click();
		    }
		    
		    //upload file
		    
		    System.out.println("Modify Upload File");
		    //driver.findElement(By.id("uploadHeaderRow")).click();
		    //driver.findElement(By.id("uploadFile")).click();
		  		    
		    File file =new File("C:\\customercontacts.xlsx");
		    FileInputStream inp = new FileInputStream(file); 
		    Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);  
            Row row = sheet.getRow(1);
            
            if (row == null) {
                row = sheet.createRow(1);
            }
            //get cell at index 3  
            Cell cell = row.getCell(2);  
            
            if (cell == null) {
               cell = row.createCell(2);
            }
            inp.close();
            String cellContents = cell.getStringCellValue(); 
            
            System.out.println("Write data to file");
            cellContents=("AutomatedUpload"+nmonth+nday+nhour+nmin);
              cell.setCellValue(cellContents);
            
            //get cell at index 5  
            Cell cellb = row.getCell(4);  
            
            if (cellb == null) {
               cellb = row.createCell(4);
            }
            inp.close();
            String cellContentsb = cellb.getStringCellValue(); 
            
            System.out.println("Write data to file");
            cellContentsb=("QAtest"+nmonth+nday+nhour+nmin+"@yopmail.com");
              cellb.setCellValue(cellContentsb);         
            FileOutputStream fileOut = new FileOutputStream("C:\\customercontacts.xlsx");  
            wb.write(fileOut);  
            fileOut.close();
		    
            
		    //upload modified file
            System.out.println("Upload modified File");
		    
		    
            WebElement addFile = driver.findElement(By.cssSelector("#uploadFile"));	
            addFile.sendKeys("C:\\customercontacts.xlsx");
            
            try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
            
            driver.findElement(By.cssSelector(".step1-content-group-check")).click();
            driver.findElement(By.id("step1NextBtn")).click();
            
            try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
            
            
            //after file upload
    		assertThat(driver.findElement(By.cssSelector("#listUploadStep2 > .upload-title")).getText(), is("Match Your Fields"));
    		driver.findElement(By.id("step2NextBtn")).click();
    		try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
    		assertThat(driver.findElement(By.cssSelector("#listUploadStep3 > .upload-title")).getText(), is("Upload Verification"));
    		//driver.findElement(By.id("step3CancelBtn")).click();
    		//driver.findElement(By.cssSelector(".confirmbutton-yes")).click();
    	  
    		driver.findElement(By.cssSelector("#step3SubmitBtn")).click();
    		try {
  		      Thread.sleep(5000);
  		    } catch (InterruptedException e) {
  		      e.printStackTrace();
  		    }
    		
    		//click Return to Dashboard
            System.out.println("Click return to Dashboard");
            
            driver.findElement(By.id("returnToDashboard")).click();
            
            //go to profile upload contacts
            //Profile - Upload Contacts
		    System.out.println("click-on Upload Contacts");
		    
		    driver.switchTo().defaultContent();
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/div/a[5]")).click(); 
		   		    
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
            
    		
		    driver.findElement(By.id("uploadHeaderRow")).click();
		    driver.findElement(By.id("uploadInstructions")).click();
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    assertThat(driver.findElement(By.cssSelector("#instructionsModal #helpModalTitle")).getText(), is("File Upload Instructions"));
		    driver.findElement(By.cssSelector("#instructionsModal span")).click();
		    driver.findElement(By.id("downloadTemplate")).click();
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    assertThat(driver.findElement(By.cssSelector("#downloadTemplateModal #helpModalTitle")).getText(), is("Download Templates"));
		    driver.findElement(By.cssSelector("#downloadTemplateModal .btn")).click();
		    driver.findElement(By.linkText("Previous Uploads To My Book")).click();
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(1) > td > .btn")).click();
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(1) > .btn")).click();
		    
		    
		    driver.switchTo().defaultContent();
		    
		    
		    //Profile - Merge Contacts
		    System.out.println("click-on Merge Contacts");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/div/a[6]")).click();
		    
		    Thread.sleep(5000);
		    driver.switchTo().frame(0);
		    
		    driver.findElement(By.id("idSortBy")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("idSortBy"));
		      dropdown.findElement(By.xpath("//option[. = 'Street Address, City, State, Last Name']")).click();
		    }
		    driver.findElement(By.id("idSortBy")).click();
		    driver.findElement(By.id("idContactType")).click();
		    driver.findElement(By.id("idContactType")).click();
		    driver.findElement(By.cssSelector(".btn-small")).click();
		    //driver.findElement(By.id("showNCOA")).click();
		    //driver.findElement(By.cssSelector(".btn-small")).click();
		    driver.findElement(By.id("pendingContacts")).click();
		    driver.findElement(By.id("idSortBy")).click();
		    
		    driver.switchTo().defaultContent();
		    
		    //Profile - My Links Maintenance
		    System.out.println("click-on My Links Maintenance");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/div/a[7]")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    
		    driver.findElement(By.cssSelector(".btn")).click();
		    driver.findElement(By.id("newLinkTitle")).click();
		    driver.findElement(By.id("newLinkTitle")).sendKeys("test");
		    driver.findElement(By.id("newLinkUrl")).click();
		    driver.findElement(By.id("newLinkUrl")).sendKeys("www.google.com");
		    driver.findElement(By.id("newLinkSeqNbr")).click();
		    driver.findElement(By.id("newLinkSeqNbr")).sendKeys("1");
		    driver.findElement(By.cssSelector(".btn-small")).click();
		    driver.findElement(By.cssSelector(".btn-small")).click();
		    
		    driver.switchTo().defaultContent();
		    
		    //Profile - Lead Opportunity Maintenance
		    System.out.println("click-on Lead Opportunity Maintenance");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[9]/div/a[8]")).click();
		    
		    Thread.sleep(5000);
		    driver.switchTo().frame(0);
		    
		    assertThat(driver.findElement(By.cssSelector("h4")).getText(), is("Available Lead Opportunities"));
		    driver.findElement(By.linkText("Invest-PReferral")).click();
		    try {
		      Thread.sleep(1000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    assertThat(driver.findElement(By.cssSelector("h4")).getText(), is("Lead Opportunity Maintenance: Invest-PReferral"));
		    driver.findElement(By.linkText("Back to Lead Opportunity Maintenance")).click();	    
		    
		    
		    driver.switchTo().defaultContent();
		    
		    
		    
		    
		    
		    //click-on Logout TAB
		    if (myLog.exists()) {
		    	System.out.println("Log Exists, so opening log for appending.");
		    	//logWriter.close();
		    	FileWriter logWriter = new FileWriter(myLog, true);
		    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
		    logWriter.write(logWithNewLine2);
		    logWriter.close();
		    }
		    
			System.out.println("click-on Logout button - Profile Test PASS");
		    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/a")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }

		    driver.close();
		    driver.quit();
		    
			 }	    

	@AfterTest
			public void tearDownTest() {
				// close browser
				System.out.println("Test Completed Successfully!");
			}

			/*
			 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
			 * driver.quit(); }
			 */
		
	}
