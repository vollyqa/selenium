package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Test;
	//import org.junit.After;
	import static org.junit.Assert.*;
	import static org.hamcrest.CoreMatchers.is;
	import static org.hamcrest.core.IsNot.not;
	import java.time.Duration;
	import java.time.LocalDate;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.Date;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.apache.commons.io.FileUtils;
	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.openqa.selenium.JavascriptExecutor;
	import org.openqa.selenium.Alert;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class UATAdmin {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		JavascriptExecutor js = (JavascriptExecutor)driver;

		//js.executeScript("alert('Javascript test');");
		

	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";
		String testCaseName = "UAT Admin";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log does not Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		  	System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver = new FirefoxDriver(options);
			//driver.manage().window().maximize();
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> wh = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandle = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    vars.put(currentWindowHandle, waitForWindow(2000));
		    
		    //#divCustomerManagerBody > div.site-menu > div > div:nth-child(3) > button
		    
		    //click-on Admin TAB
			
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    System.out.println("click-on Bulk Tag Assignment");
		    //System.out.println("Bulk Tag Assignment not tested");
		    
		    driver.findElement(By.cssSelector("#section2 > div")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.xpath("//li[contains(.,\'Bulk Tag Assignment\')]")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Assign Tag Attributes To Loan Officers"));
		    
		  
		    driver.findElement(By.id("TagCategory")).click();
		  
		    {
		      WebElement dropdown = driver.findElement(By.id("TagCategory"));
		      dropdown.findElement(By.xpath("//option[. = 'Product']")).click();
		    }
		    
		    driver.findElement(By.id("TagCategory")).click();
		    
		    File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		    FileUtils.copyFile(src,new File("C://tagging.jpg"));
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.cssSelector(".atb-checkbox")).click();
		    
		    driver.findElement(By.id("Next")).click();
		    driver.findElement(By.id("GroupId")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("GroupId"));
		      dropdown.findElement(By.xpath("//option[. = 'Journey Bank']")).click();
		    }
		    driver.findElement(By.id("Hierarchy")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("Hierarchy"));
		      dropdown.findElement(By.xpath("//option[. = 'JourneyBank Home Division 2 : ALL']")).click();
		    }
		    driver.findElement(By.id("Search")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }		
		    
		    driver.findElement(By.id("CorpUser0")).click();
		    assertThat(driver.findElement(By.cssSelector("#step2Tab > .step1-tab-content")).getText(), is("2. Select Loan Officer(s)"));
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    driver.findElement(By.id("Next")).click();
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    assertThat(driver.findElement(By.cssSelector("#step3Tab > .step1-tab-content")).getText(), is("3. Confirm"));
		    
		    
		    driver.switchTo().defaultContent();
		    
		    //click Corp User Summary add user
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    
		    Calendar c = Calendar.getInstance();
		    int nmonth = c.get(Calendar.MONTH);
		    int nday = c.get(Calendar.DAY_OF_MONTH);
		    int nhour = c.get(Calendar.HOUR_OF_DAY);
		    int nmin = c.get(Calendar.MINUTE);
		    
		    nmonth=nmonth+1;
		    
		    System.out.println("click-on Corp User Summary");
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("#section2 > div")).click();
		    driver.findElement(By.xpath("//li[contains(.,\'Corp User Summary\')]")).click();
		    driver.findElement(By.id("btnSearch")).click();
		    driver.findElement(By.cssSelector(".btn:nth-child(1)")).click();
		    
		    //switch to new window
		    assertThat(driver.findElement(By.cssSelector("td > .formTable:nth-child(1) > caption")).getText(), is(" Corp User Detail"));
		    driver.findElement(By.id("txtLoginId")).click();
		    driver.findElement(By.id("txtLoginId")).sendKeys("QAtest"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("txtLclLoginId")).click();
		    driver.findElement(By.id("txtLclLoginId")).sendKeys("JYB_QAtest"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("txtFirstName")).click();
		    driver.findElement(By.id("txtFirstName")).sendKeys("QA");
		    driver.findElement(By.id("txtLastName")).click();
		    driver.findElement(By.id("txtLastName")).sendKeys("Test"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("txtTitle")).click();
		    driver.findElement(By.id("txtTitle")).sendKeys("QA Engineer");
		    driver.findElement(By.id("txtCompanyName")).click();
		    driver.findElement(By.id("txtCompanyName")).sendKeys("Volly");
		    driver.findElement(By.id("selDivisionRegionBranch")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("selDivisionRegionBranch"));
		      dropdown.findElement(By.xpath("//option[. = 'JourneyBank Home Division 2 : Northeast ( Northeast_crm Boston )']")).click();
		    }
		    assertThat(driver.switchTo().alert().getText(), is("There is a corporate address associated with the branch that you selected, do you wish to update the following fields?\n\nChange Office Street Address 1 to 53 Commerce Way\nChange Office City to Woburn\nChange Office State Code to MA\nChange Office Zip Code to 01801\n\nPress [OK] to change them, or press [Cancel] to leave them unchanged."));
		    driver.switchTo().alert().accept();
		    
		    
		    driver.findElement(By.id("txtEmail")).click();
		    driver.findElement(By.id("txtEmail")).sendKeys("QAtest"+nmonth+nday+nhour+nmin+"@yopmail.com");
		    driver.findElement(By.id("selCorpGroup")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("selCorpGroup"));
		      dropdown.findElement(By.xpath("//option[. = 'Journey Bank']")).click();
		    }
		    driver.findElement(By.id("selCorpGroup")).click();
		    driver.findElement(By.id("txtNMLSCode")).click();
		    driver.findElement(By.id("txtNMLSCode")).sendKeys("123456");
		    driver.findElement(By.id("txtCardLine1")).click();
		    driver.findElement(By.id("txtCardLine1")).sendKeys("Test User");
		    driver.findElement(By.id("txtCardLine2")).click();
		    driver.findElement(By.id("txtCardLine2")).sendKeys("53 Commerce Way");
		    driver.findElement(By.id("txtCardLine3")).click();
		    driver.findElement(By.id("txtCardLine3")).sendKeys("Woburn, Ma. 01801");
		    driver.findElement(By.id("txtCardLine4")).click();
		    driver.findElement(By.id("txtCardLine4")).sendKeys("test");
		    driver.findElement(By.cssSelector("tr:nth-child(6) #btnSave")).click();
		  
		    driver.switchTo().defaultContent();
	
		    
			//click Custom Offering
		    //click-on Admin tab
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
			System.out.println("verify Custom Offering");
			
			driver.findElement(By.cssSelector("#section3 > div")).click();
			try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("//li[contains(.,\'Custom Offering\')]")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("selCategory")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("selCategory"));
		      dropdown.findElement(By.xpath("//option[. = 'Announcements']")).click();
		    }
		    driver.findElement(By.id("selCategory")).click();
		    driver.findElement(By.linkText("Search")).click();

		    try {
		      Thread.sleep(1000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    driver.findElement(By.linkText("Create New Offering")).click();
		    
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("txtOfferingName")).click();
		    driver.findElement(By.id("txtOfferingName")).sendKeys("QAtest"+nmonth+nday+nhour+nmin);
		    
		    //select Category
		    
		    System.out.println("Select Category");
		    
		    {
		        WebElement element = driver.findElement(By.id("selOfferingCategory"));
		        Actions builder = new Actions(driver);
		        builder.moveToElement(element).clickAndHold().perform();
		      }
		      {
		        WebElement element = driver.findElement(By.id("selOfferingCategory"));
		        Actions builder = new Actions(driver);
		        builder.moveToElement(element).perform();
		      }
		      {
		        WebElement element = driver.findElement(By.id("selOfferingCategory"));
		        Actions builder = new Actions(driver);
		        builder.moveToElement(element).release().perform();
		      }
		      driver.findElement(By.id("selOfferingCategory")).click();
		      {
		        WebElement dropdown = driver.findElement(By.id("selOfferingCategory"));
		        dropdown.findElement(By.xpath("//option[. = 'Announcements']")).click();
		      }
		      {
		        WebElement dropdown = driver.findElement(By.id("selOfferingCategory"));
		        dropdown.findElement(By.xpath("//option[. = 'Announcements']")).click();
		      }
		      driver.findElement(By.cssSelector("#selOfferingCategory > option:nth-child(2)")).click();
		      driver.findElement(By.id("selOfferingCategory")).click();
		      {
		    	  WebElement element = driver.findElement(By.id("txtDescription"));
		          Actions builder = new Actions(driver);
		          builder.moveToElement(element).clickAndHold().perform();
		      }
		         
		    {
		      WebElement element = driver.findElement(By.id("txtDescription"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).clickAndHold().perform();
		    }
		    driver.findElement(By.id("txtDescription")).click();
		    driver.findElement(By.id("txtDescription")).sendKeys("testdesc"+nmonth+nday+nhour+nmin);
		    {
		      WebElement element = driver.findElement(By.id("txtEmailSubject"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).clickAndHold().perform();
		    }
		    driver.findElement(By.id("txtEmailSubject")).click();
		    driver.findElement(By.id("txtEmailSubject")).sendKeys("test"+nmonth+nday+nhour+nmin);
		    
		    driver.findElement(By.id("btn2")).click();
		    System.out.println("Custom Offering - Step 2");
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("btnBannerImage0")).click();
		    {
		      WebElement element = driver.findElement(By.id("btn2"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).clickAndHold().perform();
		    }
		    driver.findElement(By.id("btn2")).click();
		    System.out.println("Custom Offering - Step 3");
		    //Step 3
		    //driver.switchTo().frame(2);
		    //driver.findElement(By.cssSelector("p")).click();
		    //{
		      ///WebElement element = driver.findElement(By.id("tinymce"));
		      //js.executeScript("if(arguments[0].contentEditable === 'true') {arguments[0].innerText = '<p>test</p>'}", element);
		    //}
		    //driver.switchTo().defaultContent();
		    
		    {
		      WebElement element = driver.findElement(By.id("btn2"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).release().perform();
		    }
		    driver.findElement(By.id("btn2")).click();
		    System.out.println("Custom Offering - Step 4");
		    //driver.switchTo().frame(3);
		    //Step 4
		    //driver.findElement(By.cssSelector("html")).click();
		    //{
		      //WebElement element = driver.findElement(By.id("tinymce"));
		      //js.executeScript("if(arguments[0].contentEditable === 'true') {arguments[0].innerText = '<p>test</p>'}", element);
		    //}
		    //driver.switchTo().defaultContent();
		    System.out.println("Custom Offering - Step 5");
		    driver.findElement(By.id("btn2")).click();
		    
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    
		    driver.findElement(By.id("btn0")).click();
		    {
		      WebElement element = driver.findElement(By.id("txtProofEmailRecipients"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).clickAndHold().perform();
		    }
		    driver.findElement(By.id("txtProofEmailRecipients")).click();
		    driver.findElement(By.id("txtProofEmailRecipients")).sendKeys("abeck@vollycrm.com");
		    driver.findElement(By.cssSelector("button.btn-primary")).click();
		    //driver.findElement(By.id("txtProofEmailRecipients")).sendKeys("Abeck-CM");
		    {
		      WebElement element = driver.findElement(By.cssSelector(".modal-footer > .btn-primary"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).clickAndHold().perform();
		    }
		    driver.findElement(By.cssSelector(".modal-footer > .btn-primary")).click();
		    {
		      WebElement element = driver.findElement(By.id("btn2"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).clickAndHold().perform();
		    }
		    driver.findElement(By.id("btn2")).click();
		    driver.close();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //new window need as black dashboard
		  //Adding-in Headless Chrome Options
			//ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver = new FirefoxDriver(options);
			//driver.manage().window().maximize();
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> whb = driver.getWindowHandles();
			System.out.println("this is value of wh: " + whb);
			String currentWindowHandleb = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandleb);
		    vars.put(currentWindowHandleb, waitForWindow(2000));
		    
		    
			//click on Dashboard Tiles
		    System.out.println("click-on Admin Dashboard Tiles");
		    
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		 
		    driver.switchTo().frame(0);
		    driver.findElement(By.xpath("//li[contains(.,\'Dashboard Tiles\')]")).click();
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Admin Page - Dashboard colors and styles"));
		    driver.findElement(By.id("styleBackgroundSelect")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("styleBackgroundSelect"));
		      dropdown.findElement(By.xpath("//option[. = 'Black']")).click();
		    }
		    driver.findElement(By.id("styleBackgroundSelect")).click();
		    driver.findElement(By.id("styleHeaderTxtSelect")).click();
		    driver.findElement(By.id("styleRefreshBtn")).click();
		    driver.findElement(By.id("styleSaveBtn")).click();
		    driver.switchTo().defaultContent();
		    
		    //Admin Dashboard
		    System.out.println("click-on Admin Dashboard");
		    
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.switchTo().frame(0);
		    driver.findElement(By.xpath("//li[contains(.,\'Admin Dashboard\')]")).click();
		    driver.switchTo().defaultContent();
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("html")).click();
		    driver.switchTo().defaultContent();
		    driver.findElement(By.xpath("//body/div/div[2]/div/div[2]/button")).click();
		    
		    //Admin Dashboard Settings
		    System.out.println("click-on Admin Dashboard Settings");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.id("le-tilecarousel-settingsdashboardadmin")).click();
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Tile Settings"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("Tile Preview"));
		    driver.switchTo().defaultContent();
		    
		    
		    
		    //Admin Dashboard Tiles
		    System.out.println("click-on Admin Dashboard Tiles");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //driver.switchTo().defaultContent();
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("html")).click();
		    //assertThat(driver.findElement(By.xpath("//*[@id=\"le-tilecarousel-tile0dashboardadmin\"]/div[1]")).getText(), is("LO Activity Report"));
		    assertThat(driver.findElement(By.cssSelector("#le-tilecarousel-tile0dashboardadmin > div:nth-child(1)")).getText(), is("LO Activity Report"));
		    //assertThat(driver.findElement(By.xpath("//*[@id=\"le-tilecarousel-tile1dashboardadmin\"]/div[1]")).getText(), is("Completed Online Surveys"));
		    //assertThat(driver.findElement(By.cssSelector("#le-tilecarousel-tile1dashboardadmin > div:nth-child(1)")).getText(), is("Completed Online Surveys"));
		    //driver.findElement(By.cssSelector(".fa-arrow-circle-right")).click();
		    assertThat(driver.findElement(By.cssSelector("#le-tilecarousel-tile1dashboardadmin > div:nth-child(1)")).getText(), is("New Users"));
		    //driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
		    driver.findElement(By.cssSelector("g:nth-child(5) > path:nth-child(1)")).click();
		    
		    driver.findElement(By.linkText("Division")).click();
		    
		    System.out.println("click-on LO Activity Report");
		    
		    
		    assertThat(driver.findElement(By.cssSelector("caption")).getText(), is("LO Activity Report-ADMIN"));
		    
		    
		    driver.findElement(By.linkText("Admin")).click();
		    //driver.findElement(By.cssSelector(".fa-arrow-circle-right")).click();
		    //driver.findElement(By.cssSelector("html")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    
		    driver.findElement(By.cssSelector(".tile-list-row:nth-child(1) > .tile-list-row-label")).click();
		    System.out.println("click-on New Users to get Corp User Detail");
		    
		    assertThat(driver.findElement(By.cssSelector("tr:nth-child(3) > td > .formTable:nth-child(1) > caption")).getText(), is(" Corp User Detail"));
		    driver.switchTo().defaultContent();
		    
	        //Admin Latest News
		    System.out.println("click-on Admin Latest News");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.xpath("//li[contains(.,\'Latest News\')]")).click();
		    driver.findElement(By.id("StartDt")).click();
		    driver.findElement(By.id("EndDt")).click();
		    driver.findElement(By.id("NarrativeSearchTxt")).click();
		    driver.findElement(By.id("searchBtn")).click();
		    driver.findElement(By.id("addBtn")).click();
		    driver.findElement(By.id("newsTitle")).click();
		    driver.findElement(By.id("newsTitle")).sendKeys("QA News");
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Admin - Latest News"));
		    //driver.findElement(By.cssSelector("#tinymce")).sendKeys("QA News");
		    //Javascript needs to be enabled to enter in JS editor
		    //driver.switchTo().frame(0);
		    //driver.findElement(By.cssSelector("html")).click();
		    //driver.switchTo().defaultContent();
		    //{
		      //WebElement element = driver.findElement(By.cssSelector("#mceu_12 > button:nth-child(1)"));
		      //Actions builder = new Actions(driver);
		      //builder.moveToElement(element).perform();
		    //}
		    //{
		      //WebElement element = driver.findElement(By.tagName("body"));
		      //Actions builder = new Actions(driver);
		      //builder.moveToElement(element, 0, 0).perform();
		    //}
		    //driver.switchTo().frame(0);
		    //{
		      //WebElement element = driver.findElement(By.id("tinymce"));
		      //js.executeScript("if(arguments[0].contentEditable === 'true') {arguments[0].innerText = '<p>test</p>'}", element);
		    //}
		    //driver.switchTo().defaultContent();
		    //driver.findElement(By.cssSelector("#newsModalSave")).click();
		    driver.findElement(By.id("newsModalCancel")).click();
		    driver.switchTo().defaultContent();
		    driver.findElement(By.cssSelector("#divCustomConfirmation2 > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > button:nth-child(1)")).click();
		    driver.switchTo().defaultContent();
		    //driver.findElement(By.cssSelector("#divCustomConfirmation2 .btn-success > .svg-inline--fa")).click();
		    //driver.findElement(By.cssSelector(".headerMenuBarContainer:nth-child(10) > .headerMenuBarButton")).click();
		    
		    //Marketing Store
		    System.out.println("click-on Admin Marketing Store");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.xpath("//li[contains(.,\'Marketing Store\')]")).click();
		    driver.findElement(By.id("saveBtn")).click();
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Marketing Store HTML Admin Page"));
		    driver.switchTo().defaultContent();
		    driver.findElement(By.cssSelector(".headerMenuBarContainer:nth-child(10) > .headerMenuBarButton")).click();
		    //driver.switchTo().frame(0);
		    //driver.switchTo().defaultContent();
		    
		    //Reassignment
		    System.out.println("click-on Admin Reassignment");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("#section2 > div")).click();
		    driver.findElement(By.xpath("//li[contains(.,\'Reassign\')]")).click();
		    assertThat(driver.findElement(By.cssSelector("#div0 b")).getText(), is("Specify Reassignment Type"));
		    
		    //not enabled for CIT
		    /* driver.findElement(By.name("radCustomerCampaignReassignment")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(13) .btn")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(17) .btn:nth-child(2)")).click();
		    driver.findElement(By.name("radStep3")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(15) .btn:nth-child(2)")).click();
		    driver.findElement(By.id("chkSelectDeselectedAllLHS")).click();
		    driver.findElement(By.id("chkStep4Contact_0")).click();
		    driver.findElement(By.id("chkStep4CorpUser_0")).click();
		    driver.findElement(By.id("selReassignmentReason")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("selReassignmentReason"));
		      dropdown.findElement(By.xpath("//option[. = 'Inactive LO']")).click();
		    }
		    driver.findElement(By.id("btnStep4_Next")).click();
		    driver.findElement(By.id("btnStep5LimitReassignments")).click();
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    driver.findElement(By.id("btnStep5Finish")).click();
		    */
		    driver.switchTo().defaultContent();
		    
		    //System.out.println("Tagging fails, skipping Tag Group Priority, Tag Management");
		    //working as Jan 11
		    
		    //Tag Group Priority
		    System.out.println("click-on Admin Tag Group Priority");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("#section2 > div")).click();
		    driver.findElement(By.xpath("//li[contains(.,\'Tag Group Priority\')]")).click();
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Category selections are listed below"));
		    driver.findElement(By.linkText("click here")).click();
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Tag Objects"));
		    driver.switchTo().defaultContent();
		    
		    
		    //Tag Management
		    System.out.println("click-on Admin Tag Management");
		   
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("#section2 > div")).click();
		    driver.findElement(By.xpath("//li[contains(.,\'Tag Management\')]")).click();
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Tag Objects"));
		    driver.findElement(By.id("btnAddTag")).click();
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("selectedTagNameInput")).click();
		    driver.findElement(By.id("selectedTagNameInput")).clear();
		    driver.findElement(By.id("selectedTagNameInput")).sendKeys("Autotest"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("tagGroupsSel")).click();
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    {
		      WebElement dropdown = driver.findElement(By.id("tagGroupsSel"));
		      dropdown.findElement(By.xpath("//option[. = 'Product']")).click();
		    }
		    driver.findElement(By.id("btnSave")).click();
		    driver.findElement(By.linkText("click here")).click();
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Category selections are listed below"));
		    driver.findElement(By.linkText("click here")).click();
		    driver.switchTo().defaultContent();
		    
		    //Tag Inactive
		    System.out.println("click-on Admin Tag Inactive");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("#section2 > div")).click();
		    driver.findElement(By.xpath("//li[contains(.,\'Tag Management\')]")).click();
		    driver.findElement(By.id("btnAddTag")).click();
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Tag Objects"));
		    driver.findElement(By.cssSelector(".tag-clickable > .tag-name")).click();
		    driver.findElement(By.id("btnDisableTag")).click();
		    driver.findElement(By.id("btnSave")).click();
		    
		    driver.switchTo().defaultContent();
		    
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //User Status Email
		    System.out.println("click-on Admin User Status Email");
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[10]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.xpath("//li[contains(.,\'User Status Email\')]")).click();
		    driver.findElement(By.id("selSummaryEmail")).click();
		    driver.findElement(By.id("selSummaryEmail")).click();
		    {
		      WebElement element = driver.findElement(By.cssSelector("#mceu_6 > button:nth-child(1)"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).perform();
		    }
		    //{
		      //WebElement element = driver.findElement(By.tagName("body"));
		      //Actions builder = new Actions(driver);
		      //builder.moveToElement(element, 0, 0).perform();
		    //}
		    driver.findElement(By.linkText("Admin")).click();
		    driver.switchTo().defaultContent();
		    
		    
		    
		    //click-on Logout TAB
		    if (myLog.exists()) {
		    	System.out.println("Log Exists, so opening log for appending.");
		    	//logWriter.close();
		    	FileWriter logWriter = new FileWriter(myLog, true);
		    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
		    logWriter.write(logWithNewLine2);
		    logWriter.close();
		    }
			System.out.println("click-on Logout button - Admin Test PASS");
		    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/a")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }

		    driver.close();
		    driver.quit();
	}
	

		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			//driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}
