package vollygroupid.Mavenjava;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class UATCustomer2 {

	WebDriver driver = null;

    // Function to get day, month, and
    // year from date
    
	private static final String t = null;
	// private WebDriver driver;
	private Map<String, Object> vars = new HashMap<String, Object>();

	public String waitForWindow(int timeout) {
		try {
			Thread.sleep(timeout);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

@Test
//Set Property

    public void test() throws Exception {
	
	String logHeader = "[QA run Test Log]";	
	String testCaseName = "CRM UAT Customer2";	
    System.out.println("Starting Test-Case: " + testCaseName); 
	String logWithNewLine0 = logHeader + System.getProperty("line.separator");
	String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
	LocalDateTime datetime = LocalDateTime.now();
	File myLog = new File("c:\\QATestLog.txt");

	String nuLine = "\r\n";
	//logWriter.write(nuLine);
			
	if (myLog.exists()) {
        System.out.println("Log Exists, so opening log for appending."); 
		//logWriter.close();
		FileWriter logWriter = new FileWriter(myLog, true);
		//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
		// append & write QATestlog.
				//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
				// myWriter.write("Files in Java might be tricky, but it is fun enough!");
				//LocalDateTime datetime = LocalDateTime.now();
				try {
					System.out.println(datetime);
					// logWriter.write(datetime.getHour());
					logWriter.write(logWithNewLine0);
					//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
					//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
					logWriter.write(logWithNewLine1);
					//logWriter.write(nuLine);
					// myWriter.write(dataWithNewLine1);
					// myWriter.write(dataWithNewLine2);
					// myWriter.write(dataWithNewLine3);
					logWriter.close();
					System.out.println("Successfully wrote lines to the log file.");
				} catch (IOException e) {
					System.out.println("An error occurred.");
					e.printStackTrace();
				}
	}
	else {
        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
		FileWriter logWriter = new FileWriter(myLog);
		// append & write QATestlog.
		//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
		// myWriter.write("Files in Java might be tricky, but it is fun enough!");
		//LocalDateTime datetime = LocalDateTime.now();
		try {
			System.out.println(datetime);
			// logWriter.write(datetime.getHour());
			logWriter.write(logWithNewLine0);
			//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
			//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
			logWriter.write(logWithNewLine1);
			// myWriter.write(dataWithNewLine1);
			// myWriter.write(dataWithNewLine2);
			// myWriter.write(dataWithNewLine3);
			logWriter.close();
			System.out.println("Successfully wrote lines to the log file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	  System.out.println("<<Start-Of-Chrome-Test>> \n");
		//System.setProperty(key, value)
		
	    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
	    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();		
		
	  //Adding-in Headless Chrome Options
		ChromeOptions options = new ChromeOptions();
		//options.addArguments("--headless");
		options.addArguments("window-size=1280,800");
	
	
	driver = new ChromeDriver(options);
	driver.get("https://uat.vollycrm.com/site/login.asp");
    //driver.manage().window().setSize(new Dimension(1936, 1056));
    driver.findElement(By.name("UserLogin")).click();
    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
    driver.findElement(By.name("UserPassword")).click();
    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
    driver.findElement(By.name("UserCorp")).click();
    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
    //driver.switchTo().defaultContent();
    
    //click-on Login-button
	System.out.println("click-on Login button");
    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
    //driver.findElement(By.cssSelector(".btn")).click();
    try {
	      Thread.sleep(8000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    
    //click add Customer Leads
    System.out.println("Verify Add New Lead");
    
    //click Home button
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/button")).click();

    //click valid user
    System.out.println("click-on Valid user");
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.switchTo().frame(0);
    driver.findElement(By.cssSelector("#divRecentActivitiesContents0 > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)")).click();
	   
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    
    driver.findElement(By.linkText("Select an action to perform")).click();
    driver.findElement(By.linkText("Add Contact to Opportunity")).click();
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.findElement(By.id("selAddToCampaign")).click();
    {
      WebElement dropdown = driver.findElement(By.id("selAddToCampaign"));
      dropdown.findElement(By.xpath("//option[. = 'Purch-PReferral']")).click();
    }
    driver.findElement(By.cssSelector("#modalAddOpportunity .btn-primary")).click();
    driver.switchTo().defaultContent(); 
    driver.close();
    
	driver = new ChromeDriver(options);
	driver.get("https://uat.vollycrm.com/site/login.asp");
    //driver.manage().window().setSize(new Dimension(1936, 1056));
    driver.findElement(By.name("UserLogin")).click();
    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
    driver.findElement(By.name("UserPassword")).click();
    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
    driver.findElement(By.name("UserCorp")).click();
    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
    //driver.switchTo().defaultContent();
    
    //click-on Login-button
	System.out.println("click-on Login button");
    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
    //driver.findElement(By.cssSelector(".btn")).click();
    try {
	      Thread.sleep(8000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    

    //Add a new loan to customer
    //Click Add Loan
    System.out.println("Verify Add New Loan");
  

    //click Home button
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/button")).click();

    //click valid user
    System.out.println("click-on Valid user");
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.switchTo().frame(0);
    driver.findElement(By.cssSelector("#divRecentActivitiesContents0 > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)")).click();
	   
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    System.out.println("Select action to perform");
    driver.findElement(By.linkText("Select an action to perform")).click();
    driver.findElement(By.xpath("//*[@id=\"contactActionDropdown\"]/li[7]/a")).click();
    //driver.findElement(By.linkText("Add Loan Activity")).click();
    //assertThat(driver.findElement(By.id("h2AddProperty")).getText(), is("Add Loan Activity"));
 
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
   
    
    driver.findElement(By.id("selAddPropertyTransactionType")).click();
    {
      WebElement dropdown = driver.findElement(By.id("selAddPropertyTransactionType"));
      dropdown.findElement(By.xpath("//option[. = 'Cash-Out Refinance (ENC)']")).click();
    }
    driver.findElement(By.cssSelector("#selAddPropertyTransactionType > option:nth-child(2)")).click();
    driver.findElement(By.id("selAddPropertyPropertyType")).click();
    {
      WebElement dropdown = driver.findElement(By.id("selAddPropertyPropertyType"));
      dropdown.findElement(By.xpath("//option[. = '1-UNIT']")).click();
    }
    driver.findElement(By.cssSelector("#selAddPropertyPropertyType > option:nth-child(2)")).click();
    driver.findElement(By.id("txtAddPropertyAddress1")).click();
    driver.findElement(By.id("txtAddPropertyAddress1")).sendKeys("53 commerce way");
    driver.findElement(By.id("txtAddPropertyCity")).click();
    driver.findElement(By.id("txtAddPropertyCity")).sendKeys("woburn");
    driver.findElement(By.id("selAddPropertyState")).click();
    {
        WebElement dropdown = driver.findElement(By.id("selAddPropertyState"));
        dropdown.findElement(By.xpath("//option[. = 'Massachusetts']")).click();
    }
    driver.findElement(By.cssSelector("#selAddPropertyState > option:nth-child(30)")).click();
    driver.findElement(By.cssSelector("#selAddPropertyState > option:nth-child(30)")).click();
   
    
    //driver.findElement(By.xpath("//*[@id=\"modalAddProperty\"]/div[2]/form/div/div")).click();
    
    //driver.findElement(By.cssSelector("#modalAddProperty > div.modal-body.modal-body-full > form > div > div"));
    
    driver.findElement(By.id("txtAddPropertyZipCode")).click();
    driver.findElement(By.id("txtAddPropertyZipCode")).sendKeys("01803");
   
    
    //#txtAddPropertyLoanNumber
    //driver.findElement(By.id("txtAddPropertyLoanNumber")).click();
    //driver.findElement(By.id("txtAddPropertyLoanNumber")).sendKeys("123456");
    //#txtAddPropertyAmount
    driver.findElement(By.id("txtAddPropertyAmount")).click();
    driver.findElement(By.id("txtAddPropertyAmount")).sendKeys("100000");
    //#txtAddPropertyFundingDate
    //driver.findElement(By.id("txtAddPropertyFundingDate")).click();
    //driver.findElement(By.id("txtAddPropertyFundingDate")).click();
    //driver.findElement(By.linkText("1")).click();


    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    driver.findElement(By.cssSelector("#modalAddProperty .btn-primary")).click();
    driver.switchTo().defaultContent();
    driver.close();
    
	driver = new ChromeDriver(options);
	
	
	driver.get("https://uat.vollycrm.com/site/login.asp");
    //driver.manage().window().setSize(new Dimension(1936, 1056));
    driver.findElement(By.name("UserLogin")).click();
    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
    driver.findElement(By.name("UserPassword")).click();
    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
    driver.findElement(By.name("UserCorp")).click();
    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
    //driver.switchTo().defaultContent();
    
    //click-on Login-button
	System.out.println("click-on Login button");
    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
    //driver.findElement(By.cssSelector(".btn")).click();
    try {
	      Thread.sleep(8000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    
    //add Note to Customer
    //click on Notes
    System.out.println("Verify Add Notes");
    
  //click-on Customers TAB
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
    try {
	      Thread.sleep(8000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
  //click recent user
    System.out.println("click-on Recent User");
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/div/div/a[1]")).click();
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
  
    driver.switchTo().frame(0);
   
    
    driver.findElement(By.linkText("Select an action to perform")).click();
    driver.findElement(By.linkText("Add Note")).click();
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    //assertThat(driver.findElement(By.cssSelector("#modalAddNote h2")).getText(), is("Add Note"));
    driver.findElement(By.id("txtAddNoteDetails")).click();
    driver.findElement(By.id("txtAddNoteDetails")).sendKeys("testnote");
   
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.findElement(By.cssSelector("#modalAddNote .btn-primary")).click();
    driver.close();
    
    driver = new ChromeDriver(options);
	//driver.manage().window().maximize();
	
	driver.get("https://uat.vollycrm.com/site/login.asp");
    //driver.manage().window().setSize(new Dimension(1936, 1056));
    driver.findElement(By.name("UserLogin")).click();
    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
    driver.findElement(By.name("UserPassword")).click();
    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
    driver.findElement(By.name("UserCorp")).click();
    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
    //driver.switchTo().defaultContent();
    
    //click-on Login-button
	System.out.println("click-on Login button");
    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
    //driver.findElement(By.cssSelector(".btn")).click();
    try {
	      Thread.sleep(8000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    
    //Add referral to customer
    //click on Add Referral
    System.out.println("Verify Add Referral");
  
    //click Home button
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/button")).click();

    //click valid user
    System.out.println("click-on Valid user");
    
    
    try {
	      Thread.sleep(10000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.switchTo().frame(0);
    driver.findElement(By.cssSelector("#divRecentActivitiesContents0 > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)")).click();
	   
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
   
  
    driver.findElement(By.linkText("Select an action to perform")).click();
    driver.findElement(By.linkText("Add Referral")).click();
  
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    
    //driver.findElement(By.cssSelector(".btn:nth-child(9)")).click();
    //driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[13]/div[2]/div[4]/form/fieldset/div[2]/div/table/tbody/tr/td[1]/a")).click();
    driver.findElement(By.cssSelector(".even > td:nth-child(1) > a:nth-child(1)")).click();
    

    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
   
    driver.findElement(By.linkText("Referrals")).click();
    //assertThat(driver.findElement(By.cssSelector("#referrals td:nth-child(1)")).getText(), is("Referred By"));
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.findElement(By.cssSelector("td > .btn")).click();
    assertThat(driver.switchTo().alert().getText(), is("Are you sure you want to delete this referral?"));
    driver.switchTo().alert().accept();
    driver.switchTo().defaultContent();
    driver.close();
    
    driver = new ChromeDriver(options);
	
	
	driver.get("https://uat.vollycrm.com/site/login.asp");
    //driver.manage().window().setSize(new Dimension(1936, 1056));
    driver.findElement(By.name("UserLogin")).click();
    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
    driver.findElement(By.name("UserPassword")).click();
    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
    driver.findElement(By.name("UserCorp")).click();
    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
    //driver.switchTo().defaultContent();
    
    //click-on Login-button
	System.out.println("click-on Login button");
    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
    //driver.findElement(By.cssSelector(".btn")).click();
    try {
	      Thread.sleep(8000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    
    
    //test Add to Order and Add to Group buttons from main Customer page
    //click Take Action buttons
    System.out.println("click Take Action options, Add to Order and Add to Group");
    
  //click-on Customers TAB
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
    try {
	      Thread.sleep(8000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    
    System.out.println("Verify Add to Order");
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.switchTo().frame(0);
    
  
    driver.findElement(By.xpath("//*[@id=\"mycontroller\"]/table[1]/tbody/tr[1]/td[1]")).click();
    
    //select checkbox
    driver.findElement(By.xpath("/html/body/div[1]/div/div/div[1]/div[2]/table[1]/tbody/tr[2]/td[1]/input")).click();
    driver.findElement(By.cssSelector("#mycontroller > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(7) > div > button.btn.btn-small.dropdown-toggle")).click();
    
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.findElement(By.linkText("Add to Order")).click();
    
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
  
    driver.findElement(By.xpath("/html/body/div[1]/div/div/div[1]/div[4]/div[1]/h1")).click();
    assertThat(driver.findElement(By.cssSelector("#modalAddToOrder h1")).getText(), is("Add to Order"));
    driver.findElement(By.xpath("/html/body/div[1]/div/div/div[1]/div[4]/div[3]/button")).click();
    
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
  
    System.out.println("click-on Add to Group");
    driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[3]/td[7]/div/button[2]")).click();
    driver.findElement(By.linkText("Add to Group")).click();
   
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.findElement(By.xpath("/html/body/div[1]/div/div/div[1]/div[5]/div[2]")).click();
    assertThat(driver.findElement(By.cssSelector("#modalAddToGroup h1")).getText(), is("Add to Group"));
    driver.findElement(By.xpath("/html/body/div[1]/div/div/div[1]/div[5]/div[3]/button")).click();
    driver.close();
    
    driver = new ChromeDriver(options);
	driver.get("https://uat.vollycrm.com/site/login.asp");
    //driver.manage().window().setSize(new Dimension(1936, 1056));
    driver.findElement(By.name("UserLogin")).click();
    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
    driver.findElement(By.name("UserPassword")).click();
    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
    driver.findElement(By.name("UserCorp")).click();
    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
    //driver.switchTo().defaultContent();
    
    //click-on Login-button
	System.out.println("click-on Login button");
    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
    //driver.findElement(By.cssSelector(".btn")).click();
    try {
	      Thread.sleep(8000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    
    
    //click on Create POS loan
    //click customer option to create loan application in Volly POS, window left open for user to confirm POS loan application
    System.out.println("click-on Create POS loan");
    
    //click-on Customers TAB
	System.out.println("click-on Customers TAB");
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
    try {
	      Thread.sleep(8000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    //click recent user
    System.out.println("click-on Recent User");
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/div/div/a[1]")).click();
    
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    driver.switchTo().frame(0);
    
    String ParentHandle = driver.getWindowHandle();
    System.out.println("Parent Window :" + ParentHandle);
    
    driver.findElement(By.linkText("Select an action to perform")).click();
    driver.findElement(By.linkText("Create loan app/ invitation in POS")).click();
    //vars.put("win4841", waitForWindow(2000));
    //{
      //WebElement element = driver.findElement(By.linkText("+ Show Other Information"));
      //Actions builder = new Actions(driver);
      //builder.moveToElement(element).perform();
    //}
    Set AllHandles = driver.getWindowHandles();
    System.out.println("Window handle :" + AllHandles);
    
    File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    FileUtils.copyFile(src,new File("C://Customer2POSCRM.jpg"));
    
    // Move back to Parent Window
    System.out.println("Switching to Parent window - > " + ParentHandle);
    driver.switchTo().window(ParentHandle);
    
   
    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
    
    //API error
    //String alerttext = driver.switchTo().alert().getText();
    //System.out.println(alerttext);
    //driver.switchTo().alert().accept();
    driver.switchTo().defaultContent();
    
    //click-on Logout TAB
    if (myLog.exists()) {
    	System.out.println("Log Exists, so opening log for appending.");
    	//logWriter.close();
    	FileWriter logWriter = new FileWriter(myLog, true);
    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
    logWriter.write(logWithNewLine2);
    logWriter.close();
    }
    System.out.println("click-on Logout button - Customer2 Test PASS");
    
    driver.close();
    driver.quit();
}
   


@AfterTest
public void tearDownTest() {
	// close browser
	//driver.close();
	//driver.quit();
	System.out.println("Test Completed Successfully!");
}

/*
 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
 * driver.quit(); }
 */
}