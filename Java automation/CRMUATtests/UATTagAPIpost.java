package vollygroupid.Mavenjava;

	import org.testng.Assert;

import java.io.BufferedReader;
import java.io.BufferedWriter;

//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;

//import org.json.JSONTokener;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.json.simple.JSONArray;

//import org.junit.Test;
	//import org.junit.After;
	import java.time.Duration;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
    import java.util.Optional;
    import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

//import javax.net.ssl.HttpsURLConnection;

import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;
	//import org.openqa.selenium.devtools.DevTools;
    //import org.openqa.selenium.devtools.v96.network.*;	
    //import org.openqa.selenium.devtools.v96.fetch.Fetch;

    //import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

import org.testng.annotations.AfterTest;


	//static WebDriver driver;

	public class UATTagAPIpost {

		
		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

	    
	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM UAT Tag API post tests";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log does not Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");	
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			
		  //Adding-in Headless Chrome Options
		    boolean headless = true;
		    
			//ChromeOptions options = new ChromeOptions();
			//ChromeDriver driver = new ChromeDriver();
			
		    WebDriver driver = null;
			if (headless) {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--headless");
				options.addArguments("window-size=1920,1080");
				driver = new ChromeDriver(options);
				driver.manage().window().maximize();
			}
			else {
				driver = new ChromeDriver();
				driver.manage().window().maximize();
			}
			
			
			
			//Send token example from roytuts.com
			//https://roytuts.com/httpsurlconnection-rest-api-call-auth-token-and-proxy-server/
			 //POST tests 
			//tests based on https://api.uat.vollycrm.com/tagging/swagger/index.html
			
		    System.out.println("Test CRM UAT Tag POST:\n");
		    System.out.println("1. POST Tag:\n");

		    
			
			
			String token = "Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGFpbSI6IlUyRnNkR1ZrWDErSUxBNmRkWXZFRXJZYWRpSUhCNjdHaktlZlV1YS9MbXdjT0xJU3Vnd2g4eVk5MW5DQTQxUzByYURaV2tNR0JzT09nb1NYZ3N6d3NnTi9nZHdnSGxOTFA3d25lcWZleURPSUZTUGlXRHZFcHVXaFhFWjhnMzNRQVJZTzlQcWJJTWkvUXprVDl0WkJJVjNLQUtLODRqKy9GbXZMU0RNaTQyeDVsVVlCRHpJU21wU2ttTGw1YkIwV3E5NFUrckRjcHBERjFVVm4zTzgxeWJqbUxpNGdwdUY4SlBZR1psK3E2aWQyOTIrTms2U1NHazR3bVBoTlh5Y2luajlKbjJnRFRETHVMRmZCMzl0RnhHdFNTUmZROExydnR0T2piLzJxNFZZM3VPYXFVM0F1T1YvNTZCRGJYRTBuZDZjUUZIL0tjMzd3Lzl4QytrWnYrZjlPVElIUy8rSjR4UytqZFE1MkpFcz0iLCJpYXQiOjE2MzI3Njk1NTMsImV4cCI6MTYzMjc3MzE1M30.jsTuZJUTTQ4iiJt21I8hu43OkEkfjS9SBwh8Weyk7-8";
			
			URL postUrl = new URL("https://api.uat.vollycrm.com/tagging/tag");
			
		    
			HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection();

			// Set POST as request method
			connection.setRequestMethod("POST");

			// Setting Header Parameters
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Authorization", "Bearer " + token);
			connection.setUseCaches(false);
			connection.setDoOutput(true);

			String params ="{\r\n"
					+ "  \"name\": \"Scripttest\",\r\n"
					+ "  \"color\": \"#FFFFFF\",\r\n"
					+ "  \"weight\": 0\r\n"
					+ "}";
			
			BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
			wr.write(params);
			wr.close();

			connection.connect();

			// Getting Response
			int statusCode = connection.getResponseCode();
			String statusResponse = connection.getResponseMessage();
			
			
			
			// Checking http status code for 201 (Created)
			//if (statusCode == HttpURLConnection.HTTP_CREATED) {
				StringBuffer jsonResponseData = new StringBuffer();
				String readLine = null;
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				while ((readLine = bufferedReader.readLine()) != null) {
					jsonResponseData.append(readLine + "\n");
				}

				bufferedReader.close();
				System.out.println(jsonResponseData.toString());

			//} else {
				System.out.println("Status code: "+statusCode+"\n");
				System.out.println("Response: "+statusResponse+"\n\n");
				
			//}
			
			//Test 2
           
		    System.out.println("2. Post tag group:\n");
    
			URL postUrlb = new URL("https://api.uat.vollycrm.com/tagging/group");
			HttpURLConnection connectionb = (HttpURLConnection) postUrlb.openConnection();

			// Set POST as request method
			connectionb.setRequestMethod("POST");

			// Setting Header Parameters
			connectionb.setRequestProperty("Content-Type", "application/json");
			connectionb.setRequestProperty("Authorization", "Bearer " + token);
			connectionb.setUseCaches(false);
			connectionb.setDoOutput(true);
    
			//insert datetime (nday+nmin) object in json
			 Calendar c = Calendar.getInstance();
			    int nmonth = c.get(Calendar.MONTH);
			    int nday = c.get(Calendar.DAY_OF_MONTH);
			    int nhour = c.get(Calendar.HOUR_OF_DAY);
			    int nmin = c.get(Calendar.MINUTE);
			    
			    nmonth = nmonth + 1;
			    
			    String nstamp = (nmonth + "" + nday + "" + nhour + "" + nmin);
			
            String paramsb="{\r\n"
            		+ "  \"name\":\"Autotest\",\r\n"
            		+ "  \"description\": \"automatedtest\",\r\n"
            		+ "  \"defaultPriority\": 0\r\n"
            		+ "}";
            
            
            String newvar = String.format("Autotest%s",nstamp); 
            
            String paramsbr = paramsb.replaceAll("Autotest",newvar);
             
	
			BufferedWriter wrb = new BufferedWriter(new OutputStreamWriter(connectionb.getOutputStream(), "UTF-8"));
			wrb.write(paramsbr);
			wrb.close();

			System.out.println("Replaced string:"+paramsbr);
			connectionb.connect();

			// Getting Response
			int statusCodeb = connectionb.getResponseCode();
			String statusResponseb = connectionb.getResponseMessage();
		
			// Checking http status code for 201 (Created)
			//if (statusCodeb == HttpURLConnection.HTTP_CREATED) {
				StringBuffer jsonResponseDatab = new StringBuffer();
				String readLineb = null;
				BufferedReader bufferedReaderb = new BufferedReader(new InputStreamReader(connectionb.getInputStream()));

				while ((readLineb = bufferedReaderb.readLine()) != null) {
					jsonResponseDatab.append(readLineb + "\n");
					
					
				}
				

				bufferedReaderb.close();
				System.out.println(jsonResponseDatab.toString());

			//} else {
				System.out.println("Status code: "+statusCodeb+"\n");
				System.out.println("Response: "+statusResponseb+"\n\n");
	
			//}
	       
			//Test 3
            System.out.println("3. POST tag group app key:\n");
		    
			URL postUrlc = new URL("https://api.uat.vollycrm.com/tagging/tagGroupAppKey");
			HttpURLConnection connectionc = (HttpURLConnection) postUrlc.openConnection();

			// Set POST as request method
			connectionc.setRequestMethod("POST");

			// Setting Header Parameters
			connectionc.setRequestProperty("Content-Type", "application/json");
			connectionc.setRequestProperty("Authorization", "Bearer " + token);
			connectionc.setUseCaches(false);
			connectionc.setDoOutput(true);

            String paramsc = "{\r\n"
            		+ "  \"appKey\": \"null\",\r\n"
            		+ "  \"tagGroupID\": 7,\r\n"
            		+ "  \"priority\": 0\r\n"
            		+ "}";
			
			BufferedWriter wrc = new BufferedWriter(new OutputStreamWriter(connectionc.getOutputStream(), "UTF-8"));
			wrc.write(paramsc);
			wrc.close();

			System.out.println("String:"+paramsc);
			connectionc.connect();

			// Getting Response
			int statusCodec = connectionc.getResponseCode();
			String statusResponsec = connectionc.getResponseMessage();
		
			// Checking http status code for 201 (Created)
			//if (statusCodec == HttpURLConnection.HTTP_CREATED) {
				StringBuffer jsonResponseDatac = new StringBuffer();
				String readLinec = null;
				BufferedReader bufferedReaderc = new BufferedReader(new InputStreamReader(connectionc.getInputStream()));

				while ((readLinec = bufferedReaderc.readLine()) != null) {
					jsonResponseDatac.append(readLinec + "\n");
				}

				bufferedReaderc.close();
				System.out.println(jsonResponseDatac.toString());

			//} else {
				System.out.println("Status code: "+statusCodec+"\n");
				System.out.println("Response: "+statusResponsec+"\n\n");
	        
			//Test 4
			//POST map/{appkey}
				System.out.println("4. POST map/{appKey}:\n");
			    
				URL postUrld = new URL("https://api.uat.vollycrm.com/tagging/map/CMJYB2");
				HttpURLConnection connectiond = (HttpURLConnection) postUrld.openConnection();

				// Set POST as request method
				connectiond.setRequestMethod("POST");

				// Setting Header Parameters
				connectiond.setRequestProperty("Content-Type", "application/json");
				connectiond.setRequestProperty("Authorization", "Bearer " + token);
				connectiond.setUseCaches(false);
				connectiond.setDoOutput(true);

	            String paramsd = "[{\r\n"
	            		+ "    \"tagId\": 7,\r\n"
	            		+ "    \"appKey\": \"Autotest\",\r\n"
	            		+ "    \"color\": \"#FFFFF\",\r\n"
	            		+ "    \"weight\": 0,\r\n"
	            		+ "    \"created\": \"2022-02-18T18:59:54.181Z\"\r\n"
	            		+ "  }]";
				
	            String paramsdr = paramsd.replaceAll("Autotest",newvar);
	            
				BufferedWriter wrd = new BufferedWriter(new OutputStreamWriter(connectiond.getOutputStream(), "UTF-8"));
				wrd.write(paramsdr);
				wrd.close();

				System.out.println("Replaced string:"+paramsdr);
				connectiond.connect();

				// Getting Response
				int statusCoded = connectiond.getResponseCode();
				String statusResponsed = connectiond.getResponseMessage();
			
		
				StringBuffer jsonResponseDatad = new StringBuffer();
				String readLined = null;
				BufferedReader bufferedReaderd = new BufferedReader(new InputStreamReader(connectiond.getInputStream()));

					while ((readLined = bufferedReaderd.readLine()) != null) {
						jsonResponseDatad.append(readLined + "\n");
					}

					bufferedReaderd.close();
					System.out.println(jsonResponseDatad.toString());

				//} else {
					System.out.println("Status code: "+statusCoded+"\n");
					System.out.println("Response: "+statusResponsed+"\n\n");	
				
				//Test 5
				//POST map/{appkey}/{objtype}/{objid}
					System.out.println("POST map/{appKey}/{objType}/{objId}:\n");
				    
					URL postUrle = new URL("https://api.uat.vollycrm.com/tagging/map/CMJYB2/Contact/10006395");
					HttpURLConnection connectione = (HttpURLConnection) postUrle.openConnection();

					// Set POST as request method
					connectione.setRequestMethod("POST");

					// Setting Header Parameters
					connectione.setRequestProperty("Content-Type", "application/json");
					connectione.setRequestProperty("Authorization", "Bearer " + token);
					connectione.setUseCaches(false);
					connectione.setDoOutput(true);

		            String paramse = "[\r\n"
		            		+ "  7\r\n"
		            		+ "]";
					
					BufferedWriter wre = new BufferedWriter(new OutputStreamWriter(connectione.getOutputStream(), "UTF-8"));
					wre.write(paramse);
					wre.close();

					System.out.println("String:"+paramse);
					connectiond.connect();

					// Getting Response
					int statusCodee = connectione.getResponseCode();
					String statusResponsee = connectione.getResponseMessage();
				
					// Checking http status code for 201 (Created)
					//if (statusCodec == HttpURLConnection.HTTP_CREATED) {
						StringBuffer jsonResponseDatae = new StringBuffer();
						String readLinee = null;
						BufferedReader bufferedReadere = new BufferedReader(new InputStreamReader(connectione.getInputStream()));

						while ((readLinee = bufferedReadere.readLine()) != null) {
							jsonResponseDatae.append(readLinee + "\n");
						}

						bufferedReadere.close();
						System.out.println(jsonResponseDatae.toString());

					//} else {
						System.out.println("Status code: "+statusCodee+"\n");
						System.out.println("Response: "+statusResponsee+"\n\n");	
					
					
				/*
						//Test 6
				//POST map/tag/{tagid}/{appkey}/{objType}
						System.out.println("POST map/tag/{appKey}/{objType}:\n");
					    
						URL postUrlf = new URL("https://api.uat.vollycrm.com/tagging/map/tag/70/CMJYB/test");
						HttpURLConnection connectionf = (HttpURLConnection) postUrlf.openConnection();

						// Set POST as request method
						connectionf.setRequestMethod("POST");

						// Setting Header Parameters
						connectionf.setRequestProperty("Content-Type", "application/json");
						connectionf.setRequestProperty("Authorization", "Bearer " + token);
						connectionf.setUseCaches(false);
						connectionf.setDoOutput(true);

			            String paramsf = "[\r\n"
			            		+ "  \"string\"\r\n"
			            		+ "]";
						
			            //String postUrlfr = postUrlf.replaceAll("7",newvar);
			            
						BufferedWriter wrf = new BufferedWriter(new OutputStreamWriter(connectionf.getOutputStream(), "UTF-8"));
						wrf.write(paramsf);
						wrf.close();

						System.out.println("String:\n"+paramsf);
						connectionf.connect();

						// Getting Response
						int statusCodef = connectionf.getResponseCode();
						String statusResponsef = connectionf.getResponseMessage();
					
						// Checking http status code for 201 (Created)
						//if (statusCodec == HttpURLConnection.HTTP_CREATED) {
							StringBuffer jsonResponseDataf = new StringBuffer();
							String readLinef = null;
							BufferedReader bufferedReaderf = new BufferedReader(new InputStreamReader(connectionf.getInputStream()));

							while ((readLinef = bufferedReaderf.readLine()) != null) {
								jsonResponseDataf.append(readLinef + "\n");
							}

							bufferedReaderf.close();
							System.out.println(jsonResponseDataf.toString());

						//} else {
							System.out.println("Status code: "+statusCodef+"\n");
							System.out.println("Response: "+statusResponsef+"\n\n");	
				*/		
						
	
	System.out.println("Tag API POST tests PASS");
    
    if (myLog.exists()) {
    	System.out.println("Log Exists, so opening log for appending.");
    	//logWriter.close();
    	FileWriter logWriter = new FileWriter(myLog, true);
    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
    logWriter.write(logWithNewLine2);
    logWriter.close();
    }
    
    driver.close();
    driver.quit();

	}
		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			//driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		
	}
