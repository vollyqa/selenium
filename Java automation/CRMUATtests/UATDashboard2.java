package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Before;
	//import org.junit.Test;
	import org.junit.After;
	import static org.junit.Assert.*;
	import static org.hamcrest.CoreMatchers.is;
	import static org.hamcrest.core.IsNot.not;
	import java.time.Duration;
    import java.time.LocalDate;
    import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
    import java.util.Date;
    import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;
	import org.openqa.selenium.Alert;
	import org.openqa.selenium.Keys;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.openqa.selenium.JavascriptExecutor;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class UATDashboard2 {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM UAT Dashboard2";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			//WebDriver driver = new ChromeDriver();		
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> wh = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandle = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    vars.put(currentWindowHandle, waitForWindow(2000));

            //continued from Dashboard tests
		    //click-on MyLinks
		    
			System.out.println("verify Dashboard My Links");
		    //driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/button")).click();
		    
		    try {
		      Thread.sleep(10000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    
		    driver.switchTo().defaultContent();
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector(".fa-arrow-circle-right")).click();
		    //driver.findElement(By.cssSelector(".fa-arrow-circle-right")).click();
		    assertThat(driver.findElement(By.cssSelector("#le-tilecarousel-tile2db > .tile-header")).getText(), is("My Links"));
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    
		    
		    //verify Order Summary
		    System.out.println("Verify Dashboard Order Summary");
		    
		   
		    //driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
		    driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
		    driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
		    
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    
		    driver.switchTo().defaultContent();
		    driver.switchTo().frame(0);
		    
		    driver.findElement(By.cssSelector("#le-tilecarousel-tile0db")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //driver.findElement(By.cssSelector("#le-tilecarousel-tile0db")).click();
		    //driver.findElement(By.xpath("/html/body/div/div[1]/div/div[1]/div[3]/div[2]/div[3]/div[1]/div[1]/div/svg/g[4]")).click();
		    
		    driver.findElement(By.id("StartDttm")).click();
		    driver.findElement(By.id("StartDttm")).sendKeys("1/1/2011");
		    driver.findElement(By.id("EndDttm")).click();
		    driver.findElement(By.id("EndDttm")).sendKeys("8/15/2021");
		    driver.findElement(By.id("order-summary-search-btn")).click();
		    
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().defaultContent();
		    driver.findElement(By.xpath("//button[@onclick=\'document.getElementById(\"masterIframe\").src=\"Dashboardv2.asp\"\']")).click();
		    driver.switchTo().frame(0);
		    //driver.findElement(By.cssSelector(".fa-arrow-circle-right")).click();
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    driver.switchTo().defaultContent();
		    
		    //verify Program Order Summary
		    System.out.println("Verify Program Order Summary");
		    
		    driver.switchTo().frame(0);
		    
		    try {
		        Thread.sleep(5000);
		      } catch (InterruptedException e) {
		        e.printStackTrace();
		      }
		    
		      assertThat(driver.findElement(By.cssSelector("#le-tilecarousel-tile2db > .tile-header")).getText(), is("Program Order Summary"));
		      driver.findElement(By.linkText("Contacts")).click();
		      driver.findElement(By.linkText("AutoApprove")).click();
		      driver.findElement(By.linkText("Orders")).click();
		      //driver.switchTo().defaultContent(); 
		    
			
			  
			    Calendar c = Calendar.getInstance();
			    int nmonth = c.get(Calendar.MONTH);
			    int nday = c.get(Calendar.DAY_OF_MONTH);
			    int nhour = c.get(Calendar.HOUR_OF_DAY);
			    int nmin = c.get(Calendar.MINUTE);
			    nmonth=nmonth+1;
			    
		      //verify Quick Action
		      System.out.println("Verify Quick Action Tile / Add New Contact");
		      
		      //driver.switchTo().defaultContent();
		      driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
			  driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
			  driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
			  
			    try {
			        Thread.sleep(5000);
			      } catch (InterruptedException e) {
			        e.printStackTrace();
			      }
		      
			  driver.findElement(By.cssSelector("div.tile-quickaction-row:nth-child(1) > a:nth-child(1) > div:nth-child(2)")).click();
		      driver.switchTo().defaultContent();
		      assertThat(driver.findElement(By.cssSelector(".contact-modal .site-modal-header-text")).getText(), is("Add a New Contact"));
		      driver.findElement(By.id("txtFirstName")).click();
		      driver.findElement(By.id("txtFirstName")).sendKeys("Test");
		      driver.findElement(By.cssSelector(".contact-modal tr:nth-child(5) > td:nth-child(2)")).click();
		      driver.findElement(By.id("txtLastName")).click();
		      driver.findElement(By.id("txtLastName")).sendKeys("QuickAction"+nmonth+nday+nhour+nmin);
		      driver.findElement(By.id("txtEmailAddress")).click();
		      driver.findElement(By.id("txtEmailAddress")).sendKeys("QuickAction"+nmonth+nday+nhour+nmin+"@yopmail.com");
		      driver.findElement(By.cssSelector(".text-right > .btn-success:nth-child(1)")).click();
		      driver.switchTo().frame(0);
		      {
		        List<WebElement> elements = driver.findElements(By.cssSelector(".tile-quickaction-row:nth-child(2) > .tile-quickaction-row-item:nth-child(1) > .tile-quickaction-txt"));
		        assert(elements.size() > 0);
		      }
		      try {
		          Thread.sleep(10000);
		        } catch (InterruptedException e) {
		          e.printStackTrace();
		        }
		        System.out.println("Verify Quick Action Tile / Census Data Lookup");
		        driver.findElement(By.cssSelector(".tile-quickaction-row:nth-child(3) > .tile-quickaction-row-item:nth-child(1) > .tile-quickaction-txt")).click();
		        driver.switchTo().defaultContent();
		        assertThat(driver.findElement(By.cssSelector(".census-modal .site-modal-header-text")).getText(), is("Census Data Lookup"));
		        driver.findElement(By.id("txtCensusDataAddress")).click();
		        driver.findElement(By.id("txtCensusDataAddress")).sendKeys("53 Commerce Way, Woburn, Ma.");
		        driver.findElement(By.cssSelector(".btn-primary")).click();
		        try {
			          Thread.sleep(5000);
			        } catch (InterruptedException e) {
			          e.printStackTrace();
			        }
		        
		        driver.findElement(By.cssSelector(".census-modal > .site-modal-footer > .btn")).click();
		        driver.switchTo().frame(0);
		        System.out.println("Verify Quick Action Tile / Marketing Store");
		        driver.findElement(By.cssSelector(".tile-quickaction-row:nth-child(1) > .tile-quickaction-row-item-reverse > .tile-quickaction-txt")).click();
		        try {
			          Thread.sleep(10000);
			        } catch (InterruptedException e) {
			          e.printStackTrace();
			        }
		        assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Marketing Store"));
		        driver.findElement(By.cssSelector("#sectionBody2 > ul:nth-child(1) > li:nth-child(1)")).click();
		        assertThat(driver.findElement(By.cssSelector("#offeringCategoryName")).getText(), is("New Customer"));
		        driver.switchTo().defaultContent();
		        driver.findElement(By.xpath("//button[@onclick=\'document.getElementById(\"masterIframe\").src=\"Dashboardv2.asp\"\']")).click();
		        driver.switchTo().frame(0);
		        try {
			          Thread.sleep(10000);
			        } catch (InterruptedException e) {
			          e.printStackTrace();
			        }
		        
		        System.out.println("Verify Quick Action Tile / Upload Contacts");
		        driver.findElement(By.cssSelector("#tile-body1db > div:nth-child(2) > a.tile-quickaction-row-item.tile-quickaction-row-item-reverse > div")).click();
		        
		        //driver.findElement(By.id("divContent0")).click();
		        try {
			          Thread.sleep(10000);
			        } catch (InterruptedException e) {
			          e.printStackTrace();
			        }
		        assertThat(driver.findElement(By.cssSelector("#listUploadStep1 > .upload-title")).getText(), is("Upload Contacts"));
		        driver.switchTo().defaultContent();
		        
		        
		        //delete Quickaction customer
		        System.out.println("Delete Quick Action user");

		      //click-on Customers TAB
				System.out.println("click-on Customers TAB");
			    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
			    try {
				      Thread.sleep(5000);
				    } catch (InterruptedException e) {
				      e.printStackTrace();
				    }
			    
			    //click recent user
			    System.out.println("click-on Recent User");
			    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/div/div/a[1]")).click();
			    
			    
			    try {
				      Thread.sleep(5000);
				    } catch (InterruptedException e) {
				      e.printStackTrace();
				    }
			    
			    driver.switchTo().frame(0);
			    
			    //remove customer, error that customer cannot be removed should appear
			    //click on delete Customer
			    System.out.println("click-on Delete Customer");
			    
			   
			    driver.findElement(By.linkText("Select an action to perform")).click();
			    driver.findElement(By.linkText("Delete Contact")).click();
			    assertThat(driver.switchTo().alert().getText(), is("Are you sure you want to delete this contact?"));
			    driver.switchTo().alert().accept();
			    driver.switchTo().defaultContent();
			    
			    
			  //click-on Home TAB
				System.out.println("click-on Home TAB");
			    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/button")).click();
			    
		        //verify Recent Activities
		        System.out.println("Verify Recent Activities");
		        
		        try {
				      Thread.sleep(5000);
				    } 
			    catch (InterruptedException e) {
				      e.printStackTrace();
				    }
		        
		        driver.switchTo().frame(0);
		        driver.findElement(By.cssSelector(".dashboard-row-3-cell:nth-child(2) .dashboard-row-3-cell-header")).click();
		        assertThat(driver.findElement(By.cssSelector(".dashboard-row-3-cell:nth-child(2) .dashboard-row-3-cell-header")).getText(), is("Recent Activities"));
		        driver.findElement(By.id("p-recenttab2")).click();
		        assertThat(driver.findElement(By.id("p-recenttab2")).getText(), is("Loans"));
		        driver.findElement(By.id("p-recenttab3")).click();
		        assertThat(driver.findElement(By.id("p-recenttab3")).getText(), is("Reports"));
		        driver.findElement(By.id("p-recenttab1")).click();
		        assertThat(driver.findElement(By.id("p-recenttab1")).getText(), is("Contacts"));
		        driver.switchTo().defaultContent();
		    
		    //click-on Logout TAB
		        if (myLog.exists()) {
			    	System.out.println("Log Exists, so opening log for appending.");
			    	//logWriter.close();
			    	FileWriter logWriter = new FileWriter(myLog, true);
			    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
			    logWriter.write(logWithNewLine2);
			    logWriter.close();
			    }
				System.out.println("click-on Logout button - Dashboard2 Test PASS");
			    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/a")).click();
			    try {
				      Thread.sleep(5000);
				    } 
			    catch (InterruptedException e) {
				      e.printStackTrace();
				    }
			  driver.close();
			  driver.quit();
	}
			@AfterTest
			public void tearDownTest() {
				// close browser
				//driver.close();
				//driver.quit();
				System.out.println("Test Completed Successfully!");
			}

			/*
			 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
			 * driver.quit(); }
			 */
		}