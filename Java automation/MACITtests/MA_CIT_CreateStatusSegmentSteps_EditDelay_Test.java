package MA_CIT_Tests.MA_CIT_Tests;
//import org.testng.Assert;
//import com.sun.org.apache.bcel.internal.generic.Select;
//below libraries from selenium-IDE

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
//import org.junit.Test;
//import org.junit.After;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

//import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

//static WebDriver driver;

public class MA_CIT_CreateStatusSegmentSteps_EditDelay_Test {
	
	  WebDriver driver = null;
	  String testStatus = null;	
	  private static final String t = null;
	//private WebDriver driver;
	  private Map<String, Object>  vars = new HashMap<String, Object>();
	  
	  public String waitForWindow(int timeout) {
		    try {
		      Thread.sleep(timeout);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
			return null;
	  }

	  //JavascriptExecutor js;
	  
	   // js = (JavascriptExecutor) driver;
	   
	    
//	public static void main(String[] args) {
	
	/*
	 * @BeforeTest public void setUp() throws Exception {
	 * System.setProperty("webdriver.chrome.driver",
	 * "C:\\Users\\acronin\\eclipse\\chromedriver.exe"); WebDriver driver = new
	 * ChromeDriver(); }
	 */	
@Test
//Set Property

    public void test() throws Exception {
	
	String logHeader = "[QA run Test Log]";
	String testCaseName = "[MA_CIT_CreateStatusSegmentStepsTest_EditDelay_Test.java]";		
    System.out.println("Starting Test-Case: " + testCaseName); 
	String logWithNewLine0 = logHeader + System.getProperty("line.separator");
	String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
	LocalDateTime datetime = LocalDateTime.now();
	File myLog = new File("c:\\QATestLog.txt");

	String nuLine = "\r\n";
	//logWriter.write(nuLine);
			
	if (myLog.exists()) {
        System.out.println("Log Exists, so opening log for appending."); 
		//logWriter.close();
		FileWriter logWriter = new FileWriter(myLog, true);
		//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
		// append & write QATestlog.
				//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
				// myWriter.write("Files in Java might be tricky, but it is fun enough!");
				//LocalDateTime datetime = LocalDateTime.now();
				try {
					System.out.println(datetime);
					// logWriter.write(datetime.getHour());
					logWriter.write(System.getProperty("line.separator"));
					logWriter.write(logWithNewLine0);
					logWriter.write(datetime.toString() + System.getProperty("line.separator"));
					//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
					logWriter.write(logWithNewLine1);
					//logWriter.write(nuLine);
					// myWriter.write(dataWithNewLine1);
					// myWriter.write(dataWithNewLine2);
					// myWriter.write(dataWithNewLine3);
					logWriter.close();
					System.out.println("Successfully wrote lines to the log file.");
				} catch (IOException e) {
					System.out.println("An error occurred.");
					e.printStackTrace();
				}
	}
	else {
        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
		FileWriter logWriter = new FileWriter(myLog);
		// append & write QATestlog.
		//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
		// myWriter.write("Files in Java might be tricky, but it is fun enough!");
		//LocalDateTime datetime = LocalDateTime.now();
		try {
			System.out.println(datetime);
			// logWriter.write(datetime.getHour());
			logWriter.write(System.getProperty("line.separator"));
			logWriter.write(logWithNewLine0);
			logWriter.write(datetime.toString() + System.getProperty("line.separator"));
			//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
			logWriter.write(logWithNewLine1);
			// myWriter.write(dataWithNewLine1);
			// myWriter.write(dataWithNewLine2);
			// myWriter.write(dataWithNewLine3);
			logWriter.close();
			System.out.println("Successfully wrote lines to the log file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	  System.out.println("<<Start-Of-Chrome-Test>> \n");
		//System.setProperty(key, value)
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\acronin\\eclipse\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();		
		
		//Adding-in Headless Chrome Options
				
				boolean headless = true;
				//boolean headless = false;
				//ChromeOptions options = new ChromeOptions();
				//WebDriver driver = new ChromeDriver(options);
				//options.addArguments("--headless");
				WebDriver driver = null;
				if (headless) {
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--headless");
					options.addArguments("window-size=1920,1080");
					driver = new ChromeDriver(options);
					driver.manage().window().maximize();
				}
				else {
					driver = new ChromeDriver();
					driver.manage().window().maximize();
				}
		
		//ChromeOptions options = new ChromeOptions();
		//options.addArguments("--headless");
		//options.addArguments("window-size=1280,800");
		//WebDriver driver = new ChromeDriver(options);
		//driver = new ChromeDriver(options);
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//driver.manage().window().maximize();
		driver.get("https://app.cit.vollyma.com/login");
	    //driver.manage().window().setSize(new Dimension(1936, 1056));
	    driver.findElement(By.name("username")).click();
	    //driver.findElement(By.name("username")).sendKeys("acronin@myvolly.com");
	    driver.findElement(By.name("username")).sendKeys("acronin@yopmail.com");
	    driver.findElement(By.name("password")).click();
	    driver.findElement(By.name("password")).sendKeys("Kaitlin1$");
	    //driver.findElement(By.cssSelector(".MuiButton-label")).click();
	    //<button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
	    //driver.findElement(By.className("btn btn-lg btn-primary btn-block")).click();
	    driver.findElement(By.cssSelector(".btn")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    Set<String> wh = driver.getWindowHandles();
		System.out.println("this is value of wh: " + wh);
		String currentWindowHandle = driver.getWindowHandle();
		System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
	    vars.put(currentWindowHandle, waitForWindow(2000));
	    //vars.put("window_handles", driver.getWindowHandles());
		//vars.put("window_handles", wh);
		//driver.findElement(By.cssSelector("./clients")).click();
	    driver.findElement(By.xpath("//*[@id=\"wrapper\"]/nav/div/div[2]/ul[2]/li[4]/a")).click();	    
		//driver.findElement(By.className("fa fa-fw fa-suitcase")).click();
	    driver.findElement(By.linkText("Journey Bank 2")).click();
	    //vars.put(key, value)
		System.out.println("Just Logged-IN-To MA-CIT... \n");
		
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
	    for (String handle1 : driver.getWindowHandles()) {
			System.out.println("this is window handle-1 below...");
	        System.out.println(handle1); 
			System.out.println("switching to this window handle...");	        
	        driver.switchTo().window(handle1);
	        //driver.switchTo().window(handle1.get(1)); 
	    }
	    //adding new TAB
	    System.out.println("Next, adding a new TAB... \n");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    //driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"\t");
	    //driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
	    WebElement link= driver.findElement(By.tagName("a"));  
	    String keyString =   (Keys.CONTROL+Keys.SHIFT.toString()+Keys.ENTER.toString());
	    link.sendKeys(keyString);
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	

	    //driver.switchTo().newWindow();
	    System.out.println("trying to open a new TAB in Chrome browser...\n");
	    
	 // Store all currently open tabs in tabs
	    //((JavascriptExecutor)driver).executeScript("window.open()");
	    //driver.getWindowHandles().new
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(1)); //switches to new tab
		System.out.println("this is window tabs handle below...");
        System.out.println(tabs);
	    // Click on link to open in new tab
	    //driver.findElement(By.id("Url_Link")).click();
	    
	    // Switch newly open Tab
	    driver.switchTo().window(tabs.get(1));
	    driver.get("http://google.com");
	    System.out.println("this is window tabs handle again, below...");
        System.out.println(tabs);
	    //driver.switchTo().window(tabs.get(-1));
	    //driver.switchTo().window(tabs.get(1));
        //driver.switchTo().window(handle1..get(1)); 
	    //https://app.cit.vollyma.com/reports/dashboard
	    //https://app.cit.vollyma.com/manage/statuses
	    //https://app.cit.vollyma.com/manage/attributes
	    //https://app.cit.vollyma.com/manage/statuses
//		driver.get("https://app.cit.vollyma.com/reports/dashboard");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
	    
		driver.get("https://app.cit.vollyma.com/manage/statuses");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
		
		driver.get("https://app.cit.vollyma.com/manage/attributes");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
		
		driver.get("https://app.cit.vollyma.com/manage/statuses");	
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
		
		System.out.println("Just opened statuses... \n");		
	    //driver.findElement(By.className("fa fa-exclamation-triangle fa-5x")).click();
		//driver.findElement(By.cssSelector(".fa-chevron-down:nth-child(1)")).click();
	    //driver.findElement(By.cssSelector(".accordion:nth-child(1) a:nth-child(3) > div")).click();
	    //driver.findElement(By.linkText("Manage")).click();
	    //driver.findElement(By.linkText("Attributes")).click();
	    //driver.findElement(By.linkText("Manage")).click();
	    //driver.findElement(By.linkText("Statuses")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		
			/*
			 * vars.put("win3853", waitForWindow(2000));
			 * //driver.findElement(By.cssSelector(".dropdown-sv:nth-child(2) .caret")).
			 * click(); driver.findElement(By.xpath(
			 * "//*[@id=\"header\"]/div/div[1]/div/div[2]/ul/li[2]/a/b")).click();
			 * driver.findElement(By.linkText("Statuses")).click();
			 * driver.findElement(By.linkText("Statuses")).click();
			 * driver.findElement(By.cssSelector(".pull-right:nth-child(2)")).click();
			 * driver.findElement(By.name("name")).click();
			 * driver.findElement(By.name("name")).sendKeys("MAstatus6"); try {
			 * Thread.sleep(3000); } catch (InterruptedException e) { e.printStackTrace(); }
			 * driver.findElement(By.cssSelector(".btn-primary")).click(); try {
			 * Thread.sleep(3000); } catch (InterruptedException e) { e.printStackTrace(); }
			 * driver.findElement(By.cssSelector(".btn-default")).click(); try {
			 * Thread.sleep(3000); } catch (InterruptedException e) { e.printStackTrace(); }
			 */
	    
	    //setting-up time value
	    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	    //calendar.clear();
	    //calendar.set(2011, Calendar.OCTOBER, 1);
	    long secondsSinceEpoch = calendar.getTimeInMillis() / 1000L;
	    String myStatusName = "MyMAstatus" + (secondsSinceEpoch);
	    String mySegmentName = "MyMAsegment" + (secondsSinceEpoch);
	
		String logWithNewLine2 = myStatusName + System.getProperty("line.separator");
		String logWithNewLine3 = mySegmentName + System.getProperty("line.separator");	
		String logWithNewLineOnly = System.getProperty("line.separator");	

		System.out.println("Opening log for appending."); 
		FileWriter logWriter = new FileWriter(myLog, true);
		//logWriter.write(logWithNewLine2 + System.getProperty("line.separator"));
		logWriter.write(logWithNewLine2);
		//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
		//logWriter.write(logWithNewLine3 + System.getProperty("line.separator"));
		logWriter.write(logWithNewLine3);
		//logWriter.write(nuLine);
		logWriter.write(logWithNewLineOnly);
		logWriter.close();
		
		
		System.out.println("Just got past selecting Statuses in drop-down... \n");
		System.out.println("Now creating a new Status in drop-down... \n");	
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    driver.findElement(By.cssSelector(".fa-plus")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    driver.findElement(By.name("name")).click();
	    try {
	      Thread.sleep(3000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
	    driver.findElement(By.name("name")).click();
	    //driver.findElement(By.name("name")).sendKeys("MyMAstatus20");
	    driver.findElement(By.name("name")).sendKeys(myStatusName);
	    
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		System.out.println("clicking on Save button next... \n");	    
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		System.out.println("clicking on Close button next... \n");		    
	    driver.findElement(By.cssSelector(".btn-default")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    //add in segments & steps.
		System.out.println("adding in segments & steps now.... \n");
		System.out.println("clicking the (+) segment button now.... \n");
	    driver.findElement(By.cssSelector(".segment-bgcolor > .fa-plus")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    System.out.println("clicking-in the segment name field now.... \n");
	    driver.findElement(By.name("name")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    System.out.println("Adding the Segment Name.");
//	    driver.findElement(By.name("name")).sendKeys("MAsegment20");
	    driver.findElement(By.name("name")).sendKeys(mySegmentName);
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    System.out.println("Selecting the Campaign Options");
	    // Campaign Options
	    // body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-body > dl > div:nth-child(1) > dt > a:nth-child(1)
	    //  /html/body/div[1]/div/div/form/div[2]/dl/div[1]/dt/a[1]
	    //		/html/body/div[1]/div/div/form/div[2]/dl/div[1]/dt/a[1]
	    //				document.querySelector("body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-body > dl > div:nth-child(1) > dt > a:nth-child(1)")
	    driver.findElement(By.linkText("Select All")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
// Data Trigger selections
	    System.out.println("Selecting the Data Trigger Options");
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[2]/dt/a[1]")).click();
	    
	    System.out.println("Selecting the Lead Source Options");
	    // Lead Source Options
	    //driver.findElement(By.linkText("Select All")).click();
	    //body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-body > dl > div:nth-child(3) > dt > a:nth-child(1)
	    //                           /html/body/div[1]/div/div/form/div[2]/dl/div[3]/dt/a[1]
	    //driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[2]/dt/a[1]")).click();
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[3]/dt/a[1]")).click();
//	    driver.findElement(By.cssSelector(".ng-scope:nth-child(2) > .ng-binding > a:nth-child(1)")).click();
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    try {
		      Thread.sleep(2000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }


	    //body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-footer > button
	    // /html/body/div[1]/div/div/form/div[3]/button
	    // /html/body/div[1]/div/div/form/div[2]/dl/div[2]/dt/a[1]
	    //		body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-body > dl > div:nth-child(2) > dt > a:nth-child(1)
	    		
	    		System.out.println("Selecting the My attribute Options.");
	    	    //driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[3]/dt/a[1]")).click();
	    	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[4]/dt/a[1]")).click();
	    	    //Clicking-on Save button
	    		System.out.println("Clicking-on Save button.");
	    		//	    	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    		driver.findElement(By.cssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-footer > span > button")).click();
	    	    //driver.findElement(By.cssSelector(".btn-primary")).click();
	    	    
	    	    try {
	  		      Thread.sleep(3000);
	  		    } catch (InterruptedException e) {
	  		      e.printStackTrace();
	  		    }
	    	    //driver.findElement(By.cssSelector(".btn-default")).click();
	    	    //Clicking-on Close button
	    		System.out.println("Clicking-on Close button.");
	    	    driver.findElement(By.cssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-footer > button")).click();
	    	    
	    	    try {
		  		      Thread.sleep(3000);
		  		    } catch (InterruptedException e) {
		  		      e.printStackTrace();
		  		    } 
//Lead Source -- select all	

	    	    System.out.println("Not Sure what these 3 clicks are for, below.");
	    	    try {
		  		      Thread.sleep(3000);
		  		    } catch (InterruptedException e) {
		  		      e.printStackTrace();
		  		    }
	    driver.findElement(By.cssSelector("i:nth-child(6) > .ng-binding")).click();
	    driver.findElement(By.cssSelector(".message-bgcolor > .fa-plus")).click();
	    driver.findElement(By.cssSelector(".form-control:nth-child(2)")).click();
	    
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    } 
	    
	   // body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-body > dl > div:nth-child(3) > dt > a:nth-child(1)
	   //  /html/body/div[1]/div/div/form/div[2]/dl/div[3]/dt/a[1]	    
	    {
	      WebElement dropdown = driver.findElement(By.cssSelector(".form-control:nth-child(2)"));
	      String myText = dropdown.findElement(By.xpath("//option[. = 'Demo message (Form-Fill)']")).getText();
	      System.out.println("getting drop-down val -> this is the value of myText: " + myText);
	      dropdown.findElement(By.xpath("//option[. = 'Demo message (Form-Fill)']")).click();
	      //dropdown.findElement(By.xpath("//option[. = 'Demo message']")).click();
	      
			/*
			 * /html/body/div[1]/div/div/form/div[2]/div[1]/select #step-modal >
			 * div.modal-body > div:nth-child(1) > select
			 * 
			 */	    
	      }
	    
	    
	    //Delay field input
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(3) > div.input-group > input")).click();
	    
	    //click-thru field input
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(4) > label > input")).click();
	    
	    //open-notification field input
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(5) > label > input")).click();
	    

	    
	    //driver.findElement(By.cssSelector(".form-control:nth-child(2)")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(3) .ng-pristine")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(4) .ng-pristine")).click();
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    driver.findElement(By.cssSelector(".message-bgcolor > .fa-plus")).click();
	    driver.findElement(By.cssSelector(".form-control:nth-child(2)")).click();
	    {
	      WebElement dropdown = driver.findElement(By.cssSelector(".form-control:nth-child(2)"));
	      dropdown.findElement(By.xpath("//option[. = 'als message']")).click();
	    }
	    driver.findElement(By.cssSelector(".form-control:nth-child(2)")).click();
	    driver.findElement(By.name("delay")).click();
	    driver.findElement(By.name("delay")).click();
	    {
	      WebElement element = driver.findElement(By.name("delay"));
	      Actions builder = new Actions(driver);
	      builder.doubleClick(element).perform();
	    }
	    //String myDelayVal = driver.findElement(By.name("delay")).getText();
	    //String myDelayVal = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[2]/div[1]/input")).getText();
	    //String myDelayVal = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[2]/div[1]/input")).getAttribute("value");
	    //String myDelayVal = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[3]/div[1]/input")).getAttribute("value");
	    String myDelayVal = driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(4) > div.input-group > input")).getAttribute("value");
	    System.out.println("this is the before-value of myDelayVal: " + myDelayVal);
	    driver.findElement(By.name("delay")).sendKeys("3");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    } 

	    //String myDelayVal0 = driver.findElement(By.name("delay")).toString();
	    //String myDelayVal0 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[2]/div[1]/input")).getText();
	    String myDelayVal0 = driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(3) > div.input-group > input")).getAttribute("value");
	    //String myDelayVal0 = driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(4) > div.input-group > input")).getAttribute("value");
	    System.out.println("this is the after-value of myDelayVal0: " + myDelayVal0);
	    driver.findElement(By.cssSelector(".modal-body")).click();
	    
	    //click-thru checkbox
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(4) > label > input")).click();
	    //open-notification checkbox
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(5) > label > input")).click();
	    
	    
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(3) .ng-pristine")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(4) .ng-pristine")).click();
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    //String myDelayVal0 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[2]/div[1]/input")).getText();
	    //Close Modal Form.
	    System.out.println("Closing Edit-Drip Modal Form 1st-time");
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[3]/ul/li[2]/button")).click();

	    try {
	      Thread.sleep(2000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
	    //try to re-open the Day-3 modal - Edit
	    System.out.println("re-open the Day-3 modal - Edit-Drip Modal Form");
	    driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[3]/div/div[3]/ul/li[2]/a/i")).click();
	    driver.findElement(By.xpath("/html/body/ul/li[1]/a")).click();
	    //click in the Delay form-box.
	    //String myDelayVal1 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[2]/div[1]/input")).getAttribute("value");
	    String myDelayVal1 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[3]/div[1]/input")).getAttribute("value");
	    //driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(3) > div.input-group > input")).getAttribute("value");
	    System.out.println("this is the after-value of myDelayVal1: " + myDelayVal1);
	    driver.findElement(By.name("delay")).sendKeys("2");
	    //String myDelayVal2 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[2]/div[1]/input")).getAttribute("value");
	    String myDelayVal2 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[3]/div[1]/input")).getAttribute("value");
	    //driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(3) > div.input-group > input")).getAttribute("value");
	    System.out.println("this is the after-value of myDelayVal2: " + myDelayVal2);
	    driver.findElement(By.cssSelector(".modal-body")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(3) .ng-pristine")).click();
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(4) > label > input")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(4) .ng-pristine")).click();
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(5) > label > input")).click();
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    //Close Modal Form.
	    System.out.println("Closing Edit-Drip Modal Form 2nd-time");
	    //driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[3]/ul/li[2]/button")).click();
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[3]/ul/li[1]/span/button")).click();


	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    //try to re-open the Day-3 modal - Edit
	    System.out.println("re-open the Day-2 modal - Edit-Drip Modal Form");
	    driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[3]/div/div[3]/ul/li[2]/a/i")).click();
	    driver.findElement(By.xpath("/html/body/ul/li[1]/a")).click();
	    //click in the Delay form-box.
	    //String myDelayVal3 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[2]/div[1]/input")).getAttribute("value");
	    String myDelayVal3 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[3]/div[1]/input")).getAttribute("value");
	    System.out.println("this is the after-value of myDelayVal3: " + myDelayVal3);
	    driver.findElement(By.name("delay")).sendKeys("1");
	    //String myDelayVal4 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[2]/div[1]/input")).getAttribute("value");
	    String myDelayVal4 = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/div[3]/div[1]/input")).getAttribute("value");
	    System.out.println("this is the after-value of myDelayVal4: " + myDelayVal4);
	    driver.findElement(By.cssSelector(".modal-body")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(3) .ng-pristine")).click();
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(4) > label > input")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(4) .ng-pristine")).click();
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(5) > label > input")).click();
	    System.out.println("Saving Drip Modal Form for 3rd-time.");
	    driver.findElement(By.cssSelector(".btn-primary")).click();

	    String myDelayVal4ExpectedValue = "321";
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    System.out.println("Assertion starts here...");
		try{
		    System.out.println("Now in the try block...");
			Assert.assertEquals(myDelayVal4, myDelayVal4ExpectedValue);
			testStatus = "TestPassed";
		}catch (Throwable t){
		    System.out.println("Now in the catch block...");
			System.out.println("Found the Error Condition... logging-it & then continuing-on...");
			if (myLog.exists()) {
		        System.out.println("Log Exists, so opening log for appending the error message."); 
				//logWriter.close();
		        String message1 = "the myDelayVal4 is different from the myDelayVal4ExpectedValue:";
				FileWriter logWriter1 = new FileWriter(myLog, true);
				//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
				// append & write QATestlog.
						//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
						// myWriter.write("Files in Java might be tricky, but it is fun enough!");
						//LocalDateTime datetime = LocalDateTime.now();
						try {
							System.out.println(datetime);
							// logWriter.write(datetime.getHour());
							logWriter1.write(message1);
							//logWriter1.write(datetime.toString() + System.getProperty("line.separator"));
							logWriter1.write(System.getProperty("line.separator"));
							//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
							logWriter1.write("myDelayVal4: " + myDelayVal4.toString());
							logWriter1.write(System.getProperty("line.separator"));
							logWriter1.write("myDelayVal4ExpectedValue: " + myDelayVal4ExpectedValue.toString());
							//logWriter1.write(datetime.toString() + System.getProperty("line.separator"));							
							//logWriter1.write(System.getProperty("line.separator"));
							logWriter1.write(System.getProperty("line.separator"));
							//logWriter.write(nuLine);
							// myWriter.write(dataWithNewLine1);
							// myWriter.write(dataWithNewLine2);
							// myWriter.write(dataWithNewLine3);
							logWriter1.close();
							System.out.println("Successfully wrote error message lines to the log file.");
							testStatus = "TestFailed";
						} catch (IOException e) {
							System.out.println("An error occurred.");
							e.printStackTrace();
						}
			}

		}
		
	    //String myDelayVal4ExpectedValue = "321";
		System.out.println("This is value of myDelayVal4 Day value: " + myDelayVal4);
		System.out.println("This is expected value of myDelayVal4 Day value: " + myDelayVal4ExpectedValue);
		
		if(myDelayVal4.equals(myDelayVal4ExpectedValue)) {
			System.out.println("Passed: the value of myDelayVal4 equals myDelayVal4ExpectedValue: " + myDelayVal4 + " , " + myDelayVal4ExpectedValue);
		}
		
		//Assert.assertEquals(myDelayVal4, myDelayVal4ExpectedValue);
	    //Close Modal Form.
	    //System.out.println("Closing Edit-Drip Modal Form");
	    //driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[3]/ul/li[2]/button")).click();

	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    // Switch newly open Tab
		/*
		 * driver.switchTo().window(tabs.get(2)); driver.get("http://yopmail.com");
		 * System.out.println("this is the new window tab number-2 handle, below...");
		 * System.out.println(tabs);
		 */
	    
	    System.out.println(driver.getTitle());	
	    System.out.println("End of Test-Case: " + testCaseName); 
	    System.out.println("<<End-Of-Chrome-Test>> \n");
		
		  try { Thread.sleep(8000); } catch (InterruptedException e) {
		  e.printStackTrace(); }
		 
	    //trying to open a new TAB in Chrome browser
	    //System.out.println("trying to open a new TAB in Chrome browser...\n");
	    //driver.switchTo().window(tabs.get(1));
        //driver.switchTo().window(handle1.get(1));   
//	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
//	    driver.switchTo().window(tabs.get(1));
	    //driver.quit();
	    
	    //driver.findElement(By.cssSelector(".styles__LoginBox-sc-1h27wwi-0")).click();
	    //System.out.println("got-here-8 \n");	    
	    //driver.findElement(By.cssSelector(".styles__LoginBox-sc-1h27wwi-0")).click();
	    //System.out.println("got-here-9 \n");
	    //assertThat(driver.findElement(By.linkText("Forgot Password")).getText(), is("Forgot Password"));
			System.out.println("Test Completed Successfully!");
			System.out.println("QUIT & Close driver/browser.");
			//driver.close();
			//driver.quit();


      }
/*
		@AfterTest
		public void tearDownTest() {
			if(testStatus == "TestFailed") {
			//close browser
			//driver.close();
			//driver.quit();
			System.out.println("Test Failed!");
			}
			if(testStatus == "TestPassed") {
			//close browser
			driver.close();
			driver.quit();
			System.out.println("Test Completed Successfully!");
			}

		}
*/


		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
}



