package MA_CIT_Tests.MA_CIT_Tests;
//import org.testng.Assert;
//import com.sun.org.apache.bcel.internal.generic.Select;
//below libraries from selenium-IDE

import java.io.FileWriter;
//import org.junit.Test;
//import org.junit.After;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.chrome.*;

//import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

//static WebDriver driver;

public class MA_CIT_CreateStatusSegmentStepsSampleEmailTest {
	  public static String globalStatusName = "needStatusNameNumber";
	  public static String globalSegmentName = "needSegmentNameNumber";
	  private static final String t = null;
	//private WebDriver driver;
	  //WebDriver driver = null;
	  private Map<String, Object>  vars = new HashMap<String, Object>();
	  
	  public String waitForWindow(int timeout) {
		    try {
		      Thread.sleep(timeout);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
			return null;
	  }

	  //JavascriptExecutor js;
	  
	   // js = (JavascriptExecutor) driver;
	   
	    
//	public static void main(String[] args) {
	
	/*
	 * @BeforeTest public void setUp() throws Exception {
	 * System.setProperty("webdriver.chrome.driver",
	 * "C:\\Users\\acronin\\eclipse\\chromedriver.exe"); WebDriver driver = new
	 * ChromeDriver(); }
	 */	
//@Test
//Set Property

@Test	  
      public void test() throws Exception {
	  System.out.println("<<Start-Of-Chrome-Test>> \n");
		//System.setProperty(key, value)
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\acronin\\eclipse\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();		
		
		//Adding-in Headless Chrome Options
		
		boolean headless = true;
		//boolean headless = false;
		//ChromeOptions options = new ChromeOptions();
		//WebDriver driver = new ChromeDriver(options);
		//options.addArguments("--headless");
		WebDriver driver = null;
		if (headless) {
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			options.addArguments("window-size=1920,1080");
			driver = new ChromeDriver(options);
			driver.manage().window().maximize();
		}
		else {
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}


		
		//ChromeOptions options = new ChromeOptions();
		//options.addArguments("--headless");
		//options.addArguments("window-size=1280,800");
		//WebDriver driver = new ChromeDriver(options);
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//driver.manage().window().maximize();
		driver.get("https://app.cit.vollyma.com/login");
	    //driver.manage().window().setSize(new Dimension(1936, 1056));
	    driver.findElement(By.name("username")).click();
	    //driver.findElement(By.name("username")).sendKeys("acronin@myvolly.com");
	    driver.findElement(By.name("username")).sendKeys("acronin@yopmail.com");
	    driver.findElement(By.name("password")).click();
	    driver.findElement(By.name("password")).sendKeys("Kaitlin1$");
	    //driver.findElement(By.cssSelector(".MuiButton-label")).click();
	    //<button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
	    //driver.findElement(By.className("btn btn-lg btn-primary btn-block")).click();
	    driver.findElement(By.cssSelector(".btn")).click();
		String logHeader = "[QA run Test Log]";
		String testCaseName = "[MA_CIT_CreateStatusSegmentStepsSampleEmailTest.java]";
        System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");	

		FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
		LocalDateTime datetime = LocalDateTime.now();
		
	    try {
			System.out.println(datetime);
			logWriter.write(logWithNewLine0);
			logWriter.write(datetime.toString() + System.getProperty("line.separator"));
			logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
			logWriter.close();
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    Set<String> wh = driver.getWindowHandles();
		System.out.println("this is value of wh: " + wh);
		String currentWindowHandle = driver.getWindowHandle();
		System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
	    vars.put(currentWindowHandle, waitForWindow(2000));
	    //vars.put("window_handles", driver.getWindowHandles());
		//vars.put("window_handles", wh);
		//driver.findElement(By.cssSelector("./clients")).click();
	    driver.findElement(By.xpath("//*[@id=\"wrapper\"]/nav/div/div[2]/ul[2]/li[4]/a")).click();	    
		//driver.findElement(By.className("fa fa-fw fa-suitcase")).click();
	    driver.findElement(By.linkText("Journey Bank 2")).click();
	    //vars.put(key, value)
		System.out.println("Just Logged-IN-To MA-CIT... \n");
		
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
	    for (String handle1 : driver.getWindowHandles()) {
			System.out.println("this is window handle-1 below...");
	        System.out.println(handle1); 
			System.out.println("switching to this window handle...");	        
	        driver.switchTo().window(handle1);
	        //driver.switchTo().window(handle1.get(1)); 
	    }
	    //adding new TAB
	    System.out.println("Next, adding a new TAB... \n");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    //driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"\t");
	    //driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
	    WebElement link= driver.findElement(By.tagName("a"));  
	    String keyString =   (Keys.CONTROL+Keys.SHIFT.toString()+Keys.ENTER.toString());
	    link.sendKeys(keyString);
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	

	    //driver.switchTo().newWindow();
	    System.out.println("trying to open a new TAB in Chrome browser...\n");
	    
	 // Store all currently open tabs in tabs
	    //((JavascriptExecutor)driver).executeScript("window.open()");
	    //driver.getWindowHandles().new
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(1)); //switches to new tab
		System.out.println("this is window tabs handle below...");
        System.out.println(tabs);
	    // Click on link to open in new tab
	    //driver.findElement(By.id("Url_Link")).click();
	    
	    // Switch newly open Tab
	    driver.switchTo().window(tabs.get(1));
	    driver.get("http://google.com");
	    System.out.println("this is window tabs handle again, below...");
        System.out.println(tabs);
	    //driver.switchTo().window(tabs.get(-1));
	    //driver.switchTo().window(tabs.get(1));
        //driver.switchTo().window(handle1..get(1)); 
	    //https://app.cit.vollyma.com/reports/dashboard
	    //https://app.cit.vollyma.com/manage/statuses
	    //https://app.cit.vollyma.com/manage/attributes
	    //https://app.cit.vollyma.com/manage/statuses
//		driver.get("https://app.cit.vollyma.com/reports/dashboard");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
	    
		driver.get("https://app.cit.vollyma.com/manage/statuses");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
		
		driver.get("https://app.cit.vollyma.com/manage/attributes");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
		
		driver.get("https://app.cit.vollyma.com/manage/statuses");	
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }	
		
		System.out.println("Just opened statuses... \n");		
	    //driver.findElement(By.className("fa fa-exclamation-triangle fa-5x")).click();
		//driver.findElement(By.cssSelector(".fa-chevron-down:nth-child(1)")).click();
	    //driver.findElement(By.cssSelector(".accordion:nth-child(1) a:nth-child(3) > div")).click();
	    //driver.findElement(By.linkText("Manage")).click();
	    //driver.findElement(By.linkText("Attributes")).click();
	    //driver.findElement(By.linkText("Manage")).click();
	    //driver.findElement(By.linkText("Statuses")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		
			/*
			 * vars.put("win3853", waitForWindow(2000));
			 * //driver.findElement(By.cssSelector(".dropdown-sv:nth-child(2) .caret")).
			 * click(); driver.findElement(By.xpath(
			 * "//*[@id=\"header\"]/div/div[1]/div/div[2]/ul/li[2]/a/b")).click();
			 * driver.findElement(By.linkText("Statuses")).click();
			 * driver.findElement(By.linkText("Statuses")).click();
			 * driver.findElement(By.cssSelector(".pull-right:nth-child(2)")).click();
			 * driver.findElement(By.name("name")).click();
			 * driver.findElement(By.name("name")).sendKeys("MAstatus6"); try {
			 * Thread.sleep(3000); } catch (InterruptedException e) { e.printStackTrace(); }
			 * driver.findElement(By.cssSelector(".btn-primary")).click(); try {
			 * Thread.sleep(3000); } catch (InterruptedException e) { e.printStackTrace(); }
			 * driver.findElement(By.cssSelector(".btn-default")).click(); try {
			 * Thread.sleep(3000); } catch (InterruptedException e) { e.printStackTrace(); }
			 */
	    
	    //setting-up time value
	    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	    //calendar.clear();
	    //calendar.set(2011, Calendar.OCTOBER, 1);
	    long secondsSinceEpoch = calendar.getTimeInMillis() / 1000L;
	    String myStatusName = "MyMAstatus" + (secondsSinceEpoch);
	    String mySegmentName = "MyMAsegment" + (secondsSinceEpoch);
	    //assign to global variable
	    globalStatusName = myStatusName;
	    globalSegmentName = mySegmentName;
		System.out.println("Just got past selecting Statuses in drop-down... \n");
		System.out.println("Now creating a new Status in drop-down... \n");	
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    driver.findElement(By.cssSelector(".fa-plus")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    driver.findElement(By.name("name")).click();
	    try {
	      Thread.sleep(3000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
	    driver.findElement(By.name("name")).click();
	    //driver.findElement(By.name("name")).sendKeys("MyMAstatus20");
	    driver.findElement(By.name("name")).sendKeys(myStatusName);
	    
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		System.out.println("clicking on Save button next... \n");	    
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		System.out.println("clicking on Close button next... \n");		    
	    driver.findElement(By.cssSelector(".btn-default")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    //add in segments & steps.
		System.out.println("adding in segments & steps now.... \n");
		System.out.println("clicking the (+) segment button now.... \n");
	    driver.findElement(By.cssSelector(".segment-bgcolor > .fa-plus")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    System.out.println("clicking-in the segment name field now.... \n");
	    driver.findElement(By.name("name")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

//	    driver.findElement(By.name("name")).sendKeys("MAsegment20");
	    driver.findElement(By.name("name")).sendKeys(mySegmentName);
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    System.out.println("Selecting the Campaign Options");
	    driver.findElement(By.linkText("Select All")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    System.out.println("Selecting the Data Trigger Options");
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[2]/dt/a[1]")).click();
	    
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    System.out.println("Selecting the Lead Source Options");
	    //driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[2]/dt/a[1]")).click();
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[3]/dt/a[1]")).click();
	    //driver.findElement(By.cssSelector(".ng-scope:nth-child(2) > .ng-binding > a:nth-child(1)")).click();
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    System.out.println("Selecting the My attribute Options.");
	    //driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[3]/dt/a[1]")).click();
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/dl/div[4]/dt/a[1]")).click();
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    
	    //driver.findElement(By.name("Save")).click();
	    
	    
	    
	    //driver.findElement(By.cssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-footer > span > button")).click();

	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    //driver.findElement(By.name("Close")).click();
	    
	    //driver.findElement(By.cssSelector(".btn-default")).click();
	    
	    //Close is below
	    
	    driver.findElement(By.cssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-footer > button")).click();
	    
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    } 
	    
	    driver.findElement(By.cssSelector("i:nth-child(6) > .ng-binding")).click();
	    driver.findElement(By.cssSelector(".message-bgcolor > .fa-plus")).click();
	    driver.findElement(By.cssSelector(".form-control:nth-child(2)")).click();
	    {
		  System.out.println("adding the 1st step - Day-0, Demo message, below...");	
	      WebElement dropdown = driver.findElement(By.cssSelector(".form-control:nth-child(2)"));
	      String myText = dropdown.findElement(By.xpath("//option[. = 'Demo message (Form-Fill)']")).getText();
	      System.out.println("getting drop-down val -> this is the value of myText: " + myText);
	      dropdown.findElement(By.xpath("//option[. = 'Demo message (Form-Fill)']")).click();
	    }
	    
	    //Delay field input
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(3) > div.input-group > input")).click();
	    
	    //click-thru field input
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(4) > label > input")).click();
	    
	    //open-notification field input
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(5) > label > input")).click();
	    
	    //driver.findElement(By.cssSelector(".form-control:nth-child(2)")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(3) .ng-pristine")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(4) .ng-pristine")).click();
	    
	    //clicking-on SAVE button
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    driver.findElement(By.cssSelector(".message-bgcolor > .fa-plus")).click();
	    driver.findElement(By.cssSelector(".form-control:nth-child(2)")).click();
	    {
		  System.out.println("adding the 2nd step - Day-1, al's message, below...");	
	      WebElement dropdown = driver.findElement(By.cssSelector(".form-control:nth-child(2)"));
	      dropdown.findElement(By.xpath("//option[. = 'als message']")).click();
	    }
	    driver.findElement(By.cssSelector(".form-control:nth-child(2)")).click();
	    driver.findElement(By.name("delay")).click();
	    driver.findElement(By.name("delay")).click();
	    {
	      WebElement element = driver.findElement(By.name("delay"));
	      Actions builder = new Actions(driver);
	      builder.doubleClick(element).perform();
	    }
	    driver.findElement(By.name("delay")).sendKeys("1");
	    // #step-modal > div.modal-body > div:nth-child(3) > div.input-group > input
	    driver.findElement(By.cssSelector(".modal-body")).click();
	    
	    //click-thru checkbox
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(4) > label > input")).click();
	    //open-notification checkbox
	    driver.findElement(By.cssSelector("#step-modal > div.modal-body > div:nth-child(5) > label > input")).click();
	    
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(3) .ng-pristine")).click();
	    //driver.findElement(By.cssSelector(".checkbox:nth-child(4) .ng-pristine")).click();
	    driver.findElement(By.cssSelector(".btn-primary")).click();
	    try {
	      Thread.sleep(2000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }

		System.out.println("sending the Sample e-mail - al's message, below...");	
		driver.findElement(By.cssSelector(".dropdown-sv:nth-child(2) .caret")).click();
	    driver.findElement(By.linkText("Message Library")).click();
	    {
	      WebElement element = driver.findElement(By.cssSelector(".depth-0:nth-child(1) .text-secondary > .fa"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).perform();
	    }
	    {
	      WebElement element = driver.findElement(By.tagName("body"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element, 0, 0).perform();
	    }
	    {
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }

   	//	/html/body/div[2]/div/div/div[2]/div[3]/table/tbody/tr[1]/td[6]/a[2]/i	    {x-path to airplane send icon, "als message"}
    // //*[@id="content"]/div/div/div[2]/div[3]/table/tbody/tr[3]/td[6]/a[2]/i	    {x-path to airplane send icon, "demo message"}
//	      WebElement element = driver.findElement(By.cssSelector(".text-success:nth-child(3) > .fa"));
//	      Actions builder = new Actions(driver);
//	      builder.moveToElement(element).perform();
	    }
	    //"als message".
//	    driver.findElement(By.cssSelector(".depth-0:nth-child(1) .text-success > .fa")).click();
	    //"demo message"
	    //driver.findElement(By.cssSelector(".depth-0:nth-child(3) .text-success > .fa")).click();
	    //driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div[3]/table/tbody/tr[3]/td[6]/a[2]/i")).click();
	    driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div[3]/table/tbody/tr[7]/td[6]/a[2]/i")).click();
	                                 
//	                                 /html/body/div[2]/div/div/div[2]/div[3]/table/tbody/tr[7]/td[6]/a[2]/i/text()
	    {
	      WebElement element = driver.findElement(By.tagName("body"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element, 0, 0).perform();
	    }
	    
	    //begin.
	    
		// Now creating a BatchSend variable for randomized emails
		long randomEmailVar = System.currentTimeMillis();
		String s = Long.toString(randomEmailVar);
		System.out.println("This is value of randomEmailVar: " + s);
		System.out.println("This is the length of randomEmailVar: " + s.length());

		//alfc11613632866233@maildrop.cc		
		String ALFC1var = "ALFCsample" + s + "@" + "yopmail.com";
		//String ALFC1var = "al_cronin" + "@" + "yahoo.com";
		
	    //end.
	    
		//Select Recipient
		System.out.println("Select Recipient");
	    driver.findElement(By.id("recipients")).click();
	    // xpath --> /html/body/div[1]/div/div/form/div[2]/fieldset[1]/div[1]/input
	    //driver.findElement(By.id("recipients")).sendKeys("alcsample98@yopmail.com");
	    driver.findElement(By.id("recipients")).sendKeys(ALFC1var);
	    
	    //Select Sender
	    System.out.println("Select Sender");
	    // Sender caret xpath --> 
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/fieldset[1]/div[2]/div/div/span/i")).click();
	    // /html/body/div[1]/div/div/form/div[2]/fieldset[1]/div[2]/div/div/span/i
	                                 
	    //driver.findElement(By.cssSelector(".ui-select-toggle")).click();
	    // #ui-select-choices-row-1-1 > a > div
	    // acronin xpath --> 
	    // abellomy - driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/fieldset[1]/div[2]/div/ul/li/div[4]/a/div")).click();
	    //Sender-select: acronin
	    driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/fieldset[1]/div[2]/div/ul/li/div[5]/a/div")).click();
	    //driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[2]/fieldset[1]/div[2]/div/ul/li/div[6]/a/div")).click();
	    ///html/body/div[1]/div/div/form/div[2]/fieldset[1]/div[2]/div/div/span/span[2]/span
	    
	    //driver.findElement(By.cssSelector("#ui-select-choices-row-1-2 > a > div")).click();
	    //driver.findElement(By.cssSelector("#ui-select-choices-row-0-0 .ng-binding")).click();
	    
	    // Enter recipient.firstname
	    System.out.println("Enter Recipient.FirstName");
	    driver.findElement(By.name("recipient.firstname")).click();
	    driver.findElement(By.name("recipient.firstname")).sendKeys("alc");
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //driver.findElement(By.cssSelector(".checkbox-inline:nth-child(3) > .ng-pristine")).click();
	    
	    //Click-in Click-Through-Notification checkbox.
	    System.out.println("Click-in Click-Through-Notification checkbox.");
	    //driver.findElement(By.cssSelector(".ng-untouched:nth-child(1)")).click();
	    driver.findElement(By.cssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-footer > div.clearfix > label:nth-child(3) > input")).click();
	    
	    //Click-in Open-Notification checkbox.
	    System.out.println("Click-in Open-Notification checkbox.");
	    driver.findElement(By.cssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-footer > div.clearfix > label:nth-child(4) > input")).click();
	    
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    // Click-on the Trigger button
	    //driver.findElement(By.cssSelector(".btn-primary")).click();
	    driver.findElement(By.cssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-footer > div:nth-child(2) > span > button")).click();
	    
	    try {
		      Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    //Click-on the Close button
	    //driver.findElement(By.cssSelector(".modal-footer > div > .btn")).click();
	    //driver.findElement(By.cssSelector(".btn-default")).click();
	    driver.findElement(By.cssSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > form > div.modal-footer > div:nth-child(2) > button")).click();
	    
	    try {
		      Thread.sleep(8000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		
	    System.out.println(driver.getTitle());
	    //Logout.
	    //driver.findElement(By.xpath("//*[@id=\"header\"]/div/div[1]/div/div[2]/div/div/a")).click();
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }


		
	    // Switch to the newly opened Tab.
	    //driver.switchTo().window(tabs.get(2));
	    //driver.get("http://www.yopmail.com");
	    // Switch to the newly opened Tab.
	    driver.switchTo().window(tabs.get(2));
	    //driver.get("http://yopmail.com/en/");
		System.out.println("going to yopmail.com site. \n");
	    //driver.get("http://yopmail.com");
		//driver.get("https://yopmail.com/en/wm");
		System.out.println("going to yopmail.com site. \n");
	    driver.get("http://yopmail.com");

	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
	    try {
		      Thread.sleep(2000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    //driver.findElement(By.name("login")).sendKeys("alcsample98@yopmail.com");
	    driver.findElement(By.name("login")).sendKeys(ALFC1var);
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    //driver.findElement(By.xpath("//*[@id="f"]/table/tbody/tr[1]/td[3]/input")).click();
	    //driver.findElement(By.className("sbut")).click();
	    //driver.findElement(By.className("i.material-icons-outlined.f36")).click();
	    //driver.findElement(By.xpath("/html/body/div/div[2]/main/div[3]/div/div[1]/div[2]/div/div/form/div/div/div[4]/button/i")).click();
		/*
		 * try { Thread.sleep(10000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 * 
		 */	
	    //Clicking-on right-arrow submit button now.
		  //driver.findElement(By.xpath("/html/body/div/div[2]/main/div[3]/div/div[1]/div[2]/div/div/form/div/div/div[4]/button")).click();
	    System.out.println(" Click-on Submit email-address right-arrow & show email address: " + ALFC1var);
		  driver.findElement(By.xpath("/html/body/div/div[2]/main/div[3]/div/div[1]/div[2]/div/div/form/div/div[1]/div[4]/button/i")).click();
		  		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		  
		  //Clicking on the circle-Refresh button now.
		  System.out.println("Now Click-on the Refresh button");
		  driver.findElement(By.id("refresh")).click();			 

	    //Click-on unsub(opt-out)
	    //driver.findElement(By.xpath("//*[@id=\"mailmillieu\"]/div[2]/a[3]")).click();
	    //driver.findElement(By.xpath("//a[@*][3]")).click();
	    //driver.findElement(By.xpath("/html/body/div/div[3]/div[2]/a[3]")).click();
	  //a[@rel="nofollow"]")).click();
	    //driver.findElement(By.xpath("//a[starts-with(@href,'https://app.cit.vollyma.com/unsub')]")).click();
	    driver.switchTo().frame("ifmail");
	    //driver.findElement(By.xpath("//a[@rel='nofollow']")).click();
	    
	    //WebElement m = driver.findElement(By.xpath("/html/body/div/div[3]/div[2]/a[3]"));
	    //System.out.println("Element identified with xpath: " +m.getText());
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    //driver.findElement(By.xpath("/html/body/div/div[3]/div[2]/a[3]")).click();
	    
	    //System.out.println("Element identified with xpath: " +m.getText());
/*	    
	    List<WebElement> anchors = driver.findElements(By.tagName("a"));
	    Iterator<WebElement> i = anchors.iterator();
	    while(i.hasNext()) {
	    WebElement anchor = i.next();
	    String href = anchor.getAttribute("href");
	    System.out.println("Printing-out - href:");
	    System.out.println(href);
	    }
*/
	    
	    try {
		      Thread.sleep(15000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	  //*[@id="login"]
	    System.out.println("this is the new window tab number-2 handle, below...");
        System.out.println(tabs);

	    // Switch back to the middle Tab.
	    System.out.println("switching to TAB-2, below...");
		  driver.switchTo().window(tabs.get(2)); 
		  try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

		 //driver.get("http://yopmail.com");
		 
        //Check-on the Samples TAB
		System.out.println("switching to Samples TAB, below...");
		driver.get("https://app.cit.vollyma.com/reports/realtime/samples?size=20&page=1");
		try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    System.out.println(driver.getTitle());
	    //String myExpectedSampleReportRecipientValue = "alcsample98@yopmail.com";
	    String myExpectedSampleReportRecipientValue = ALFC1var;
	    //pick the top of the Samples Recipient Message column
	    String myReportValue = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/table/tbody/tr/td[4]")).getText();
		System.out.println("This is value of myReportVal: " + myReportValue);
		System.out.println("This is value of myExpectedSamplesReportValue: " + myExpectedSampleReportRecipientValue);
		Assert.assertEquals(myReportValue, myExpectedSampleReportRecipientValue);

		if(myReportValue.equals(myExpectedSampleReportRecipientValue)) {
			System.out.println("Passed: the value of myReportValue equals myExpectedSampleReportRecipientValue: " + myReportValue + " , " + myExpectedSampleReportRecipientValue);
		}
		        
        try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    //Done. Quit Browser.
	    System.out.println(driver.getTitle());	    
	    //driver.quit();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("End of Test-Case: " + testCaseName); 
	    System.out.println("<<End-Of-Chrome-Test>> \n");
		/*
		 * try { Thread.sleep(4000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 */
	    //trying to open a new TAB in Chrome browser
	    //System.out.println("trying to open a new TAB in Chrome browser...\n");
	    //driver.switchTo().window(tabs.get(1));
        //driver.switchTo().window(handle1.get(1));   
//	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
//	    driver.switchTo().window(tabs.get(1));
	    //driver.quit();
	    
	    //driver.findElement(By.cssSelector(".styles__LoginBox-sc-1h27wwi-0")).click();
	    //System.out.println("got-here-8 \n");	    
	    //driver.findElement(By.cssSelector(".styles__LoginBox-sc-1h27wwi-0")).click();
	    //System.out.println("got-here-9 \n");
	    //assertThat(driver.findElement(By.linkText("Forgot Password")).getText(), is("Forgot Password"));
	    
		System.out.println("Test Completed Successfully!");
		System.out.println("QUIT & Close driver/browser.");
		//driver.close();
		//driver.quit();
		
      }
/*
@AfterTest
public void tearDownTest(){
	driver.quit();
}
	//close browser
	System.out.println("Got to AfterTest Successfully!");
	//driver.close();

	System.out.println("Test Completed Successfully!");
}
*/

/*
 * @AfterTest public void tearDownTest() { //close browser
 * System.out.println("Got to AfterTest Successfully!"); //driver.close();
 * driver.quit(); System.out.println("Test Completed Successfully!"); }
 * 
 */
		
		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
}



