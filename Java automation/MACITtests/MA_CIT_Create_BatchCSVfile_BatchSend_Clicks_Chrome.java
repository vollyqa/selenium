package MA_CIT_Tests.MA_CIT_Tests;
//package AutomationTests;

//import org.testng.Assert;
//import com.sun.org.apache.bcel.internal.generic.Select;
//below libraries from selenium-IDE

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File; // Import the File class
import java.io.IOException; // Import the IOException class to handle errors
import java.sql.Date;
import java.io.FileWriter; // Import the FileWriter class
import java.io.IOException; // Import the IOException class to handle errors

//import org.junit.Test;
//import org.junit.After;
//package AutomationTests;
//import org.testng.Assert;
//import com.sun.org.apache.bcel.internal.generic.Select;
//below libraries from selenium-IDE

//import org.junit.Test;
//import org.junit.After;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriverException;
//import org.openqa.selenium.WebDriverInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

//import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MA_CIT_Create_BatchCSVfile_BatchSend_Clicks_Chrome {
	public static String globalStatusName = "needStatusNameNumber";
	public static String globalSegmentName = "needSegmentNameNumber";
	private static final String t = null;
	// private WebDriver driver;
	  WebDriver driver = null;
	private Map<String, Object> vars = new HashMap<String, Object>();

	public String waitForWindow(int timeout) {
		try {
			Thread.sleep(timeout);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Test
//Set Property

	public void test() throws Exception {

		// Creating&Writing to the QA-Test-Log: "c:\QATestLog.txt"
		// Now writing to the BatchSend CSV file
		String logHeader = "[QA run Test Log]";
		//String testCaseName = "[MA_CIT_Create_BatchCSVfile_BatchSend_OptOuts3.java]";
		String testCaseName = "[MA_CIT_Create_BatchCSVfile_BatchSend_Clicks_Chrome.java]";
        System.out.println("Starting Test-Case: " + testCaseName); 
		// String dataFirstEmail = "acronin@myvolly.com, ALFC1.yopmail.com, ALC1";
		// String dataSecondEmail = "acronin@myvolly.com, ALFC2.yopmail.com, ALC2";
		// String dataThirdEmail = "acronin@myvolly.com, ALFC3.yopmail.com, ALC3";
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		//String logWithNewLine1 = testCaseName + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName;
		// String dataWithNewLine1 = dataFirstEmail +
		// System.getProperty("line.separator");
		// String dataWithNewLine2 = dataSecondEmail +
		// System.getProperty("line.separator");
		// String dataWithNewLine3 = dataThirdEmail +
		// System.getProperty("line.separator");
		// Date date = java.util.Calendar.getInstance().getDate();
//    System.out.println(date);
		// overwrite QATestlog.
		// FileWriter logWriter = new FileWriter("c:\\QATestLog.txt");
		// append & write QATestlog.
		//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
		// myWriter.write("Files in Java might be tricky, but it is fun enough!");
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");
		
		if (myLog.exists()) {
            System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
            System.out.println("Log doesnot Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		
		
		/*
		 * try { System.out.println(datetime); // logWriter.write(datetime.getHour());
		 * logWriter.write(logWithNewLine0); logWriter.write(datetime.toString() +
		 * System.getProperty("line.separator")); logWriter.write(logWithNewLine1 +
		 * System.getProperty("line.separator")); // myWriter.write(dataWithNewLine1);
		 * // myWriter.write(dataWithNewLine2); // myWriter.write(dataWithNewLine3);
		 * //logWriter.close();
		 * System.out.println("Successfully wrote lines to the log file."); } catch
		 * (IOException e) { System.out.println("An error occurred.");
		 * e.printStackTrace(); }
		 */
		
		
		// public static String globalStatusName = "needStatusNameNumber";
		// public static String globalSegmentName = "needSegmentNameNumber";
		// private static final String t = null;
		// private WebDriver driver;

		// private Map<String, Object> vars = new HashMap<String, Object>();
		/*
		 * public String waitForWindow(int timeout) { try { Thread.sleep(timeout); }
		 * catch (InterruptedException e) { e.printStackTrace(); } return null; }
		 */
		// JavascriptExecutor js;

		// js = (JavascriptExecutor) driver;

//	public static void main(String[] args) {

		/*
		 * @BeforeTest public void setUp() throws Exception {
		 * System.setProperty("webdriver.chrome.driver",
		 * "C:\\Users\\acronin\\eclipse\\chromedriver.exe"); WebDriver driver = new
		 * ChromeDriver(); }
		 */

		System.out.println("<<Start-Of-Chrome-Test>> \n");
		// System.setProperty(key, value)
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\acronin\\eclipse\\chromedriver.exe");
		// WebDriver driver = new ChromeDriver();

		// Adding-in Headless Chrome Options
		boolean headless = true;
		//boolean headless = false;
		//ChromeOptions options = new ChromeOptions();
		//WebDriver driver = new ChromeDriver(options);
		//options.addArguments("--headless");
			
		WebDriver driver = null;
		if (headless) {
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			options.addArguments("window-size=1920,1080");
			driver = new ChromeDriver(options);
		}
		else {
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}

		//ChromeOptions options = new ChromeOptions();
		//options.addArguments("--headless");
		//options.addArguments("window-size=1280,800");
		//WebDriver driver = new ChromeDriver(options);
		//driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.manage().window().maximize();
		driver.get("https://app.cit.vollyma.com/login");
		//driver.get("https://app.vollyma.com/login");
		// driver.manage().window().setSize(new Dimension(1936, 1056));
		driver.findElement(By.name("username")).click();
		//driver.findElement(By.name("username")).sendKeys("acronin@myvolly.com");
		driver.findElement(By.name("username")).sendKeys("acronin@yopmail.com");
		driver.findElement(By.name("password")).click();
		driver.findElement(By.name("password")).sendKeys("Kaitlin1$");
		// driver.findElement(By.cssSelector(".MuiButton-label")).click();
		// <button type="submit" class="btn btn-lg btn-primary btn-block">Sign
		// in</button>
		// driver.findElement(By.className("btn btn-lg btn-primary btn-block")).click();
		driver.findElement(By.cssSelector(".btn")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Set<String> wh = driver.getWindowHandles();
		System.out.println("this is value of wh: " + wh);
		String currentWindowHandle = driver.getWindowHandle();
		System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		vars.put(currentWindowHandle, waitForWindow(2000));
		// vars.put("window_handles", driver.getWindowHandles());
		// vars.put("window_handles", wh);
		// driver.findElement(By.cssSelector("./clients")).click();
		driver.findElement(By.xpath("//*[@id=\"wrapper\"]/nav/div/div[2]/ul[2]/li[4]/a")).click();
		// driver.findElement(By.className("fa fa-fw fa-suitcase")).click();
		driver.findElement(By.linkText("Journey Bank 2")).click();
		// vars.put(key, value)
		System.out.println("Just Logged-IN-To MA-CIT... \n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (String handle1 : driver.getWindowHandles()) {
			System.out.println("this is window handle-1 below...");
			System.out.println(handle1);
			System.out.println("switching to this window handle...");
			driver.switchTo().window(handle1);
			// driver.switchTo().window(handle1.get(1));
		}
		// adding new TAB
		System.out.println("Next, adding a new TAB... \n");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"\t");
		// driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		WebElement link = driver.findElement(By.tagName("a"));
		String keyString = (Keys.CONTROL + Keys.SHIFT.toString() + Keys.ENTER.toString());
		link.sendKeys(keyString);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// driver.switchTo().newWindow();
		System.out.println("trying to open a new TAB in Chrome browser...\n");

		// Store all currently open tabs in tabs
		// ((JavascriptExecutor)driver).executeScript("window.open()");
		// driver.getWindowHandles().new
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1)); // switches to new tab
		System.out.println("this is window tabs handle below...");
		System.out.println(tabs);
		// Click on link to open in new tab
		// driver.findElement(By.id("Url_Link")).click();

		// Switch newly open Tab
		driver.switchTo().window(tabs.get(1));
		driver.get("http://google.com");
		System.out.println("this is window tabs handle again, below...");
		System.out.println(tabs);
		// driver.switchTo().window(tabs.get(-1));
		// driver.switchTo().window(tabs.get(1));
		// driver.switchTo().window(handle1..get(1));
		// https://app.cit.vollyma.com/reports/dashboard
		// https://app.cit.vollyma.com/manage/statuses
		// https://app.cit.vollyma.com/manage/attributes
		// https://app.cit.vollyma.com/manage/statuses
//		driver.get("https://app.cit.vollyma.com/reports/dashboard");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		driver.get("https://app.cit.vollyma.com/manage/statuses");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		driver.get("https://app.cit.vollyma.com/manage/attributes");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		driver.get("https://app.cit.vollyma.com/manage/statuses");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Just opened statuses... \n");

		// Now, creating a sample test CSV file with 3 batchSend emails.
		File myObj = new File("c:\\myBatchSendTestFile.txt");
		if (myObj.exists()) {
			System.out.println("File name: " + myObj.getName());
			System.out.println("Absolute path: " + myObj.getAbsolutePath());
			System.out.println("Writeable: " + myObj.canWrite());
			System.out.println("Readable " + myObj.canRead());
			System.out.println("File size in bytes " + myObj.length());
		} else {
			System.out.println("The file does not exist.\n");
			System.out.println("So, creating a new BatchSend CSV file.\n");
			try {
				if (myObj.createNewFile()) {
					System.out.println("File created: " + myObj.getName());
				} else {
					System.out.println("File already exists.");
				}
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		// Now creating a BatchSend variable for randomized emails
		long randomEmailVar = System.currentTimeMillis();
		String s = Long.toString(randomEmailVar);
		System.out.println("This is value of randomEmailVar: " + s);
		System.out.println("This is the length of randomEmailVar: " + s.length());

		//alfc11613632866233@maildrop.cc		
		//String ALFC1var = "ALFC1" + s + "@" + "yopmail.com";
		String ALFC1var = s + "@" + "yopmail.com";
		//String ALFC1var = "ALFC1" + s + "@" + "maildrop.cc";
		//String ALFC1var = "myjunkmail@dispostable.com";
		//String ALFC1var = "ALFC1" + s + "@" + "owlymail.com";
		String nuLine = "\r\n";
		FileWriter logWriter = new FileWriter(myLog, true);
		logWriter.write("Opt-Out email: " + ALFC1var + System.getProperty("line.separator"));
		logWriter.write(System.getProperty("line.separator"));
		logWriter.write(nuLine);
		logWriter.close();
		//Now writing to the BatchSend CSV file
		//just send the dataFirstEmail.
		String dataHeader = "agent,recipient.email,recipient.firstname";
		String dataFirstEmail = "acronin@myvolly.com," + ALFC1var + ",ALC1";
		String dataSecondEmail = "acronin@myvolly.com,ALFC2@yopmail.com,ALC2";
		String dataThirdEmail = "acronin@myvolly.com,ALFC3@yopmail.com,ALC3";
		String dataWithNewLine0 = dataHeader + System.getProperty("line.separator");
		String dataWithNewLine1 = dataFirstEmail + System.getProperty("line.separator");
		String dataWithNewLine2 = dataSecondEmail + System.getProperty("line.separator");
		String dataWithNewLine3 = dataThirdEmail + System.getProperty("line.separator");
		try {
			//FileWriter myWriter = new FileWriter("c:\\myBatchSendTestFile.txt");
			FileWriter myWriter = new FileWriter("C:\\Users\\acronin\\bsclickopen.csv");
			
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			myWriter.write(dataWithNewLine0);
			myWriter.write(dataWithNewLine1);
			//myWriter.write(dataWithNewLine2);
			//myWriter.write(dataWithNewLine3);
			myWriter.close();
			//System.out.println("Successfully wrote 4 lines to the file.");
			System.out.println("Successfully wrote 2 lines to the file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

		//driver.get("https://owlymail.com/en/");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Enter Email ID into Owly Mail
		//driver.findElement(By.xpath("/html/body/main/div[1]/div/div[1]/div[2]/form[1]/input[2]")).sendKeys(ALFC1var);
		
		//Click-on the CREATE button of Owly Email
		//driver.findElement(By.xpath("/html/body/main/div[1]/div/div[1]/div[2]/form[1]/input[4]")).click();
		
		
		// Now sending a BatchSend
		System.out.println("So, sending a batch email here.\n");
		driver.get("https://app.cit.vollyma.com/manage/batches");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Just opened Batch Sends screen in MA... \n");
		driver.findElement(By.xpath("//button[contains(text(),'New Batch Send')]")).click();
		driver.findElement(By.xpath("//*[@id=\"header\"]/div/div[1]/div/div[2]/ul/li[2]/a")).click();

		// driver.findElement(By.cssSelector(".dropdown-sv:nth-child(2)
		// .caret")).click();
		driver.findElement(By.linkText("Batch Sends")).click();
		driver.findElement(By.name("name")).click();
		driver.findElement(By.name("name")).clear();

		driver.findElement(By.name("name")).sendKeys("BS_DTS_" + (datetime));
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// driver.findElement(By.cssSelector(".col-xs-12 > .ng-untouched")).click();
		System.out.println("got here-1... \n");
		System.out.println("Enabling Open Notification-checkbox=true... \n");
		driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div[2]/form/div[1]/fieldset[1]/div[3]/div[2]/label[2]/i[2]")).click();
		// driver.findElement(By.cssSelector(".col-xs-12 > .ng-untouched")).click();
		System.out.println("got here-2... \n");
		System.out.println("Clicking-on Message-Selectioin Demo-Message... \n");
		//alanmessage(clone)
		//driver.findElement(By.xpath("//*[@id='content']/div/div/div/div[2]/form/div[2]/fieldset/div/div/select/option[4]")).click();
		
		//Demo message
		System.out.println("Select Demo message - Forms. \n");
		driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div[2]/fieldset/div/div/select/option[8]")).click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("launching file-upload pop-up...next--disabled\n");
		// driver.findElement(By.xpath("//button[contains(text(),'Reset')]")).click();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Clicking-on Batch CSV... \n");
		// driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div[2]/form/div[4]/fieldset/div[1]/div[1]/div/span/input")).click();
		// driver.findElement(By.xpath("//*[@id='content']/div/div/div/div[2]/form/div[4]/fieldset/div[2]/div/button[2]")).click();
		// driver.findElement(By.linkText("Reset")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div[2]/form/div[4]/fieldset/div[1]/div[1]/div/div")).click();
		// driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div[4]/fieldset/div[1]/div[1]/div/div")).click();
		// driver.findElement(By.name("Choose File")).click();
		// driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div[2]/form/div[4]/fieldset/div[1]/div[1]/div/div")).sendKeys("c:\\myBatchSendTestFile.csv");
//	    driver.findElement(By.className(className)
//	    	    driver.findElement(By.ByClassName
//	    WebElement inputField = driver.findElement(By.xpath("//*[@id='content']/div/div/div/div[2]/form/div[2]/fieldset/div/div/select/option[4]"));
		// WebElement inputField = driver.findElement(By.Locator("LocatorValue"));
//	    inputField.sendKeys(Keys.TAB);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.TAB).build().perform();
		System.out.println("Just finished TABing from Demo Message... \n");
		act.sendKeys(Keys.RETURN).build().perform();
		System.out.println("Just finished hitting RETURN... \n");
		// act.sendKeys("c:\\myBatchSendTestFile.csv");
		// act.sendKeys(Keys.valueOf(name)
		// String item = "myBatchSendTestFile.csv";
		// act.sendKeys(Keys.valueOf(item));
		// act.sendKeys(Keys.
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Just starting to send csv file name... \n");
		System.out.println("Sending csv file name to the PATH c-temp-bsbounced.csv... \n");
		//String FILE_UPLOAD_PATH = "C:\\temp\\bsclickopen.csv";
		String FILE_UPLOAD_PATH = "C:\\Users\\acronin\\bsclickopen.csv";
		String path = FILE_UPLOAD_PATH; 
		//(Full path with file name from /tmp folder)
		//driver.findElement(By.id("FILE_INPUT_ID")).sendKeys(path);
		//driver.findElement(By.id("FILE_INPUT_ID")).sendKeys(path);
		//driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div[4]/fieldset/div[1]/div[1]/div/div")).sendKeys(path);
		//driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div[4]/fieldset/div[1]/div[1]/div/span/input")).click();
		//driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div[4]/fieldset/div[1]/div[1]/div/div/span")).sendKeys(path);
		System.out.println("Sending path to CSV file... \n");
		System.out.println("Sending path to CSV file... \n");
		System.out.println("Sending path to CSV file... \n");
		driver.findElement(By.name("batchcsv")).sendKeys(path);

		//Robot robot = new Robot();
		// Simulate key Events
		// robot.keyPress(KeyEvent.VK_C);
		// robot.keyRelease(KeyEvent.VK_C);
		// robot.keyPress(KeyEvent.VK_SHIFT);
		// robot.keyRelease(KeyEvent.VK_SHIFT);
		// robot.keyPress(KeyEvent.VK_SEMICOLON);
		// robot.keyRelease(KeyEvent.VK_SEMICOLON);
		// robot.keyRelease(KeyEvent.VK_SHIFT);
		// robot.keyPress(KeyEvent.VK_BACK_SLASH);
		// robot.keyRelease(KeyEvent.VK_BACK_SLASH);
		// robot.keyPress(KeyEvent.VK_BACK_SLASH);
		// robot.keyRelease(KeyEvent.VK_BACK_SLASH);
		/*
		 * robot.keyPress(KeyEvent.VK_B); robot.keyPress(KeyEvent.VK_S);
		 * robot.keyPress(KeyEvent.VK_C); robot.keyPress(KeyEvent.VK_L);
		 * robot.keyPress(KeyEvent.VK_I); robot.keyPress(KeyEvent.VK_C);
		 * robot.keyPress(KeyEvent.VK_K); robot.keyPress(KeyEvent.VK_O);
		 * robot.keyPress(KeyEvent.VK_P); robot.keyPress(KeyEvent.VK_E);
		 * robot.keyPress(KeyEvent.VK_N); robot.keyPress(KeyEvent.VK_PERIOD);
		 * robot.keyRelease(KeyEvent.VK_PERIOD); robot.keyPress(KeyEvent.VK_C);
		 * robot.keyPress(KeyEvent.VK_S); robot.keyPress(KeyEvent.VK_V); try {
		 * Thread.sleep(4000); } catch (InterruptedException e) { e.printStackTrace(); }
		 * 
		 * robot.keyPress(KeyEvent.VK_TAB); robot.keyRelease(KeyEvent.VK_TAB);
		 * robot.keyPress(KeyEvent.VK_TAB); robot.keyRelease(KeyEvent.VK_TAB);
		 * robot.keyPress(KeyEvent.VK_ENTER); robot.keyRelease(KeyEvent.VK_ENTER);
		 * 
		 */		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		/*
		 * robot.keyPress(KeyEvent.VK_CAPS_LOCK);
		 * robot.keyRelease(KeyEvent.VK_CAPS_LOCK); robot.keyPress(KeyEvent.VK_H);
		 * robot.keyRelease(KeyEvent.VK_H); robot.keyPress(KeyEvent.VK_E);
		 * robot.keyRelease(KeyEvent.VK_E); robot.keyPress(KeyEvent.VK_L);
		 * robot.keyRelease(KeyEvent.VK_L); robot.keyPress(KeyEvent.VK_L);
		 * robot.keyRelease(KeyEvent.VK_L); robot.keyPress(KeyEvent.VK_O);
		 * robot.keyRelease(KeyEvent.VK_O); robot.keyPress(KeyEvent.VK_CAPS_LOCK);
		 * robot.keyRelease(KeyEvent.VK_CAPS_LOCK); robot.keyPress(KeyEvent.VK_TAB);
		 * robot.keyRelease(KeyEvent.VK_TAB); robot.keyPress(KeyEvent.VK_TAB);
		 * robot.keyRelease(KeyEvent.VK_TAB); robot.keyPress(KeyEvent.VK_ENTER);
		 * robot.keyRelease(KeyEvent.VK_ENTER); robot.keyPress(KeyEvent.VK_PERIOD);
		 * robot.keyPress(KeyEvent.VK_BACK_SLASH); robot.keyPress(KeyEvent.VK_COLON);
		 * 
		 * 
		 * Actions action = new Actions(driver);
		 * action.sendKeys(Keys.SPACE).build().perform();
		 * 
		 * WebElement body = driver.findElement(By.tagName("body"));
		 * body.sendKeys(Keys.SPACE);
		 * driver.findElement(By.className("form-control")).sendKeys(
		 * "c:\\myBatchSendTestFile.csv");
		 * 
		 * Actions action1 = new Actions(driver);
		 * action.sendKeys(Keys.SPACE).build().perform();
		 * //driver.findElement(By.className("ng-pristine ng-untouched ng-valid")).click
		 * (); //driver.findElement(By.xpath(
		 * "//*[@id=\"content\"]/div/div/div/div[2]/form/div[4]/fieldset/div[1]/div[1]/div/span"
		 * )).click(); //driver.findElement(By.name("batchcsv")).click();
		 */

		/*
		 * try { Thread.sleep(3000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 * driver.switchTo().activeElement().sendKeys("c:\\myBatchSendTestFile.csv");
		 * try { Thread.sleep(5000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 * driver.switchTo().activeElement().sendKeys("c:\\myBatchSendTestFile.csv");
		 * try { Thread.sleep(5000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 */

		System.out.println("got here-2.2... \n");
		
		// driver.findElement(By.xpath("//input[contains(@type, 'file')]")).clear();
		// driver.findElement(By.xpath("//input[contains(@type,
		// 'file')]")).sendKeys("c:\\myBatchSendTestFile.csv");
		// driver.navigate().forward();

		/*
		 * try { Thread.sleep(5000); } catch (InterruptedException e) {
		 * e.printStackTrace(); } System.out.println("got here-2.3... \n");
		 * driver.switchTo().activeElement().sendKeys("Test1"); try {
		 * Thread.sleep(5000); } catch (InterruptedException e) { e.printStackTrace(); }
		 * System.out.println("got here-2.4... \n"); Actions actions = new
		 * Actions(driver); actions.sendKeys("Test2").perform();
		 * 
		 * 
		 * driver.switchTo().alert();
		 * System.out.println(driver.switchTo().alert().getText());
		 * driver.switchTo().alert().sendKeys("c:\\myBatchSendTestFile.csv");
		 * System.out.println("got here-2.5... \n");
		 * 
		 * 
		 * 
		 * try { Thread.sleep(5000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 * //actions.sendKeys("\"c:\\\\myBatchSendTestFile.csv\"");
		 * //actions.build().perform(); for (String handle2 : driver.getWindowHandles())
		 * { System.out.println("this is window handle-2 below...");
		 * System.out.println(handle2);
		 * System.out.println("switching to this window handle...");
		 * driver.switchTo().window(handle2);
		 * //driver.switchTo().window(handle1.get(1));
		 * System.out.println("got here-2.6... \n"); } try { Thread.sleep(5000); } catch
		 * (InterruptedException e) { e.printStackTrace(); }
		 * driver.switchTo().activeElement().sendKeys("c:\\myBatchSendTestFile.csv");
		 * //driver.switchTo().window(arg0) //driver.switchTo().frame(arg0) try {
		 * Thread.sleep(5000); } catch (InterruptedException e) { e.printStackTrace(); }
		 * 
		 * //driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		 * //driver.findElement(By.id("convertButton"));
		 * 
		 * 
		 * System.out.println("got here-3... \n");
		 * //driver.findElement(By.cssSelector(".ng-isolate-scope > .btn")).click();
		 * driver.findElement(By.xpath(
		 * "//*[@id=\"content\"]/div/div/div/div[2]/form/div[4]/fieldset/div[1]/div[1]/div/div"
		 * )).click(); System.out.println("got here-4... \n");
		 * //driver.findElement(By.cssSelector(".col-xs-2 > .btn-default > .fa-square-o"
		 * )).click();
		 * driver.findElement(By.name("name")).sendKeys("myBatchSendTestFile.csv");
		 * System.out.println("got here-5... \n"); driver.findElement(By.
		 * cssSelector(".form-group:nth-child(1) > .col-xs-12 > .ng-pristine")).click();
		 * System.out.println("got here-6... \n"); { WebElement dropdown =
		 * driver.findElement(By.cssSelector(".col-xs-12 > .ng-untouched"));
		 * System.out.println("got here-7... \n");
		 * dropdown.findElement(By.xpath("//option[. = 'Demo message']")).click();
		 * System.out.println("got here-8... \n"); }
		 * driver.findElement(By.cssSelector(".col-xs-12 > .ng-untouched")).click();
		 * System.out.println("got here-9... \n");
		 * driver.findElement(By.name("batchcsv")).click();
		 * System.out.println("got here-10... \n"); try { Thread.sleep(5000); } catch
		 * (InterruptedException e) { e.printStackTrace(); }
		 * driver.findElement(By.cssSelector(".ng-isolate-scope > .btn")).click(); try {
		 * Thread.sleep(5000); } catch (InterruptedException e) { e.printStackTrace(); }
		 * driver.findElement(By.cssSelector(".btn-group > .btn:nth-child(2)")).click();
		 * try { Thread.sleep(5000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 * 
		 */
		
		// clicking-on Save button.
		//System.out.println("clicking-on Save button.... \n");
		//driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div[2]/form/div[4]/fieldset/div[2]/div/span/button")).click();
		
		// setting-up schedule of batch send.
		//*[@id="content"]/div/div/div/div[2]/form/div[1]/fieldset[1]/div[2]/div/div/div[1]/div/label[2]
		//#content > div > div > div > div.col-xs-9.ng-scope > form > div:nth-child(1) > fieldset.col-xs-8 > div:nth-child(2) > div > div > div.col-xs-4 > div > label:nth-child(2)
		//<label ng-click="batch.schedule.now = false" ng-class="{active: !batch.schedule.now}" class="btn btn-primary"><input type="radio">Scheduled</label>
		//<label ng-click="batch.schedule.now = false" ng-class="{active: !batch.schedule.now}" class="btn btn-primary"><input type="radio">Scheduled</label>
		//document.querySelector("#content > div > div > div > div.col-xs-9.ng-scope > form > div:nth-child(1) > fieldset.col-xs-8 > div:nth-child(2) > div > div > div.col-xs-4 > div > label:nth-child(2)")
		
		//click-on Scheduled button.
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("clicking-on Schedule button..1st-try.. \n");
		//label[class='btn btn-primary']/input[@value='MRS']
		//.//*label[text()='Scheduled']
		//driver.findElement(By.xpath(".//*[text()='Scheduled']")).click();
		driver.findElement(By.xpath(".//label[contains(.,'Scheduled')]")).click();
		//driver.findElement(By.xpath("//input[contains(text(), 'Scheduled')]")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("clicking-on Schedule button..2nd-try.. \n");		
		//driver.findElement(By.xpath("//input[contains(@type, 'radio')]")).click();
		driver.findElement(By.xpath("//label[contains(.,'Scheduled')]")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		driver.findElement(By.cssSelector(".glyphicon-calendar")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		driver.findElement(By.cssSelector(".glyphicon-time")).click();
		//driver.findElement(By.cssSelector("body > div.bootstrap-datetimepicker-widget.dropdown-menu.bottom.picker-open > ul > li.picker-switch.accordion-toggle > a > span")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

//		driver.findElement(By.cssSelector(".glyphicon-time")).click();
//		driver.findElement(By.cssSelector("span.glyphicon.glyphicon-time")).click();

		driver.findElement(By.cssSelector("td:nth-child(3) .glyphicon-chevron-up")).click();
		//driver.findElement(By.cssSelector("body > div.bootstrap-datetimepicker-widget.dropdown-menu.picker-open.bottom > ul > li.collapse.in > div > div.timepicker-picker > table > tbody > tr:nth-child(1) > td:nth-child(3) > a > span")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		driver.findElement(By.cssSelector("td:nth-child(3) .glyphicon-chevron-down")).click();
		//driver.findElement(By.cssSelector("body > div.bootstrap-datetimepicker-widget.dropdown-menu.picker-open.bottom > ul > li.collapse.in > div > div.timepicker-picker > table > tbody > tr:nth-child(3) > td:nth-child(3) > a > span")).click();
		
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		driver.findElement(By.cssSelector(".ng-isolate-scope > .btn")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		driver.findElement(By.cssSelector(".btn-lg:nth-child(2)")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		driver.findElement(By.cssSelector(".dropdown-sv:nth-child(1) .caret")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		//relocated click-on SAVE after scheduling and before Deploy button
		
		System.out.println("Skip, trying to scroll to the SAVE button... \n");
		
		/*
		 * System.out.println("Next, trying to scroll to the SAVE button... \n");
		 * //start-of-scrolling-down to SAVE button //WebElement ele =
		 * driver.findElement(By.xpath(
		 * "//*[@id='content']/div/div/div[2]/form/span/button")); WebElement ele =
		 * driver.findElement(By.xpath(
		 * "/html/body/div[2]/div/div/div/div[2]/form/div[4]/fieldset/div[2]/div/span/button"
		 * )); JavascriptExecutor js = (JavascriptExecutor) driver;
		 * js.executeScript("arguments[0].scrollIntoView();", ele);
		 * //end-of-scrolling-down to SAVE button
		 * System.out.println("Done scrolling to the SAVE button... \n");
		 * System.out.println("Now wait for 20 seconds... \n"); try {
		 * Thread.sleep(5000); } catch (InterruptedException e) { e.printStackTrace(); }
		 * 
		 */		
		// clicking-on Save button.
		System.out.println("clicking-on BS Save button.... \n");
		//driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div[2]/form/div[4]/fieldset/div[2]/div/span/button")).click();
		driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div[4]/fieldset/div[2]/div/span/button")).click();
		//driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div[4]/fieldset/div[2]/div/span/button")).click();
		
		//Clicking-on the Deploy button.
		System.out.println("clicking-on Deploy button..1st-try.. \n");
		driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div[2]/form/div[1]/fieldset[2]/div/div/button[2]")).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("failed code step line 789 - no such element \n");
		System.out.println("clicking-on Realtime Events...1st-try.. \n");
		driver.findElement(By.linkText("Realtime Events")).click();
		//driver.findElement(By.name("batchcsv")).sendKeys("myBatchSendTestFile.csv");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("clicking-on Realtime Events...2nd-try.. \n");
		driver.get("https://app.cit.vollyma.com/reports/realtime");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	    // Switch to the newly opened Tab.
	    driver.switchTo().window(tabs.get(2));
	    System.out.println("going to yopmail.com site. \n");
	    driver.get("http://yopmail.com");
	    
	    try {
		      Thread.sleep(1000);
		      // Thread.sleep(3000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	  //By finding all the web elements using iframe tag
	    List<WebElement> iframeElements = driver.findElements(By.tagName("iframeResult"));
	    System.out.println("Total number of iframes in yopmail are " + iframeElements.size());
	    
	  //By executing a java script
	    JavascriptExecutor exe = (JavascriptExecutor) driver;
	    Integer numberOfFrames = Integer.parseInt(exe.executeScript("return window.length").toString());
	    System.out.println("No. of iframes on the page are " + numberOfFrames);
	    
      //Assume driver is initialized properly. 
	    List<WebElement> elem = driver.findElements(By.tagName("iframe"));
	    System.out.println("Number of iframes in a page :" + elem.size());
	    for(WebElement el : elem){
	      //Returns the Id of a frame.
	        System.out.println("Frame Id :" + el.getAttribute("id"));
	      //Returns the Name of a frame.
	        System.out.println("Frame name :" + el.getAttribute("name"));
	    }
	    
		System.out.println("in yopmail.com site and enterring the target email. \n");
//yopmail.com "Check-Inbox" form-field
			 driver.findElement(By.xpath("//*[@id=\"login\"]")).click(); 

			 try {
			 Thread.sleep(1000); } 
			 catch (InterruptedException e) { e.printStackTrace(); }
						 
//yopmail.com write email address to the "Check-Inbox" form-field
			 driver.findElement(By.name("login")).sendKeys(ALFC1var);
			  
			  try { Thread.sleep(1000); } catch (InterruptedException e) {
			  e.printStackTrace(); }

//Click-on yopmail.com "Check Inbox" button.			  
			  driver.findElement(By.xpath("/html/body/div/div[2]/main/div[3]/div/div[1]/div[2]/div/div/form/div/div/div[4]/button/i")).click();
			  
			  try { Thread.sleep(3000); } catch (InterruptedException e) {
			  e.printStackTrace(); }
			 
	    //Click-on unsub(opt-out)
	    //switch to yopmail.com iframe in-order to click-on the opt-out link.
		//switch to yopmail.com iframe in-order to click-on the CLICK HERE button.
//		System.out.println("switching to iframe in yopmail.com site. \n");
		System.out.println("switching to iframe in yopmail.com site. \n");
		driver.switchTo().frame("ifmail");
		System.out.println("done switching to iframe in yopmail.com site. \n");
		//System.out.println("clicking-on the opt-out link in the yopmail.com site iframe. \n");
		System.out.println("clicking-on the CLICK HERE button in the yopmail.com site iframe. \n");
		/*
		 * for(WebElement el1 : elem1){ //Returns the Id of a frame.
		 * System.out.println("Frame Id :" + el1.getAttribute("id")); //Returns the Name
		 * of a frame. System.out.println("Frame name :" + el1.getAttribute("name")); }
		 */
		//System.out.println("now, again, clicking-on the opt-out link in the yopmail.com site iframe. \n");
		System.out.println("now, again, clicking-on the CLICK HERE button in the yopmail.com site iframe. \n");
	    
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//upper unsub-link
		//System.out.println("Clicking-on the upper unsub link in top-of-email.");
		System.out.println("No longer clicking-on the upper unsub link in top-of-email.");
	    //driver.findElement(By.linkText("Unsubscribe.")).click();		    
	    //System.out.println("Done Clicking-on the upper unsub link in top-of-email.");
		System.out.println("Clicking-on the CLICK HERE link in Yopmail.com email to produce a click-event.");
		//#mail > div > table > tbody > tr > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr > td > table > tbody > tr > td:nth-child(2) > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr > td > a
		///html/body/main/div/div/div/table/tbody/tr/td/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr/td/table/tbody/tr/td/a
		driver.findElement(By.linkText("CLICK HERE")).click();	
		
	    try {
		      Thread.sleep(1000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

	    
	    try {
		      Thread.sleep(1000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

		driver.switchTo().window(tabs.get(1)); // switches to new tab
		System.out.println("this is window tabs handle below...");
		System.out.println(tabs);

		try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		//Now clicking-on the Realtime Events...Realtime Reports...Opt-Outs TAB
		//System.out.println("clicking-on Realtime Events...Opt-Outs TAB.. \n");
	    //driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[1]/div/a[9]")).click();
		//Now clicking-on the Realtime Events...Realtime Reports...Clicks TAB
		System.out.println("clicking-on Realtime Events...Clicks TAB.. \n");
	    driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[1]/div/a[2]")).click();

	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }

		//Now clicking-on the Realtime Events...Realtime Reports top-most Email field
		System.out.println("clicking-on Realtime Events...Realtime Reports top-most Email field... \n");
	    //driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/table/tbody/tr[1]/td[2]")).click();
		//WebElement myReportVal = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/table/tbody/tr[1]/td[2]"));
		//full-Xpath: /html/body/div[2]/div/div/div[3]/div/table/tbody/tr[1]/td[2]
		//String myReportVal = driver.findElement(By.className("ng-binding")[1]).getAttribute("Name");
		
		//Clicks
		String myReportVal = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/table/tbody/tr[1]/td[4]")).getText();
		boolean myReportElement = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/table/tbody/tr[1]/td[4]")).isEnabled();
		//Opt-Outs
		//String myReportVal = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/table/tbody/tr[1]/td[2]")).getText();
		//boolean myReportElement = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/table/tbody/tr[1]/td[2]")).isEnabled();
		if(myReportElement) {
		System.out.println("If this is value of myReportElement: " + myReportElement);
		System.out.println("This is value of myReportVal: " + myReportVal);
		System.out.println("This is value of ALFC1var: " + ALFC1var);
		}
		//Assert.assertEquals(actual, expected);
		//Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/table/tbody/tr[1]/td[2]")), ALFC1var);
		Assert.assertEquals(myReportVal, ALFC1var);
		//System.out.println("Passed: the value of myReportVal equals ALFC1var: " + myReportVal + " , " + ALFC1var);
		
		
		if(myReportVal.equals(ALFC1var)) {
			System.out.println("Passed: the value of myReportValue equals ALFC1var: " + myReportVal + " , " + ALFC1var);
		}


		//Assert.assertTrue(myReportVal == ALFC1var);
		//Assert.assertSame
		//Assert.assertTrue
		
		/*
		 * if(isElementPresent(By.linkText("login"))){
		 * System.out.println("Login link is present"); } else{
		 * System.out.println("Login link is not present"); }
		 */
		
		if(myReportVal == ALFC1var) {
			System.out.println("Passed: the value of myReportVal equals ALFC1var: " + myReportVal);
		}
		

	    //Assert.assertTrue(condition);
	    
	    System.out.println(driver.getTitle());
	    //Logout.
	    //driver.findElement(By.xpath("//*[@id=\"header\"]/div/div[1]/div/div[2]/div/div/a")).click();
	    //driver.quit();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    System.out.println("End of Test-Case: " + testCaseName); 
	    System.out.println("<<End-Of-Chrome-Test>> \n");
		System.out.println("Test Completed Successfully!");
		System.out.println("QUIT & Close driver/browser.");
		//driver.close();
		//driver.quit();

	}
	/*
	@AfterTest
	public void tearDownTest() {
		//close browser
		driver.close();
		driver.quit();
		System.out.println("Test Completed Successfully!");
	}
	*/
}
