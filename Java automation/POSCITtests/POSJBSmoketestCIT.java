package vollygroupid.Mavenjava;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Test;
	//import org.junit.After;
	import java.time.Duration;
	import java.time.LocalDate;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.Date;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Select;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;
	import org.testng.annotations.AfterTest;

	public class POSJBSmoketestCIT {
		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}	
	  
	  
	  @Test
	  //set property
	  public void test() throws Exception{
		  
		    
		    String logHeader = "[QA run Test Log]";	
			String testCaseName = "POS CIT Ken Customer";	
		    System.out.println("Starting Test-Case: " + testCaseName); 
			String logWithNewLine0 = logHeader + System.getProperty("line.separator");
			String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
			LocalDateTime datetime = LocalDateTime.now();
			File myLog = new File("c:\\QATestLog.txt");

			String nuLine = "\r\n";
			//logWriter.write(nuLine);
					
			if (myLog.exists()) {
		        System.out.println("Log Exists, so opening log for appending."); 
				//logWriter.close();
				FileWriter logWriter = new FileWriter(myLog, true);
				//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
				// append & write QATestlog.
						//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
						// myWriter.write("Files in Java might be tricky, but it is fun enough!");
						//LocalDateTime datetime = LocalDateTime.now();
						try {
							System.out.println(datetime);
							// logWriter.write(datetime.getHour());
							logWriter.write(logWithNewLine0);
							logWriter.write(datetime.toString() + System.getProperty("line.separator"));
							//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
							logWriter.write(logWithNewLine1);
							//logWriter.write(nuLine);
							// myWriter.write(dataWithNewLine1);
							// myWriter.write(dataWithNewLine2);
							// myWriter.write(dataWithNewLine3);
							logWriter.close();
							System.out.println("Successfully wrote lines to the log file.");
						} catch (IOException e) {
							System.out.println("An error occurred.");
							e.printStackTrace();
						}
			}
			else {
		        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
				FileWriter logWriter = new FileWriter(myLog);
				// append & write QATestlog.
				//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
				// myWriter.write("Files in Java might be tricky, but it is fun enough!");
				//LocalDateTime datetime = LocalDateTime.now();
				try {
					System.out.println(datetime);
					// logWriter.write(datetime.getHour());
					logWriter.write(logWithNewLine0);
					logWriter.write(datetime.toString() + System.getProperty("line.separator"));
					//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
					logWriter.write(logWithNewLine1);
					// myWriter.write(dataWithNewLine1);
					// myWriter.write(dataWithNewLine2);
					// myWriter.write(dataWithNewLine3);
					logWriter.close();
					System.out.println("Successfully wrote lines to the log file.");
				} catch (IOException e) {
					System.out.println("An error occurred.");
					e.printStackTrace();
				}
			}

			  System.out.println("<<Start-Of-Chrome-Test>> \n");
				//System.setProperty(key, value)
			    System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
				//WebDriver driver = new ChromeDriver();	
			    
			    Calendar c = Calendar.getInstance();
			    int nmonth = c.get(Calendar.MONTH);
			    int nday = c.get(Calendar.DAY_OF_MONTH);
			    int nhour = c.get(Calendar.HOUR_OF_DAY);
			    int nmin = c.get(Calendar.MINUTE);
			    int nyear = c.get(Calendar.YEAR);
			    
			    nmonth=nmonth+1;
			    
			  //Adding-in Headless Chrome Options
				ChromeOptions options = new ChromeOptions();
				//options.addArguments("--headless");
				options.addArguments("window-size=1280,800");
				//WebDriver driver = new ChromeDriver(options);
				driver = new ChromeDriver(options);
				//driver.manage().window().maximize();
				
				driver.get("https://journeybank.vollyposstage.com/");
				//driver.get("https://journeybank.vollyposuat.com/");
				driver.manage().window().setSize(new Dimension(1936, 1056));
				driver.findElement(By.cssSelector(".option-col:nth-child(1) .active")).click();
	    
				driver.findElement(By.id("first_name-field")).sendKeys("Ken");
				driver.findElement(By.id("last_name-field")).sendKeys("Customer");
				driver.findElement(By.id("email-field")).sendKeys("kencustomer"+nmonth+nday+nhour+nmin+"@yopmail.com");
				driver.findElement(By.id("username-field")).click();
				driver.findElement(By.id("username-field")).sendKeys("kencustomer"+nmonth+nday+nhour+nmin);
				System.out.println("Username: kencustomer"+nmonth+nday+nhour+nmin);
			    driver.findElement(By.id("password")).click();
			    driver.findElement(By.id("password")).sendKeys("Passw0rd!");
			    driver.findElement(By.id("password-confirm")).click();
			    driver.findElement(By.id("password-confirm")).sendKeys("Passw0rd!");
			    System.out.println("Password: Passw0rd!\n");
			    driver.findElement(By.cssSelector(".btn")).click();
	    
			    try {
				      Thread.sleep(5000);
				    } catch (InterruptedException e) {
				      e.printStackTrace();
				    }  
			    
			    //confirmation page - click Start My Loan
			    driver.switchTo().defaultContent();
	            WebElement element = driver.findElement(By.xpath("//*[@id=\"wrapper\"]/div/div[2]/section/header/div/h1"));
			    String strng = element.getText();
			    assertEquals("Welcome Ken!", strng);
			    
			    System.out.println("Click Start My Loan button");
	            driver.findElement(By.xpath("//*[@id=\"main\"]/section/div[1]/div[2]/button/div/h3")).click();
	            
	  
	    System.out.println("Select Loan Officer");
	    driver.findElement(By.cssSelector(".btn-radio-custom:nth-child(1)")).click();
	    driver.findElement(By.cssSelector(".main-content")).click();
	    driver.findElement(By.cssSelector(".mt-4")).click();
	    //assertThat(driver.findElement(By.cssSelector(".mb-4:nth-child(1)")).getText(), is("Please select your loan officer"));
	    
	    //driver.findElement(By.cssSelector("#loan_officers-field > div > div.css-1wy0on6 > div > svg > path")).click();
	    //driver.findElement(By.xpath("//span[@name=\'loan_officer_id\']")).click();
	    driver.findElement(By.cssSelector(".css-8mmkcg")).click();
	    driver.findElement(By.id("react-select-2-option-6")).click();
	    //assertThat(driver.findElement(By.linkText("Continue")).getText(), is("Continue"));
	    driver.findElement(By.linkText("Continue")).click();
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    } 
	    
	    
	    System.out.println("Select Loan Type");
	    driver.findElement(By.cssSelector(".option-col:nth-child(1) .active")).click();
	    driver.findElement(By.cssSelector(".btn-primary-custom:nth-child(2)")).click();
	    //driver.findElement(By.linkText("Save & Continue")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    } 
	    
	    System.out.println("Do you have a Referral Partner?");
	    driver.findElement(By.cssSelector(".btn-primary-custom:nth-child(2)")).click();
	    driver.findElement(By.cssSelector("#react_referrer_section > div > section > footer > a")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    } 
	    
	    System.out.println("Will you have a Co-borrower?");
	    driver.findElement(By.cssSelector(".btn-primary-custom:nth-child(2)")).click();
	    driver.findElement(By.cssSelector("#react_coborrowers_section > section > footer > a")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    } 
	    
	    
	    System.out.println("Borrower Information section");
	    
	    driver.findElement(By.id("borrower_birthday-field")).click();
	    driver.findElement(By.id("borrower_birthday-field")).sendKeys("01/01/1971");
	    //driver.findElement(By.cssSelector(".col-lg-3 .jcf-select-opener")).click();
	    //driver.findElement(By.cssSelector(".col-lg-3 .jcf-select-text")).click();
	    //driver.findElement(By.cssSelector(".jcf-hover")).click();
	    
	    //driver.findElement(By.cssSelector(".col-lg-10 > .ml-2:nth-child(3) > .jcf-radio")).click();
	    //driver.findElement(By.cssSelector(".col-lg-4 .jcf-select-opener")).click();
	    
	    //Marital Status - Unmarried
	    driver.findElement(By.cssSelector("#react_borrowers_section > div > section:nth-child(3) > form > div:nth-child(2) > div.col-lg-3.col-md-12.mb-2.pl-0.pr-0 > div > div > div > span.jcf-select-text")).click();
	    driver.findElement(By.cssSelector("#react_borrowers_section > div > section:nth-child(3) > form > div:nth-child(2) > div.col-lg-3.col-md-12.mb-2.pl-0.pr-0 > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	      
	    
	    //years of school
	    driver.findElement(By.cssSelector("#react_borrowers_section > div > section:nth-child(3) > form > div:nth-child(3) > div:nth-child(1) > div > div > div > span.jcf-select-text")).click();
	    driver.findElement(By.cssSelector("#react_borrowers_section > div > section:nth-child(3) > form > div:nth-child(3) > div:nth-child(1) > div > div > div > div > div > span > div > span > ul > li:nth-child(6) > span")).click();
	    
	    driver.findElement(By.id("borrower_home_phone-field")).click();
	    driver.findElement(By.id("borrower_home_phone-field")).sendKeys("617-276-5835");
	    driver.findElement(By.id("borrower_mobile_phone-field")).click();
	    driver.findElement(By.id("borrower_mobile_phone-field")).sendKeys("781-654-2345");
	   
	    //US citizen
	    driver.findElement(By.xpath("//*[@id=\"react_borrowers_section\"]/div/section[1]/form/div[4]/div[1]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#react_borrowers_section > div > section:nth-child(3) > form > div:nth-child(4) > div:nth-child(1) > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	    
	   
	    driver.findElement(By.id("borrower_full_ssn-field")).click();
	    driver.findElement(By.id("borrower_full_ssn-field")).sendKeys("500-50-7000");
	    driver.findElement(By.cssSelector(".col > .ml-2:nth-child(3)")).click();
	    driver.findElement(By.cssSelector(".main-content:nth-child(4) .btn:nth-child(1)")).click();
	    
	    //Credit Authorization
	    driver.findElement(By.cssSelector(".main-content:nth-child(5) .btn:nth-child(1)")).click();
	    
	    //Insurance Authorization
	    driver.findElement(By.cssSelector("#react_borrowers_section > div > section:nth-child(5) > div.row.ml-0.mr-0.answer-wrap > button:nth-child(1)")).click();
	    
	  //Add Dependent
	    driver.findElement(By.cssSelector("#react_borrowers_section > div > div:nth-child(6) > section > section > div > button ")).click();
	    
	    driver.findElement(By.id("borrowerage-field")).click();
	    driver.findElement(By.id("borrowerage-field")).sendKeys("21");
	    
	    driver.findElement(By.cssSelector("#react_borrowers_section > div > div:nth-child(6) > section > section > form > div.col-12.col-md-7.pl-0 > div.pb-2 > div > button.btn-primary-custom-solid.btn.active")).click();
	    /*
	    {
	      WebElement elementb = driver.findElement(By.cssSelector(".btn-primary-custom-solid:nth-child(2)"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(elementb).clickAndHold().perform();
	    }
	    {
	      WebElement elementb = driver.findElement(By.cssSelector(".btn-primary-custom-solid:nth-child(2)"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(elementb).perform();
	    }
	    {
	      WebElement elementb = driver.findElement(By.cssSelector(".btn-primary-custom-solid:nth-child(2)"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(elementb).release().perform();
	    }
	    */
	    driver.findElement(By.cssSelector(".btn-primary-custom-solid:nth-child(2)")).click();
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    driver.findElement(By.linkText("Save & Continue")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Residence Section
	    
	    System.out.println("Residence section");
	    //assertThat(driver.findElement(By.cssSelector("h1")).getText(), is("Residence"));
	    driver.findElement(By.id("street-field")).click();
	    driver.findElement(By.id("street-field")).sendKeys("10655 Birch St.");
	    //driver.findElement(By.id("unit-field")).sendKeys("1100");
	    driver.findElement(By.id("city-field")).sendKeys("Burbank");
	    
	    driver.findElement(By.cssSelector("#react_residence_section > div > div:nth-child(3) > section > div.row.mb-4 > div > form > div:nth-child(2) > div:nth-child(2) > div > div > div > span.jcf-select-text")).click();
	     
	   	driver.findElement(By.cssSelector("#react_residence_section > div > div:nth-child(3) > section > div.row.mb-4 > div > form > div:nth-child(2) > div:nth-child(2) > div > div > div > div > div > span > div > span > ul > li:nth-child(6) > span")).click();
	    
	    driver.findElement(By.id("zipcode-field")).sendKeys("91502");
	    driver.findElement(By.cssSelector(".date-field .ml-2")).click();
	    //assertThat(driver.findElement(By.cssSelector(".date-field .ml-2")).getText(), is("When did you move here? *"));
	    driver.findElement(By.id("start_date-field")).click();
	    driver.findElement(By.id("start_date-field")).sendKeys("12/30/1999");
	    //assertThat(driver.findElement(By.cssSelector(".col-md-12 .ml-2")).getText(), is("Do you own, rent, or live for free at this residence? *"));
	    driver.findElement(By.cssSelector(".jcf-option-placeholder:nth-child(2)")).click();
	    driver.findElement(By.cssSelector("#react_residence_section > div > div:nth-child(3) > section > div.row.mb-4 > div > form > div:nth-child(3) > div.col-xl-4.col-lg-6.col-md-12.col-sm-12.pl-0.pr-0 > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	   
	    //mailing address question
	    driver.findElement(By.xpath("//*[@id=\"react_residence_section\"]/div/div[3]/section/div[2]/div/form/div[4]/div/div/label[2]/label/span[1]")).click();
	    //assertThat(driver.findElement(By.cssSelector(".btn-wrap > .btn")).getText(), is("Save"));
	    
	    //click Save button
	    driver.findElement(By.cssSelector("#react_residence_section > div > div:nth-child(3) > section > div.row.mb-4 > div > form > div.d-flex.justify-content-end.align-items-center.pb-0.pt-2 > div > button")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		      
	    //Click Save + Continue button
	    driver.findElement(By.cssSelector("#react_residence_section > div > section > footer > a")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Residence History
	    System.out.println("Residence History section");
	    
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Proposed Property section
	    
	    System.out.println("Proposed Property section");
	    driver.findElement(By.cssSelector("h1")).click();
	    //assertThat(driver.findElement(By.cssSelector("h1")).getText(), is("Proposed Property"));
	    driver.findElement(By.cssSelector(".main-content")).click();
	    //assertThat(driver.findElement(By.cssSelector(".mb-2")).getText(), is("Tell us about the property you are interested in"));
	    driver.findElement(By.id("street-field")).sendKeys("56 Roberts Drive");
	    //driver.findElement(By.id("unit-field")).sendKeys("1100");
	    driver.findElement(By.id("city-field")).sendKeys("Bedford");
	    driver.findElement(By.xpath("//form[@id=\'proposed-property\']/div[2]/div[2]/div/div/div/span[2]")).click();
	    driver.findElement(By.cssSelector("#proposed-property > div:nth-child(2) > div:nth-child(2) > div > div > div > div > div > span > div > span > ul > li:nth-child(23) > span")).click();
	    
	    //driver.findElement(By.xpath("//span[@name=\'state_id\']")).click();
	    //assertThat(driver.findElement(By.xpath("//label[contains(.,\'Zipcode *\')]")).getText(), is("Zipcode *"));
	    driver.findElement(By.id("zipcode-field")).sendKeys("01730");
	    //assertThat(driver.findElement(By.cssSelector(".form-row:nth-child(3) label")).getText(), is("County *"));
	    driver.findElement(By.id("county-field")).click();
	    driver.findElement(By.id("county-field")).sendKeys("Middlesex");
	    
	    //Property Type
	    driver.findElement(By.xpath("//*[@id=\"proposed-property\"]/div[4]/div[1]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#proposed-property > div:nth-child(4) > div:nth-child(1) > div > div > div > div > div > span > div > span > ul > li:nth-child(2) > span")).click();
	    //Residence Type
	    driver.findElement(By.xpath("//*[@id=\"proposed-property\"]/div[4]/div[2]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#proposed-property > div:nth-child(4) > div:nth-child(2) > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	    //Number of Units
	    driver.findElement(By.xpath("//*[@id=\"proposed-property\"]/div[3]/div[2]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#proposed-property > div:nth-child(3) > div:nth-child(2) > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	    //Expected Closing Date
	    driver.findElement(By.id("settlement_date-field")).click();
	    driver.findElement(By.id("settlement_date-field")).sendKeys("12/31/2022");
	    //driver.findElement(By.id("settlement_date-field")).sendKeys(nmonth+ '/' +nday+ '/' +nyear);
	    
	    //Sales Price
	    driver.findElement(By.id("sales_price-field")).click();
	    driver.findElement(By.id("sales_price-field")).sendKeys("595000");
	    driver.findElement(By.id("loan_amount-field")).click();
	    driver.findElement(By.id("loan_amount-field")).sendKeys("300000");
	    driver.findElement(By.id("estimated_value-field")).click();
	    driver.findElement(By.id("estimated_value-field")).sendKeys("600000");
	    driver.findElement(By.id("management_company-field")).click();
	    driver.findElement(By.id("management_company-field")).sendKeys("Alanco");
	    driver.findElement(By.id("year_built-field")).sendKeys("2021");
	    driver.findElement(By.id("year_built-field")).click();
	    driver.findElement(By.id("year_acquired-field")).click();
	    driver.findElement(By.id("year_acquired-field")).sendKeys("2022");
	    
	    //manufactured home radio button
	    driver.findElement(By.cssSelector("#proposed-property > div:nth-child(8) > div:nth-child(2) > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    
	    //Title Held
	    driver.findElement(By.cssSelector("#proposed-property > div:nth-child(10) > div:nth-child(1) > div > div > div > span.jcf-select-text")).click();
	    driver.findElement(By.cssSelector("#proposed-property > div:nth-child(10) > div:nth-child(1) > div > div > div > div > div > span > span > ul > li:nth-child(6) > span")).click();
	    
	    //Estate will be held in
	    driver.findElement(By.cssSelector("#proposed-property > div:nth-child(10) > div:nth-child(2) > div > div > div > span.jcf-select-text")).click();
	    driver.findElement(By.cssSelector("#proposed-property > div:nth-child(10) > div:nth-child(2) > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	    
	    driver.findElement(By.id("leasehold_expiration_date-field")).click();
	    driver.findElement(By.id("leasehold_expiration_date-field")).sendKeys("12/12/2022");
	    driver.findElement(By.id("title_holder_name-field")).click();
	    driver.findElement(By.id("title_holder_name-field")).sendKeys("Alan Beck");
	    driver.findElement(By.id("original_cost-field")).click();
	    driver.findElement(By.id("original_cost-field")).sendKeys("500000");
	    driver.findElement(By.id("existing_liens_amount-field")).click();
	    driver.findElement(By.id("existing_liens_amount-field")).sendKeys("10000");
	    driver.findElement(By.id("lot_present_value-field")).click();
	    driver.findElement(By.id("lot_present_value-field")).sendKeys("563000");
	    driver.findElement(By.id("cost_of_improvements-field")).click();
	    driver.findElement(By.id("cost_of_improvements-field")).sendKeys("50000");
	    
	    driver.findElement(By.cssSelector("#react_proposed_propery_section > div > section > footer > div > button")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Income
	    System.out.println("Income section");
	    driver.findElement(By.cssSelector("h1")).click();
	    //assertThat(driver.findElement(By.cssSelector("h1")).getText(), is("Proposed Property"));
	    driver.findElement(By.cssSelector(".main-content")).click();
	    
	    
	    //Add Employer
	    driver.findElement(By.cssSelector("#react_employment_section > section:nth-child(3) > div.row.ml-0.mr-0.answer-wrap > button")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    System.out.println("Add Current Employer");
	    driver.findElement(By.id("employer_name-field")).click();
	    driver.findElement(By.id("employer_name-field")).sendKeys("Testco");
	    driver.findElement(By.id("street-field")).click();
	    driver.findElement(By.id("street-field")).sendKeys("100 Great Road");
	    driver.findElement(By.id("city-field")).click();
	    driver.findElement(By.id("city-field")).sendKeys("Bedford");
	   
	 
	    driver.findElement(By.xpath("//*[@id=\"employer-add\"]/div[4]/div[2]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#employer-add > div:nth-child(4) > div:nth-child(2) > div > div > div > div > div > span > div > span > ul > li:nth-child(23) > span")).click();
	    
	    driver.findElement(By.id("zipcode-field")).click();
	    driver.findElement(By.id("zipcode-field")).sendKeys("01730");
	    //assertThat(driver.findElement(By.cssSelector(".form-row:nth-child(3) > .col-lg-4:nth-child(2) .ml-2")).getText(), is("Number of Units"));
	    driver.findElement(By.id("borrower_phone-field")).click();
	    driver.findElement(By.id("borrower_phone-field")).sendKeys("781-275-3256");
	    //assertThat(driver.findElement(By.cssSelector(".date-field .ml-2")).getText(), is("Expected Closing Date *"));
	    driver.findElement(By.id("position-field")).click();
	    driver.findElement(By.id("position-field")).sendKeys("QA Engineer");
	    driver.findElement(By.id("start_date-field")).click();
	    driver.findElement(By.id("start_date-field")).sendKeys("03/17/2018");
	    
	    //Years in line of work
	    driver.findElement(By.xpath("//*[@id=\"employer-add\"]/div[6]/div[2]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#employer-add > div:nth-child(6) > div:nth-child(2) > div > div > div > div > div > span > div > span > ul > li:nth-child(4) > span")).click();
	    
	    //Months in line of work
	    driver.findElement(By.xpath("//*[@id=\"employer-add\"]/div[6]/div[3]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#employer-add > div:nth-child(6) > div:nth-child(3) > div > div > div > div > div > span > div > span > ul > li:nth-child(4) > span")).click();
	    
	    //Self Employed, Employed By
	    driver.findElement(By.cssSelector("#employer-add > div:nth-child(7) > div:nth-child(1) > div > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#employer-add > div:nth-child(7) > div:nth-child(2) > div > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    
	    //Monthly Income
	    driver.findElement(By.id("monthly_income_base-field")).click();
	    driver.findElement(By.id("monthly_income_base-field")).sendKeys("5000");
	    driver.findElement(By.id("monthly_income_overtime-field")).click();
	    driver.findElement(By.id("monthly_income_overtime-field")).sendKeys("100");
	    driver.findElement(By.id("monthly_income_bonus-field")).click();
	    driver.findElement(By.id("monthly_income_bonus-field")).sendKeys("100");
	    driver.findElement(By.id("monthly_income_commission-field")).click();
	    driver.findElement(By.id("monthly_income_commission-field")).sendKeys("0");
	    driver.findElement(By.id("monthly_income_military-field")).click();
	    driver.findElement(By.id("monthly_income_military-field")).sendKeys("100");
	    driver.findElement(By.id("monthly_income_other-field")).click();
	    driver.findElement(By.id("monthly_income_other-field")).sendKeys("50");
	    
	    driver.findElement(By.cssSelector("#employer-add > div.d-flex.justify-content-end.align-items-center > div > button.btn-primary-custom-solid.btn.active")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Add Other Income
	    System.out.println("Add Other Income");
	    driver.findElement(By.cssSelector("#react_employment_section > section:nth-child(4) > div > section > div > button")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Other Income - Source Alimony
	    driver.findElement(By.xpath("//*[@id=\"react_employment_section\"]/section[2]/div/form/div[1]/div[2]/div/div/div/div/span[1]")).click();;
	    driver.findElement(By.cssSelector("#react_employment_section > section:nth-child(4) > div > form > div.col-12.col-md-4.col-lg-5 > div.pb-2 > div > div > div > div > div > div > span > div > span > ul > li:nth-child(3) > span")).click();
	    
	    driver.findElement(By.id("amount-field")).click();
	    driver.findElement(By.id("amount-field")).sendKeys("500");
	    
	    driver.findElement(By.cssSelector("#react_employment_section > section:nth-child(4) > div > form > div.col-12.pt-3.pt-md-0.col-md-4.col-lg-3 > div.pb-2 > div > button.btn-primary-custom-solid.btn.active")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Save + Continue
	    driver.findElement(By.cssSelector("#react_employment_section > section.main-content.container.pb-0 > footer > a")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Expenses
	    System.out.println("Add Expense");
	    driver.findElement(By.cssSelector("#react_expenses_section > section.main-content.container.pt-3 > div.row.ml-0.mr-0.pt-0.answer-wrap > button")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Add Expense Alimony
	    driver.findElement(By.xpath("//*[@id=\"react_expenses_section\"]/section[2]/form/div[1]/div[2]/div/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#react_expenses_section > section.main-content.container.pt-3 > form > div:nth-child(1) > div.pb-2 > div > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	    
	    driver.findElement(By.id("amount-field")).click();
	    driver.findElement(By.id("amount-field")).sendKeys("1000");
	    
	    try {
		      Thread.sleep(2000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	  //Save
	    driver.findElement(By.cssSelector("#react_expenses_section > section.main-content.container.pt-3 > form > div.col-12.pt-3.pt-md-0 > div.pb-2 > div > button.btn-primary-custom-solid.btn.active")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Save + Continue
	    driver.findElement(By.cssSelector("#react_expenses_section > section:nth-child(5) > footer > div > a")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Assets section
	    System.out.println("Add Assets");
	    
	    //Add Asset Account
	    driver.findElement(By.cssSelector("#react_assets_section > div > section:nth-child(5) > div.row.ml-0.mr-0.answer-wrap.pt-0 > button")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Add Traditional Assets
	    //Account Type
	    driver.findElement(By.xpath("//*[@id=\"react_assets_section\"]/div/section[3]/form/div[1]/div[2]/div/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#react_assets_section > div > section:nth-child(5) > form > div:nth-child(1) > div.pb-2 > div > div > div > div > div > div > span > div > span > ul > li:nth-child(2) > span")).click();
	    
	    driver.findElement(By.id("institution_name-field")).click();
	    driver.findElement(By.id("institution_name-field")).sendKeys("Bank of America");
	    driver.findElement(By.id("account_number-field")).click();
	    driver.findElement(By.id("account_number-field")).sendKeys("1234567890");
	    driver.findElement(By.id("market_value-field")).click();
	    driver.findElement(By.id("market_value-field")).sendKeys("1000000");
	    
	    //click Save
	    driver.findElement(By.cssSelector("#react_assets_section > div > section:nth-child(5) > form > div.col-sm-12.col-md-4.pt-3.pt-md-0 > div.pb-2 > div > button.btn-primary-custom-solid.btn.active")).click();
	   
	   
	    //Add Non Traditional Assets
	    System.out.println("Add Non traditional Assets");
	   
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    driver.findElement(By.xpath("//*[@id=\"react_assets_section\"]/div/section[5]/div[2]/button")).click();
	    
	    driver.findElement(By.xpath("//*[@id=\"react_assets_section\"]/div/section[5]/form/div[1]/div[2]/div/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#react_assets_section > div > section:nth-child(7) > form > div:nth-child(1) > div.pb-2 > div > div > div > div > div > div > span > div > span > ul > li:nth-child(2) > span")).click();
	    
	    driver.findElement(By.id("market_value-field")).click();
	    driver.findElement(By.id("market_value-field")).sendKeys("12760");
	    
	    //click Save
	    driver.findElement(By.cssSelector("#react_assets_section > div > section:nth-child(7) > form > div.col-12.col-md-3.pt-3.pt-md-0 > div.pb-2 > div > button.btn-primary-custom-solid.btn.active")).click();
		   
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //click Save + Continue
	    driver.findElement(By.cssSelector("#react_assets_section > div > section:nth-child(8) > footer > div > a")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Real Estate Owned
	    System.out.println("Add Real Estate Owned");
	    //assertThat(driver.findElement(By.cssSelector("h1")).getText(), is("Real Estate Owned"));
	   
	    //assertThat(driver.findElement(By.cssSelector(".mb-4:nth-child(1)")).getText(), is("Do you own any real estate assets?"));
	    
	    driver.findElement(By.cssSelector("#react_real_estate_section > div > section:nth-child(6) > div > button")).click();
	   
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    driver.findElement(By.id("street-field")).click();
	    driver.findElement(By.id("street-field")).sendKeys("53 Commerce Way");
	    driver.findElement(By.id("city-field")).click();
	    driver.findElement(By.id("city-field")).sendKeys("Woburn");
	    
	    //State
	    driver.findElement(By.xpath("//*[@id=\"real-estate-add\"]/div[2]/div[2]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#real-estate-add > div:nth-child(2) > div:nth-child(2) > div > div > div > div > div > span > div > span > ul > li:nth-child(23) > span")).click();
	    
	    
	    driver.findElement(By.id("zipcode-field")).click();
	    driver.findElement(By.id("zipcode-field")).sendKeys("01801");
	    
	    //Property Type
	    driver.findElement(By.xpath("//*[@id=\"real-estate-add\"]/div[3]/div[1]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#real-estate-add > div:nth-child(3) > div:nth-child(1) > div > div > div > div > div > span > div > span > ul > li:nth-child(2) > span")).click();
	    
	    //Property Disposition
	    driver.findElement(By.xpath("//*[@id=\"real-estate-add\"]/div[3]/div[2]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#real-estate-add > div:nth-child(3) > div:nth-child(2) > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	    
	    
	    //Current Occupancy Type
	    driver.findElement(By.xpath("//*[@id=\"real-estate-add\"]/div[3]/div[3]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#real-estate-add > div:nth-child(3) > div:nth-child(3) > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	    
	    //Intended Occupancy Type
	    driver.findElement(By.xpath("//*[@id=\"real-estate-add\"]/div[3]/div[4]/div/div/div/span[1]")).click();
	    driver.findElement(By.cssSelector("#real-estate-add > div:nth-child(3) > div:nth-child(4) > div > div > div > div > div > span > span > ul > li:nth-child(2) > span")).click();
	    
	    driver.findElement(By.id("market_value-field")).click();
	    driver.findElement(By.id("market_value-field")).sendKeys("250000");
	    driver.findElement(By.id("monthly_rental_income-field")).click();
	    driver.findElement(By.id("monthly_rental_income-field")).sendKeys("2000");
	    driver.findElement(By.id("monthly_expenses-field")).click();
	    driver.findElement(By.id("monthly_expenses-field")).sendKeys("560");
	    
	    //Mortgage Loan
	    driver.findElement(By.cssSelector("#real-estate-add > div:nth-child(5) > div > div > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    
	    //Save
	    driver.findElement(By.cssSelector("#real-estate-add > div.d-flex.justify-content-end.align-items-center > div > button.btn-primary-custom-solid.btn.active")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Save + Continue
	    driver.findElement(By.cssSelector("#react_real_estate_section > div > section:nth-child(7) > footer > div > a")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Legal Declarations
	    System.out.println("Add Legal Declarations");
	    
	    driver.findElement(By.cssSelector("h1")).click();
	    //assertThat(driver.findElement(By.cssSelector("h1")).getText(), is("Legal Declarations"));
	    driver.findElement(By.cssSelector(".mb-5")).click();
	    //assertThat(driver.findElement(By.cssSelector(".mb-5")).getText(), is("Your Declarations"));
	    
	    //About this property
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(1) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(2) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(3) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(4) > div > div.col-md-2 > div:nth-child(1) > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(4) > div > div.col-md-2 > div.pt-3.input-group > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(5) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    
	    //About Your Finances
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(7) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(8) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(9) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(10) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(11) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(12) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(13) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(14) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    //driver.findElement(By.cssSelector("#react_declarations_section > section > div > div > div > form > div:nth-child(15) > div > div.col-md-2 > div > label:nth-child(2) > label > span.jcf-radio.jcf-unchecked")).click();
	    
	    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
	    //Save + Continue
	    driver.findElement(By.cssSelector("#react_declarations_section > section > footer > div > a")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Demographic Information
	    System.out.println("Add Demographic Information");
	   
	    driver.findElement(By.cssSelector("h1")).click();
	    //assertThat(driver.findElement(By.cssSelector("h1")).getText(), is("Demographic Information"));
	    //White, Male, non Hispanic
	    driver.findElement(By.cssSelector("#borrower-demographics-form > div:nth-child(12) > div > label > input")).click();
	    driver.findElement(By.cssSelector("#borrower-demographics-form > div:nth-child(17) > div > label > input")).click();
	    driver.findElement(By.cssSelector("#borrower-demographics-form > div:nth-child(40) > div > label > input")).click();
	   
	    
	    //assertThat(driver.findElement(By.cssSelector(".btn")).getText(), is("Save & Continue"));
	    //Click Save + Continue
	    driver.findElement(By.cssSelector("#main > section:nth-child(4) > footer > div > button")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Review Summary
	    System.out.println("Verify Review Summary");
	    
	    driver.findElement(By.id("main")).click();
	    
	    //Click Save + Continue
	    driver.findElement(By.cssSelector("#assets-section > div.row.ml-0.mr-0.mt-md-6 > a")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	  //Review Summary
	    System.out.println("Verify Acknowlegements and Legal Consent");
	    
	    driver.findElement(By.id("borrower_first_name-field")).click();
	    driver.findElement(By.id("borrower_first_name-field")).sendKeys("Ken");
	    driver.findElement(By.id("borrower_last_name-field")).click();
	    driver.findElement(By.id("borrower_last_name-field")).sendKeys("Customer");
	    
	    driver.findElement(By.cssSelector("#borrower-acknowledgements > div:nth-child(3)")).click();
	    
	    driver.findElement(By.cssSelector("#main > section:nth-child(3) > div.answer-wrap")).click();
	  
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Click Agree
	    driver.findElement(By.cssSelector("#borrower-agree")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	  //Save + Continue
	    driver.findElement(By.cssSelector("#continue_button")).click();
	    
	    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    //Submit Application
	    System.out.println("Submit Application");
	    driver.findElement(By.cssSelector("#main > section > footer > div > button")).click();
	    
	    try {
		      Thread.sleep(10000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
	    
	    System.out.println("Loan Completed");
	    
	    
	  }
	}
