package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Test;
	//import org.junit.After;
	import static org.junit.Assert.*;
	import static org.hamcrest.CoreMatchers.is;
	import static org.hamcrest.core.IsNot.not;
	import java.time.Duration;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class ProdLogin {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM Prod Login";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		    //WebDriver driver = new ChromeDriver();		
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
			
			
			
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    //driver.findElement(By.name("UserLogin")).click();
		    //driver.findElement(By.name("username")).sendKeys("acronin@myvolly.com");
		    //driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    //driver.findElement(By.name("UserPassword")).click();
		    //driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    //driver.findElement(By.name("UserCorp")).click();
		    //driver.findElement(By.name("UserCorp")).sendKeys("ABC");

		    
		    
		  //Login forgot password
		    System.out.println("Login forgot password");
			
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    //driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }

		    driver.findElement(By.id("spanReset")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		  
		    assertThat(driver.findElement(By.id("spanForgotPasswordText")).getText(), is("An email with a link to the page where you can enter a new password will be sent to your email address. If you do not receive it within 10 minutes please contact VollyCare@myvolly.com or call (866) 435-7050."));
		    driver.findElement(By.cssSelector(".control-group:nth-child(4) .btn")).click();
		    assertThat(driver.switchTo().alert().getText(), is("Please enter your email address before clicking OK."));
		    driver.switchTo().alert().accept();
		    
		    driver.findElement(By.id("idEmailAddress")).click();
		    driver.findElement(By.id("idEmailAddress")).sendKeys("abeck@myvolly.com");
		    driver.findElement(By.cssSelector("#divForgotPassword .btn")).click();
		    driver.close();
		    
		    //Set<String> wh = driver.getWindowHandles();
			//System.out.println("this is value of wh: " + wh);
			//String currentWindowHandle = driver.getWindowHandle();
			//System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    //vars.put(currentWindowHandle, waitForWindow(2000));
		    
		  //Login forgot password no email 
		    System.out.println("Login forgot password no email");
		    
			//ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
			
		 
		    
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).clear();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    //driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).clear();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		    
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("spanReset")).click();
		    
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    assertThat(driver.findElement(By.id("spanForgotPasswordText")).getText(), is("An email with a link to the page where you can enter a new password will be sent to your email address. If you do not receive it within 10 minutes please contact VollyCare@myvolly.com or call (866) 435-7050."));
		    //driver.findElement(By.cssSelector(".control-group:nth-child(4) .btn")).click();
		    driver.findElement(By.id("idEmailAddress")).click();
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[1]/div[4]/div/button")).click();
		    
		    System.out.println("Login forgot password alert");
		    assertThat(driver.switchTo().alert().getText(), is("Please enter your email address before clicking OK."));  
		    driver.switchTo().alert().accept();
		    //driver.switchTo().alert().dismiss();
		    
		    driver.findElement(By.linkText("cancel")).click();
		    
		    driver.close();
		    
		    //Login forgot password no user
		    System.out.println("Login forgot password no user");
		    
			//ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
			
			
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).clear();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    //driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).clear();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    driver.findElement(By.id("spanReset")).click();
		    assertThat(driver.findElement(By.id("spanForgotPasswordText")).getText(), is("An email with a link to the page where you can enter a new password will be sent to your email address. If you do not receive it within 10 minutes please contact VollyCare@myvolly.com or call (866) 435-7050."));
		    driver.findElement(By.id("idLoginForgotPassword")).clear();
		    driver.findElement(By.id("idEmailAddress")).sendKeys("abeck@myvolly.com");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[1]/div[4]/div/button")).click();
		    
		    assertThat(driver.switchTo().alert().getText(), is("Please enter your Login ID before clicking OK."));
		    driver.switchTo().alert().accept();
		    
		    
		    driver.findElement(By.linkText("cancel")).click();
		    driver.close();
		    
		    
		  //Login no corpcode 
		    System.out.println("Login no corpcode");
		    
		  //ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
			
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).clear();
		    //driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		      
		  //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //assertThat(driver.switchTo().alert().getText(), is("Please fill out this field."));
		    //driver.switchTo().alert().accept();
		    
		    
		    driver.close();
		    
		  //Login no password 
		    System.out.println("Login no password");
		    
		  //ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
		    
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    //driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		      
		  //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //assertThat(driver.switchTo().alert().getText(), is("Please fill out this field."));
		    //driver.switchTo().alert().accept();
	
		    driver.close();
		    
		    
		    
		  //Login no username 
		    System.out.println("Login no username");
		    
		  //ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
			
		    driver.findElement(By.name("UserLogin")).click();
		    //driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		      
		  //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //close all windows
		    String parent=driver.getWindowHandle();
		    
		    Set<String>s=driver.getWindowHandles();

		 // Now iterate using Iterator
		 Iterator<String> I1= s.iterator();

		 while(I1.hasNext())
		 {

		 String child_window=I1.next();


		 if(!parent.equals(child_window))
		 {
		 driver.switchTo().window(child_window);

		 System.out.println(driver.switchTo().window(child_window).getTitle());

		 driver.close();
		 }
		 }
		 if (myLog.exists()) {
		    	System.out.println("Log Exists, so opening log for appending.");
		    	//logWriter.close();
		    	FileWriter logWriter = new FileWriter(myLog, true);
		    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
		    logWriter.write(logWithNewLine2);
		    logWriter.close();
		    }
		    
		    System.out.println("Login Successful - Login Test PASS");
		   driver.close();
		   driver.quit();

		    
	}
		    

		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			// driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}
