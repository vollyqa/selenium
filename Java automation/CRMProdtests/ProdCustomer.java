package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Test;
	//import org.junit.After;
	import static org.junit.Assert.*;
	import static org.hamcrest.CoreMatchers.is;
	import static org.hamcrest.core.IsNot.not;
	import java.time.DayOfWeek;
	import java.time.Duration;
	import java.time.Month;
	import java.time.LocalDate;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.Date;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class ProdCustomer {

		WebDriver driver = null;


		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}


	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM Prod Customer";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			//WebDriver driver = new ChromeDriver();		
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			
			driver.get("https://vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		    
		    //click-on Login-button
			System.out.println("click Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> wh = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandle = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    vars.put(currentWindowHandle, waitForWindow(2000));
		    
		    //#divCustomerManagerBody > div.site-menu > div > div:nth-child(3) > button
		    //click-on Customers TAB
			System.out.println("click Customers TAB");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
		    try {
			   Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    Calendar c = Calendar.getInstance();
		    int nmonth = c.get(Calendar.MONTH);
		    int nday = c.get(Calendar.DAY_OF_MONTH);
		    int nhour = c.get(Calendar.HOUR_OF_DAY);
		    int nmin = c.get(Calendar.MINUTE);
		    nmonth=nmonth+1;
		    
		    //click Add Customer
		    System.out.println("click Add Customer");
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("#mycontroller > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(7) > a")).click();
		    driver.switchTo().defaultContent();
		    
		    //Enter Customer information
		    System.out.println("Enter Customer information");
		    
		    	   
		    driver.findElement(By.id("txtFirstName")).click();
		    driver.findElement(By.id("txtFirstName")).sendKeys("Test");
		    driver.findElement(By.id("txtLastName")).click();
		    driver.findElement(By.id("txtLastName")).sendKeys("Customer"+nmonth+nday+nhour+nmin);
		    driver.findElement(By.id("txtEmailAddress")).click();
		    driver.findElement(By.id("txtEmailAddress")).sendKeys("customer"+nmonth+nday+nhour+nmin+"@yopmail.com");
		    driver.findElement(By.xpath("/html/body/div[11]/div/div[3]/button[1]")).click();
		    driver.switchTo().defaultContent();
		    
		    
		    //click on Field Search Options
		    //verify customer menu selections are functioning correctly
		    System.out.println("click-on Field Search Options");
		   
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    
		    //assertThat(driver.findElement(By.cssSelector("#GroupType")).getText(), is("Customers"));
		    driver.findElement(By.cssSelector(".filteroptions:nth-child(1) tr:nth-child(3) > td:nth-child(4) > .input-xlarge")).click();
		    driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]/select")).click();
		    driver.findElement(By.xpath("//option[@value=\'0\']")).click();
		    driver.findElement(By.cssSelector(".filteroptions:nth-child(1) td:nth-child(2) > .input-xlarge")).click();
		    driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[3]/td[2]/select/option[4]")).click();
		    driver.findElement(By.id("idsearchbutton")).click(); 
		    
		    driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[3]/td[2]/select")).click();
		    {
		      WebElement dropdown = driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[3]/td[2]/select"));
		      dropdown.findElement(By.xpath("//option[. = 'Construction']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty > option:nth-child(3)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		        
		    driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'Construction']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-dirty > option:nth-child(4)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'Construction/Perm']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-dirty > option:nth-child(5)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'Construction/Perm']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-dirty > option:nth-child(6)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'ConstructionOnly (ENC)']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-dirty > option:nth-child(2)")).click();
		   
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(2) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-dirty > option:nth-child(1)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    
	
		    
		    
		    driver.findElement(By.id("idDateFrom")).click();
		    //driver.findElement(By.id("idDateFrom")).sendKeys("1/1/2019");
		    driver.findElement(By.id("idDateTo")).click();
		    //driver.findElement(By.id("idDateTo")).sendKeys("3/3/2021");
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.id("idDateFrom")).click();
		    //driver.findElement(By.id("idDateFrom")).sendKeys("1");
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.id("idDateTo")).click();
		    //driver.findElement(By.id("idDateTo")).sendKeys("1");
		    driver.findElement(By.id("idsearchbutton")).click();
		    
		    driver.findElement(By.cssSelector(".filteroptions:nth-child(1) tr:nth-child(2) .input-xlarge")).click();
		    driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]/select")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    
		    driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]/select")).click();
		    {
		      WebElement dropdown = driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]/select"));
		      dropdown.findElement(By.xpath("//option[. = 'Conventional']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty > option:nth-child(3)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'Conventional']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(4) > .ng-dirty > option:nth-child(4)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'Conventional (ENC)']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(4) > .ng-dirty > option:nth-child(5)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'FHA']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(4) > .ng-dirty > option:nth-child(6)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'VA']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(4) > .ng-dirty > option:nth-child(7)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(2) .ng-dirty > option:nth-child(1)")).click();
		    driver.findElement(By.cssSelector(".filteroptions:nth-child(1) td:nth-child(4) > .ng-pristine")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(4) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(4) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = 'Active']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(4) > .ng-touched")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    //driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(4) > .ng-touched")).click();
		    //{
		      //WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(4) > .ng-touched"));
		      //dropdown.findElement(By.xpath("//option[. = 'Inactive']")).click();
		    //}
		    //driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(4) > .ng-touched")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(4) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(4) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(4) > .ng-touched")).click();
		    driver.findElement(By.xpath("//option[@value=\'6\']")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = '0-100']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(2)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = '100-200']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(3)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = '200-300']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(4)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = '300-400']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(5)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = '400-500']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(6)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = '500-600']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(7)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = '600-700']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(8)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = '700-800']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(9)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = '800+']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(10)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(4) > .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(4) .ng-dirty > option:nth-child(1)")).click();
		    {
		      WebElement dropdown = driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]/select"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.xpath("(//option[@value=\'\'])[6]")).click();
		    driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[2]/td[6]/select")).click();
		    driver.findElement(By.xpath("//option[@value=\'1\']")).click();
		    driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[2]/td[6]/select")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '$100k - $250k']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched > option:nth-child(3)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '$250k - $500k']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched > option:nth-child(4)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '$500k - $1M']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched > option:nth-child(5)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '$1M+']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched > option:nth-child(6)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched > option:nth-child(1)")).click();
		    driver.findElement(By.cssSelector("td:nth-child(7) > .ng-touched")).click();
		    driver.findElement(By.xpath("//option[@value=\'0.0\']")).click();
		    driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[3]/td[6]/select")).click();
		    {
		      WebElement dropdown = driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[3]/td[6]/select"));
		      dropdown.findElement(By.xpath("//option[. = '0.0']")).click();
		    }
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '0.5']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched > option:nth-child(3)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '1.0']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched > option:nth-child(4)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '1.5']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched > option:nth-child(5)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '2.0']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched > option:nth-child(6)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '2.5']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched > option:nth-child(7)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '3.0']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched > option:nth-child(8)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '3.5']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched > option:nth-child(9)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '4.0']")).click();
		    }
		    driver.findElement(By.cssSelector("td:nth-child(6) > .ng-touched > option:nth-child(10)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '4.5']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(11)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '5.0']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(12)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '5.5']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(13)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '6.0']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(14)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '6.5']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(15)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '7.0']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(16)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '7.5']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(17)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '8.0']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(18)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '8.5']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(19)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '9.0']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(20)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(6) > .ng-touched"));
		      dropdown.findElement(By.xpath("//option[. = '9.5']")).click();
		    }
		    driver.findElement(By.cssSelector(".ng-touched > option:nth-child(21)")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector("tr:nth-child(3) .ng-dirty")).click();
		    {
		      WebElement dropdown = driver.findElement(By.cssSelector("tr:nth-child(3) .ng-dirty"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.cssSelector("tr:nth-child(3) .ng-dirty")).click();
		    driver.findElement(By.id("idsearchbutton")).click();
		    driver.findElement(By.cssSelector(".filteroptions:nth-child(1) tr:nth-child(3) .input-small:nth-child(2)")).click();
		    {
		      WebElement dropdown = driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[4]/td[6]/select"));
		      dropdown.findElement(By.xpath("//option[. = 'Manual']")).click();
		    }
		    driver.findElement(By.id("idsearchbutton")).click();
		    {
		      WebElement dropdown = driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[4]/td[6]/select"));
		      dropdown.findElement(By.xpath("//option[. = 'Corporate']")).click();
		    }
		    driver.findElement(By.id("idsearchbutton")).click();
		    {
		      WebElement dropdown = driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[4]/td[6]/select"));
		      dropdown.findElement(By.xpath("//option[. = 'File Upload']")).click();
		    }
		    driver.findElement(By.id("idsearchbutton")).click();
		    {
		      WebElement dropdown = driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[4]/td[6]/select"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    {
		      WebElement dropdown = driver.findElement(By.xpath("//div[@id=\'mycontroller\']/table/tbody/tr/td[2]/table/tbody/tr[4]/td[6]/select"));
		      dropdown.findElement(By.xpath("//option[. = 'All']")).click();
		    }
		    driver.findElement(By.id("idsearchbutton")).click();
		    
		    driver.close();
		    
			driver = new ChromeDriver(options);
			
			driver.get("https://vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
	        //driver.switchTo().defaultContent();
		    
		    //click-on Login-button
			System.out.println("click Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //click-on Customers TAB
			System.out.println("click Customers TAB");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //click recent user
		    System.out.println("click Recent User");
		    driver.findElement(By.cssSelector("#customerLinks > a:nth-child(1)")).click();
		    
		    driver.switchTo().frame(0);
		    
	        //click on Add Calendar Entry
		    //verify customer can add calendar entry
	        System.out.println("click Add Calendar Entry");
	        
	        try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
	          
	        
		    driver.findElement(By.linkText("Select an action to perform")).click();
		    driver.findElement(By.linkText("Add Calendar Entry")).click();
		    
		    try {
			      Thread.sleep(1000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("txtCalEntrySubject")).click();
		    driver.findElement(By.id("txtCalEntrySubject")).sendKeys("testcalendar");
		    driver.findElement(By.id("txtCalEntryDate")).click();
		    driver.findElement(By.id("selCalEntryStartTime")).click();
		    driver.findElement(By.cssSelector("#selCalEntryStartTime > option:nth-child(25)")).click();
		    driver.findElement(By.id("selCalEntryEndTime")).click();
		    driver.findElement(By.cssSelector("#selCalEntryEndTime > option:nth-child(27)")).click();
		    driver.findElement(By.id("txtCalEntryWhere")).click();
		    driver.findElement(By.id("txtCalEntryWhere")).sendKeys("here");
		    driver.findElement(By.id("txtCalEntryDscr")).click();
		    driver.findElement(By.id("txtCalEntryDscr")).sendKeys("qatest");
		    driver.findElement(By.id("selCalEntryStatus")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("selCalEntryStatus"));
		      dropdown.findElement(By.xpath("//option[. = 'In Process']")).click();
		    }
		    driver.findElement(By.cssSelector("#selCalEntryStatus > option:nth-child(2)")).click();
		    //driver.findElement(By.id("chkCalEntryOutlook")).click();
		    driver.findElement(By.cssSelector(".calendar-modal-footer-row .btn-success")).click();
		    
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    
		    driver.findElement(By.xpath("//button[@onclick=\'document.getElementById(\"masterIframe\").src=\"Dashboardv2.asp\"\']")).click();
		    driver.switchTo().frame(0);
		    
		    //delete customer calendar entry
		    System.out.println("click calendar entry in dashboard");
		   
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector("#divDashboardBody > div.dashboard-row-3 > div:nth-child(3) > div > div.dashboard-row-3-cell-header")).click();
		    
		    driver.findElement(By.xpath("//*[@id=\"rowToday\"]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]/div")).click();
 

		    driver.switchTo().defaultContent();
		    System.out.println("click delete calendar window");
		   
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //bug with Delete calendar
		    driver.findElement(By.id("btnCalendarEntryDelete")).click();
		    driver.findElement(By.xpath("//*[@id=\"btnCalendarEntryDelete\"]")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    /*
		   
            driver.close();
		    
			driver = new ChromeDriver(options);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
	        //driver.switchTo().defaultContent();
		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    */
		  //click-on Customers TAB
			
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //click recent user
		    System.out.println("click Valid User");
		    driver.findElement(By.cssSelector("#customerLinks > a:nth-child(1)")).click();
		    
		    
		    //Add image to customer profile
		    //click on Add New Image
		    System.out.println("click Add New Image");
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    
		    //assertThat(driver.findElement(By.cssSelector("#GroupType")).getText(), is("Customers"));
		    
		   
		    driver.findElement(By.linkText("Select an action to perform")).click();
		    driver.findElement(By.linkText("Add New Image")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("fileTypeId")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("fileTypeId"));
		      dropdown.findElement(By.xpath("//option[. = 'Photo']")).click();
		    }
		    driver.findElement(By.cssSelector("#fileTypeId > option:nth-child(3)")).click();
		    //driver.findElement(By.id("fileTypeId")).click();
		    //{
		    //  WebElement dropdown = driver.findElement(By.id("fileTypeId"));
		    //  dropdown.findElement(By.xpath("//option[. = 'Photo']")).click();
		    //}
		    
		    driver.findElement(By.id("txtFileDescription")).click();
		    driver.findElement(By.id("txtFileDescription")).sendKeys("testphoto");
		    //driver.findElement(By.linkText("Select an action to perform")).click();
		    //driver.findElement(By.id("uploadFileBtn")).click();
		    driver.findElement(By.id("imageUploadClose")).click();
		    
		    driver.switchTo().defaultContent();
		    
		    
		    //click-on Customers TAB
			//System.out.println("click-on Customers TAB");
		    //driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    //Create Order
		    System.out.println("click Create Order");
		  
		    //click recent user
		    //System.out.println("click-on Recent User");
		    //driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[3]/div/div/a[1]")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    

		    driver.findElement(By.linkText("Select an action to perform")).click();
		    driver.findElement(By.linkText("Create Order")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //choose Offering from list - Email types are best
		    driver.findElement(By.id("idGiftPrgm")).click();
		    {
		        WebElement dropdown = driver.findElement(By.id("idGiftPrgm"));
		        dropdown.findElement(By.xpath("//option[. = 'Anniversary Email']")).click();
		    }
		    try {
		        Thread.sleep(5000);
		        } catch (InterruptedException e) {
		        e.printStackTrace();
		        }
		    
		    System.out.println("click Save As Draft");
		    driver.findElement(By.id("savebuttonaprovelater")).click();
		    
		    //click-on Logout TAB
		    if (myLog.exists()) {
		    	System.out.println("Log Exists, so opening log for appending.");
		    	//logWriter.close();
		    	FileWriter logWriter = new FileWriter(myLog, true);
		    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
		    logWriter.write(logWithNewLine2);
		    logWriter.close();
		    }
		    driver.switchTo().defaultContent();
		    System.out.println("click Logout button - Customer Test PASS");
		    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/a")).click();
		  
		    driver.close();
		    driver.quit();
	}

		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			// driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}
