package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Before;
	//import org.junit.Test;
	import org.junit.After;
	import static org.junit.Assert.*;
	import static org.hamcrest.CoreMatchers.is;
	import static org.hamcrest.core.IsNot.not;
	import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;
	import org.openqa.selenium.Alert;
	import org.openqa.selenium.Keys;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.openqa.selenium.JavascriptExecutor;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class ProdDashboard {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}


	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM Prod Dashboard";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			//WebDriver driver = new ChromeDriver();		
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			
			driver.get("https://vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> wh = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandle = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    vars.put(currentWindowHandle, waitForWindow(2000));
		 
		    //click-on Dashboard Tiles
			
		    System.out.println("verify Dashboard Tile Order needed for test");
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.id("le-tilecarousel-refreshdb")).click();
		    driver.findElement(By.id("le-tilecarousel-settingsdb")).click();
		    
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Tile Settings"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("Tile Preview"));
		    assertThat(driver.findElement(By.cssSelector(".tile-selection-box:nth-child(2) > .tile-selection-box-title")).getText(), is("Available Tiles"));
		    assertThat(driver.findElement(By.cssSelector(".tile-selection-box:nth-child(3) > .tile-selection-box-title")).getText(), is("Display Order"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector("#availableTiles > .tile-selection-tile:nth-child(1)")).click();
		    assertThat(driver.findElement(By.xpath("//div[@id=\'availableTiles\']/div")).getText(), is("Admin Leads Summary"));
		    driver.findElement(By.cssSelector("#availableTiles > .tile-selection-tile:nth-child(2)")).click();
		    assertThat(driver.findElement(By.cssSelector(".tile-selection-tile-selected")).getText(), is("Admin Weekly Email Tracking"));
		    driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(1)")).click();
		    assertThat(driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(1)")).getText(), is("Alert/Notification Summary"));
		    driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(2)")).click();
		    assertThat(driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(2)")).getText(), is("Admin Email Volume"));
		    driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(3)")).click();
		    assertThat(driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(3)")).getText(), is("Program Order Summary"));
		    driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(4)")).click();
		    assertThat(driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(4)")).getText(), is("Leads Summary"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("Leads Summary"));
		    driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(5)")).click();
		    assertThat(driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(5)")).getText(), is("My Links"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("My Links"));
		    driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(6)")).click();
		    assertThat(driver.findElement(By.cssSelector("#selectedTiles > .tile-selection-tile:nth-child(6)")).getText(), is("Group Subscriptions Recipients"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("Group Subscriptions Recipients"));
		    driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(7)")).click();
		    assertThat(driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(7)")).getText(), is("Weekly Email Tracking"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("Weekly Email Tracking"));
		    driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(8)")).click();
		    assertThat(driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(8)")).getText(), is("Today"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("  Today"));
		    driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(9)")).click();
		    assertThat(driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(9)")).getText(), is("Quick Action"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("Quick Action"));
		    driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(10)")).click();
		    assertThat(driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(10)")).getText(), is("Marketing Summary"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("Marketing Summary"));
		    driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(11)")).click();
		    assertThat(driver.findElement(By.cssSelector(".tile-selection-tile:nth-child(11)")).getText(), is("Order Summary"));
		    assertThat(driver.findElement(By.cssSelector(".tile-header")).getText(), is("Order Summary"));
		    //driver.switchTo().defaultContent();
		    
		    driver.close();
		    driver.quit();
		    
			driver = new ChromeDriver(options);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    Set<String> whb = driver.getWindowHandles();
			System.out.println("this is value of wh: " + whb);
			String currentWindowHandleb = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandleb);
		    vars.put(currentWindowHandleb, waitForWindow(2000));
		    
            //Dashboard Admin Email Volume
		    System.out.println("click-on Dashboard Admin Email Volume");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/button")).click();
		    
		    driver.switchTo().frame(0);
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.xpath("/html/body/div/div[1]/div/div[1]/div[3]/div[2]/div[1]")).click();
		    driver.findElement(By.xpath("//a[contains(@href, \'#tileTab1_12\')]")).click();
		    driver.findElement(By.xpath("//a[contains(@href, \'#tileTab2_12\')]")).click();
		    driver.findElement(By.xpath("//a[contains(@href, \'#tileTab0_12\')]")).click();
		    driver.findElement(By.cssSelector("#tileTab0_12 > .tile-3tab-row:nth-child(1) > .tile-3tab-row-txt")).click();
		    driver.findElement(By.id("elemOfferingCategoryId")).click();
		    assertThat(driver.findElement(By.id("pgHeader")).getText(), is("Marketing Activity"));
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.linkText("Home")).click();
		    driver.switchTo().defaultContent();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //Dashboard Alert Summary
		    System.out.println("click-on Dashboard Alert Summary");
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.xpath("/html/body/div/div[1]/div/div[1]/div[3]/div[2]/div[1]")).click();
		    assertThat(driver.findElement(By.cssSelector("#le-tilecarousel-tile0db > div:nth-child(1)")).getText(), is("Alert/Notification Summary"));
		    driver.switchTo().defaultContent();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		  //Dashboard Calendar
		    System.out.println("click-on Dashboard Calendar");
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector(".dashboard-row-3-cell:nth-child(3) .dashboard-row-3-cell-header")).click();
		    driver.findElement(By.id("p-calendartab2")).click();
		    driver.findElement(By.id("p-calendartab1")).click();
		    assertThat(driver.findElement(By.id("p-calendartab1")).getText(), is("Events"));
		    driver.findElement(By.cssSelector(".addCalendarEntry")).click();
		    driver.switchTo().defaultContent();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("txtCalEntrySubject")).click();
		    driver.findElement(By.id("txtCalEntrySubject")).sendKeys("testcalendardashboard");
		    driver.findElement(By.id("txtCalEntryDate")).click();
		    driver.findElement(By.id("txtCalEntrySubject")).click();
		    driver.findElement(By.id("selCalEntryStartTime")).click();
		    driver.findElement(By.cssSelector("#selCalEntryStartTime > option:nth-child(29)")).click();
		    driver.findElement(By.id("selCalEntryEndTime")).click();
		    driver.findElement(By.cssSelector("#selCalEntryEndTime > option:nth-child(31)")).click();
		    driver.findElement(By.id("txtCalEntryWhere")).click();
		    driver.findElement(By.id("txtCalEntryWhere")).sendKeys("here");
		    driver.findElement(By.id("txtCalEntryDscr")).click();
		    driver.findElement(By.id("txtCalEntryDscr")).sendKeys("Automated dashboard calendar");
		    driver.findElement(By.id("selCalEntryStatus")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("selCalEntryStatus"));
		      dropdown.findElement(By.xpath("//option[. = 'In Process']")).click();
		    }
		    driver.findElement(By.cssSelector("#selCalEntryStatus > option:nth-child(2)")).click();
		    driver.findElement(By.cssSelector(".calendar-modal-footer-row .btn-success")).click();
		    
		    //driver.switchTo().defaultContent();
		    
		    //click Group Subscriptions Recipients Tile
		    System.out.println("click-on Dashboard Group Subscriptions Recipients Tile");
		    
		    driver.switchTo().frame(0);
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    driver.findElement(By.cssSelector(".fa-arrow-circle-right")).click();
		    driver.findElement(By.cssSelector(".fa-arrow-circle-right")).click();
		    driver.findElement(By.cssSelector(".fa-arrow-circle-right")).click();
		    
		    try {
		        Thread.sleep(5000);
		      } catch (InterruptedException e) {
		        e.printStackTrace();
		      }
		    assertThat(driver.findElement(By.cssSelector("#le-tilecarousel-tile2db > .tile-header")).getText(), is("Group Subscriptions Recipients"));
		    driver.findElement(By.cssSelector("#tile-body2db > .tile-list-row:nth-child(1) > .tile-list-row-label")).click();
		    
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    
		    assertThat(driver.findElement(By.id("GroupType")).getText(), is("Edit Group"));
		    driver.findElement(By.linkText("Home")).click();
		    
		    try {
		        Thread.sleep(5000);
		      } catch (InterruptedException e) {
		        e.printStackTrace();
		      }
		    
		    driver.findElement(By.cssSelector(".le-tilecarousel")).click();
		    driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
		    driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
		    driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
		    driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
		    driver.findElement(By.cssSelector(".fa-arrow-circle-left")).click();
		    
		    
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //Dashboard Latest News
		    System.out.println("Verify Dashboard Latest News");
		    

		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.cssSelector("div.dashboard-row-3-cell:nth-child(1) > div:nth-child(1) > div:nth-child(1)")).getText(), is("Latest News"));
		    driver.switchTo().defaultContent();
		    
		    /* driver.close();
		    
			driver = new ChromeDriver(options);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://uat.vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck-CM");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");
		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    Set<String> whb = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandleb = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandleb);
		    vars.put(currentWindowHandleb, waitForWindow(2000));
		    */
		    
		    //Dashboard Marketing Summary
            System.out.println("Verify Dashboard Marketing Summary");
            
           
    	    
            driver.switchTo().frame(0);
		    
            try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		   
		    
		    assertThat(driver.findElement(By.cssSelector("#le-tilecarousel-tile0db > div:nth-child(1)")).getText(), is("Marketing Summary"));
		    driver.findElement(By.linkText("This Week")).click();
		    driver.findElement(By.linkText("This Month")).click();
		    driver.findElement(By.linkText("Today")).click();
		    driver.findElement(By.cssSelector("#tileTab0_4 .tile-3tab-row-txt")).click();
		    assertThat(driver.findElement(By.id("pgHeader")).getText(), is("Marketing Activity"));
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.linkText("Home")).click();
		    
		    //driver.switchTo().defaultContent();
		    
		    
		    //click-on Logout TAB
			    if (myLog.exists()) {
			    	System.out.println("Log Exists, so opening log for appending.");
			    	//logWriter.close();
			    	FileWriter logWriter = new FileWriter(myLog, true);
			    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
			    logWriter.write(logWithNewLine2);
			    logWriter.close();
			    }
			    driver.switchTo().defaultContent();
			System.out.println("click-on Logout button - Dashboard Test PASS");
		    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/a")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.close();
			driver.quit();
	}
		   
		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			// driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}

