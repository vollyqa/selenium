package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Test;
	//import org.junit.After;
	import java.time.Duration;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class ProdReports {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "Prod Reports";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			//WebDriver driver = new ChromeDriver();		
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			
			driver.get("https://vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> wh = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandle = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    vars.put(currentWindowHandle, waitForWindow(2000));
		 
		    //click-on Reports TAB
			System.out.println("click-on Reports TAB");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[7]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.xpath("//*[@id=\"section0\"]")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //click ten reports as specified by Product Management
		    //Custom Closed Loan Report, ARM Expiring, All Contacts Report, Comprehensive Mailing List, Contact Birthday Report, Contacts in a Group, Program Order Summary
		    //Reassignment by Corp Account, Comprehensive Mailing List, Opt-out contacts
		    
		    System.out.println("click-on Custom Closed Loan Report");
		    driver.findElement(By.xpath("/html/body/div/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[2]/div[2]/ul/li[7]")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("submitBtn")).click();
		    driver.findElement(By.linkText("Reports")).click();
		    
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    System.out.println("click-on ARM Expiring");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[2]/div[2]/ul/li[2]")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("submitBtn")).click();
		    driver.findElement(By.linkText("Reports")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    System.out.println("click-on All Contacts Report");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[2]/div[2]/ul/li[2]")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("submitBtn")).click();
		    driver.findElement(By.linkText("Reports")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    System.out.println("click-on Comprehensive Mailing List");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[2]/div[2]/ul/li[4]")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("submitBtn")).click();
		    driver.findElement(By.linkText("Reports")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    System.out.println("click-on Contact Birthday Report");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[2]/div[2]/ul/li[5]")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("submitBtn")).click();
		    driver.findElement(By.linkText("Reports")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    System.out.println("click-on Program Order Summary");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[2]/div[2]/ul/li[21]")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("submitBtn")).click();
		    driver.findElement(By.linkText("Reports")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    System.out.println("click-on Reassignment by Corp Account");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[3]/div[2]/ul/li[28]")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("submitBtn")).click();
		    driver.findElement(By.linkText("Reports")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //System.out.println("click-on Comprehensive Mailing List");
		    //driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[2]/div[2]/ul/li[4]")).click();
		    //driver.findElement(By.id("submitBtn")).click();
		    //driver.findElement(By.linkText("Reports")).click();
		    
		    System.out.println("click-on Opt Out-Contacts");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[2]/div[2]/ul/li[19]")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("submitBtn")).click();
		    driver.findElement(By.linkText("Reports")).click();
		  
		    driver.switchTo().defaultContent();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //click-on Reports TAB
			System.out.println("click-on Reports TAB");
			driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[7]/button")).click();
			
		    //Click on Contact Retention Prgs
		    System.out.println("click-on Contact Retention Prgs");
		    driver.findElement(By.linkText("Contact Retention Prgs")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
	        //imported from Selenium IDE	    
		    driver.switchTo().frame(0);
		    //driver.switchTo().defaultContent();
		    driver.findElement(By.id("dateFrom")).click();
		    driver.findElement(By.linkText("1")).click();
		    driver.findElement(By.id("dateTo")).click();
		    driver.findElement(By.linkText("1")).click();
		    driver.findElement(By.id("selOffering")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("selOffering"));
		      dropdown.findElement(By.xpath("//option[. = 'Any Retention Program Available']")).click();
		    }
		    driver.findElement(By.id("selOffering")).click();
		    driver.findElement(By.id("contactStatus")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("contactStatus"));
		      dropdown.findElement(By.xpath("//option[. = 'Any Status']")).click();
		    }
		    driver.findElement(By.id("contactStatus")).click();
		    driver.findElement(By.id("contactType")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("contactType"));
		      dropdown.findElement(By.xpath("//option[. = 'Any Type']")).click();
		    }
		    driver.findElement(By.id("contactType")).click();
		    driver.findElement(By.id("categoryType")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("categoryType"));
		      dropdown.findElement(By.xpath("//option[. = 'Any Category']")).click();
		    }
		    driver.findElement(By.id("categoryType")).click();
		    driver.findElement(By.id("contacts-retention-search-btn")).click();
		    driver.findElement(By.id("categoryType")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("categoryType"));
		      dropdown.findElement(By.xpath("//option[. = 'Any Category']")).click();
		    }
		    driver.findElement(By.id("categoryType")).click();
		    driver.findElement(By.id("contactType")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("contactType"));
		      dropdown.findElement(By.xpath("//option[. = 'Any Type']")).click();
		    }
		    driver.findElement(By.id("contactType")).click();
		    driver.findElement(By.id("contactStatus")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("contactStatus"));
		      dropdown.findElement(By.xpath("//option[. = 'Any Status']")).click();
		    }
		    driver.findElement(By.id("contactStatus")).click();
		    driver.findElement(By.id("selOffering")).click();
		    {
		      WebElement dropdown = driver.findElement(By.id("selOffering"));
		      dropdown.findElement(By.xpath("//option[. = 'Any Retention Program Available']")).click();
		    }
		    driver.findElement(By.id("selOffering")).click();
		    driver.findElement(By.id("contacts-retention-search-btn")).click();
		    driver.findElement(By.id("placeOrderBtn")).click();
		    //driver.close();

		    
		    driver.switchTo().defaultContent();
		    
		    //click-on Logout TAB
		    if (myLog.exists()) {
		    	System.out.println("Log Exists, so opening log for appending.");
		    	//logWriter.close();
		    	FileWriter logWriter = new FileWriter(myLog, true);
		    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
		    logWriter.write(logWithNewLine2);
		    logWriter.close();
		    }
			System.out.println("click-on Logout button - Reports Test PASS");
		    
			driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/a")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.close();
		    driver.quit();
		    
	}
		    
		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			// driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}

