package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Before;
	//import org.junit.Test;
	import org.junit.After;
	import static org.junit.Assert.*;
	import static org.hamcrest.CoreMatchers.is;
	import static org.hamcrest.core.IsNot.not;
	import java.time.Duration;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.HashMap;
	import java.util.Iterator;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.Iterator;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;
	import org.openqa.selenium.Alert;
	import org.openqa.selenium.Keys;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.openqa.selenium.JavascriptExecutor;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class ProdMarketing {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		
	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM Prod Marketing";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			//WebDriver driver = new ChromeDriver();		
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> wh = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandle = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    vars.put(currentWindowHandle, waitForWindow(2000));
		 
		    //click-on Marketing TAB
			System.out.println("click-on Marketing TAB");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[8]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    System.out.println("Verify Marketing Store Content");
		    
		    //verify Birthday
		    System.out.println("Verify Marketing Store Birthday");
		    driver.findElement(By.cssSelector("#section6 > div")).click();
		    driver.findElement(By.cssSelector("#sectionBody6 .item-link:nth-child(1)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.xpath("//div[@id=\'product\']/div[2]/div[2]/div")).getText(), is("Birthday 1 Postcard"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[4]")).getText(), is("Birthday 2 E-mail"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[7]")).getText(), is("Birthday 2 Postcard"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[10]")).getText(), is("Birthday 3 Postcard"));
		 
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //verify Bulk Selections
		    System.out.println("Verify Marketing Store Bulk Selection");
		    driver.findElement(By.cssSelector("#section8 > div")).click();
		    driver.findElement(By.cssSelector("#sectionBody8 .item-link:nth-child(1)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.xpath("//div[@id=\'product\']/div[2]/div[2]/div")).getText(), is("Co-Marketing Flyer"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //verify Gifts
		    System.out.println("Verify Marketing Store Gifts");
		    driver.findElement(By.cssSelector("#section7 > div")).click();
		    driver.findElement(By.cssSelector("#sectionBody7 .item-link:nth-child(1)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]")).getText(), is("8-piece Sampler"));
		    assertThat(driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/div[1]/div[2]/div[3]/div[2]/div[2]/div[1]")).getText(), is("Holiday Assortment"));
		    assertThat(driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/div[1]/div[2]/div[4]/div[2]/div[2]/div[1]")).getText(), is("Sweet Sloops"));
		    assertThat(driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/div[1]/div[2]/div[5]/div[2]/div[2]/div[1]")).getText(), is("Tower Assortment"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //verify Holiday
		    System.out.println("Verify Marketing Store Holiday");
		    driver.findElement(By.cssSelector("#section5 > div")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.cssSelector("#sectionBody5 .item-link:nth-child(1)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.cssSelector(".offering-row-data-name")).getText(), is("Holiday Email Program"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //verify On Demand Announcements
		    System.out.println("Verify Marketing Store On Demand Announcements");
		    driver.findElement(By.cssSelector("#section3 > div")).click();
		    driver.findElement(By.cssSelector("#sectionBody3 .item-link:nth-child(4)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.id("offeringCategoryName")).getText(), is("Announcements"));
		    assertThat(driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]")).getText(), is("Change of Address Postcard"));
		    assertThat(driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]")).getText(), is("LO Announcement Card"));
		    assertThat(driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/div[1]/div[2]/div[5]/div[2]/div[2]/div[1]")).getText(), is("LO Announcement Postcard"));
		    driver.findElement(By.cssSelector("#section3 > div")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //verify On Demand Partner
		    System.out.println("Verify Marketing Store On Demand Partner");
		    driver.findElement(By.cssSelector("#section3 > div")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.cssSelector("#sectionBody3 > ul:nth-child(1) > li:nth-child(3)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.id("offeringCategoryName")).getText(), is("Partner"));
		    assertThat(driver.findElement(By.xpath("//div[@id=\'product\']/div[2]/div[2]/div")).getText(), is("Friend or Family Thank You Email"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[7]")).getText(), is("Partner Prospecting Email"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[10]")).getText(), is("Partner Prospecting Realtor Email"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[13]")).getText(), is("Partner Thank You Email"));
		    driver.findElement(By.cssSelector("#section3 > div")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //verify On Demand Loan Related
		    System.out.println("Verify Marketing Store On Demand Loan Related");
		    driver.findElement(By.cssSelector("#section3 > div")).click();
		    driver.findElement(By.cssSelector("#sectionBody3 .item-link:nth-child(1)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.id("offeringCategoryName")).getText(), is("Loan-related"));
		    assertThat(driver.findElement(By.xpath("//div[@id=\'product\']/div[2]/div[2]/div")).getText(), is("Anniversary Email"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[4]")).getText(), is("Anniversary Postcard"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[7]")).getText(), is("ARM Reset Email"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[16]")).getText(), is("Mortgage Check-up Email"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[40]")).getText(), is("Refinance Email"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[43]")).getText(), is("Refinance Postcard"));
		    driver.findElement(By.cssSelector("#section3 > div")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //verify On Demand Newsletter
		    System.out.println("Verify Marketing Store On Demand Newsletter");
		    driver.findElement(By.cssSelector("#section3 > div")).click();
		    driver.findElement(By.cssSelector("li.item-link:nth-child(5)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.id("offeringCategoryName")).getText(), is("Newsletters"));
		    assertThat(driver.findElement(By.xpath("//div[@id=\'product\']/div[2]/div[2]/div")).getText(), is("HomeFront eNewsletter"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[4]")).getText(), is("inHome eNewsletter (Consumer)"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[7]")).getText(), is("inMarket eNewsletter (Partner)"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[10]")).getText(), is("inRealty eNewsletter"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[13]")).getText(), is("Weekly Market Update Email"));
		    driver.findElement(By.cssSelector("#section3 > div")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //verify On Demand Prospecting
		    System.out.println("Verify Marketing Store On Demand Prospecting");
		    driver.findElement(By.cssSelector("#section3 > div")).click();
		    driver.findElement(By.cssSelector("#sectionBody3 .item-link:nth-child(2)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.id("offeringCategoryName")).getText(), is("Prospecting"));
		    assertThat(driver.findElement(By.xpath("//div[@id=\'product\']/div[2]/div[2]/div")).getText(), is("Credit Program"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[4]")).getText(), is("First-time Buyer Email"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[7]")).getText(), is("Milestone Program"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[10]")).getText(), is("Prequal Prospect Email"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[19]")).getText(), is("Prospecting Program"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[22]")).getText(), is("Recruiter Postcard"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //verify Retention Programs
		    System.out.println("Verify Marketing Store Retention Programs");
		    driver.findElement(By.cssSelector("#sectionBody2 .item-link:nth-child(3)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.id("offeringCategoryName")).getText(), is("Newsletter Subscriptions"));
		    assertThat(driver.findElement(By.xpath("//div[@id=\'product\']/div[2]/div[2]/div")).getText(), is("HomeFront eNewsletter"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[4]")).getText(), is("inHome eNewsletter (Consumer)"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[7]")).getText(), is("inMarket eNewsletter (Partner)"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[10]")).getText(), is("inRealty eNewsletter"));
		    assertThat(driver.findElement(By.xpath("(//div[@id=\'product\']/div[2]/div[2]/div)[13]")).getText(), is("Weekly Market Update Email"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    /*
		    //complete Test order
			System.out.println("Complete test order");
			driver.findElement(By.cssSelector("div.offering-row:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > a:nth-child(2)")).click();
			
			//Create customer recipient list
			driver.findElement(By.id("idShowRecipientList")).click();
			driver.findElement(By.cssSelector("#idContactType > option:nth-child(2)")).click();
			driver.findElement(By.cssSelector(".formTable > tbody:nth-child(1) > tr:nth-child(21) > td:nth-child(1) > input:nth-child(1)")).click();
			
			//select customer
			driver.findElement(By.id("btnRed1")).click();
			
			//click save
			driver.findElement(By.id("savebuttonaprovelater")).click();
			
			
			//Add Contacts
			driver.findElement(By.id("idContactType")).click();
		   
		    
		   //click-on Marketing TAB
			System.out.println("click-on Marketing TAB");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[8]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }

		  */
		    
		    driver.switchTo().defaultContent();
		    
		    String parentHandle = driver.getWindowHandle();
		    
		    //click-on  Marketing Portal
			System.out.println("click-on Marketing Portal - About Us Flyer");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[8]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[8]/div/a[2]")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		  
		    System.out.println("Select About Us Flyer");
		    	
		    for (String winHandle : driver.getWindowHandles()) {
		    	driver.switchTo().window(winHandle);
		    }
		    System.out.println("Page title : "+ driver.getTitle());
		    
		    driver.switchTo().defaultContent();
		  //click all categories
		    driver.findElement(By.id("id-0")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector(".button")).click();
		    
		     
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //select About Us Flyer
		    driver.findElement(By.cssSelector("div.product-item:nth-child(7) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1)")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    assertThat(driver.findElement(By.id("ctl00_containerPageProductName")).getText(), is("About Us Flyer"));
		    //driver.findElement(By.id("ctl00_cphMainContent_ucDialCustomization_Duc157831_StringTextBox")).click();
		    driver.findElement(By.id("ctl00_cphMainContentFooter_WizardStepsNextPrev1_ButtonNext")).click();
		    
		    System.out.println("click Marketing Portal Proof");
		    //driver.findElement(By.id("ctl00_cphMainContent_ctl09_chkProofApproval")).click();
		    String ParentHandle = driver.getWindowHandle();
		    System.out.println("Parent Window :" + ParentHandle);
		    
		    driver.findElement(By.cssSelector(".proofLink")).click();
		    try {
		      Thread.sleep(10000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    
		    Set AllHandles = driver.getWindowHandles();
		    System.out.println("Windows handle :" + AllHandles);
		    
		    //driver.close();
		    
		    System.out.println("Switching to parent window :" + ParentHandle);
		    driver.switchTo().window(ParentHandle);
		    
		    //driver.switchTo().window(vars.get("root").toString());
		    //driver.findElement(By.id("ctl00_AISDnldCtl1_AISDnldBtn1")).click();
		    //assertThat(driver.findElement(By.id("ct100")).getText(), is("Please approve the proof before downloading."));
		    //driver.findElement(By.cssSelector(".finish")).click();
		    //driver.findElement(By.cssSelector(".icon > .cart-icon")).click();
		    //assertThat(driver.findElement(By.id("ctl00_containerPageSubTitle")).getText(), is("Shopping Cart"));
		    
		    driver.switchTo().frame(0);
		    //driver.switchTo().frame(0);
		    
		    driver.findElement(By.id("ctl00_cphMainContent_ctl09_tdProofApproval")).click();
		    driver.findElement(By.id("ctl00_cphMainContentFooter_WizardStepsNextPrev1_ButtonFinish")).click();
		    
		    //add to MP Order
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //verify shopping cart
		    System.out.println("Verify Marketing Portal - Shopping Cart");
		    
		    //driver.switchTo().frame(0);
		    //assertThat(driver.findElement(By.id("ct100_containerPageSubtitle")).getText(), is("Shopping Cart"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("ctl00_cphMainContent_BtnSubmit")).click();
		    
		    //verify checkout
		    System.out.println("verify Marketing Portal - Checkout - Order Summary");
		    
		    //driver.switchTo().frame(0);
		    //assertThat(driver.findElement(By.id("ct100_containerPageSubtitle")).getText(), is("Checkout - Order Summary"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("ctl00_cphMainContentFooter_btnNext")).click();
		    
		  //verify checkout
		    System.out.println("verify Marketing Portal - Checkout - Order Summary, Address Details");
		    
		    
		    //driver.switchTo().frame(0);
		    //assertThat(driver.findElement(By.id("ct100_containerPageSubtitle")).getText(), is("Checkout - Order Summary"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.cssSelector("#radAsBilling")).click();
		   
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.id("ctl00_cphMainContentFooter_btnNext")).click();
		 
		    
		  //verify order summary
		    System.out.println("verify Marketing Portal - Order Summary");
		    
		    //driver.switchTo().frame(0);
		    //assertThat(driver.findElement(By.id("ct100_containerPageSubtitle")).getText(), is("Order Summary"));
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //driver.switchTo().frame(0);
		    driver.findElement(By.id("ctl00_cphMainContentFooter_btnCheckout")).click();
	
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //driver.close();
		    //driver.navigate().back();
		   
		    String parent=driver.getWindowHandle();
		    
		    Set<String>s=driver.getWindowHandles();
		 // Now iterate using Iterator
			 Iterator<String> I1= s.iterator();

			 while(I1.hasNext())
			 {

			 String child_window=I1.next();


			 if(!parent.equals(child_window))
			 {
			 driver.switchTo().window(child_window);

			 System.out.println(driver.switchTo().window(child_window).getTitle());

			 driver.close();
			 }
			 }
			//switch to parent window
			 driver.switchTo().window(parent);
			 driver.quit();
		    
		  //Adding-in Headless Chrome Options
			//ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//driver.manage().window().maximize();
			driver.get("https://vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();	    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //click-on Marketing TAB
			System.out.println("click-on Marketing TAB");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[8]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    	    
		    //click-on Order Summary
			
		    System.out.println("click-on Order Summary");		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[8]/div/a[3]")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.switchTo().frame(0);
		    
		    
		    
		    driver.switchTo().defaultContent();
		    
		  //click-on Marketing Activity
			System.out.println("click-on Marketing Activity");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[8]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[8]/div/a[4]")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    //driver.switchTo().frame(0);
		    
		    //driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/table/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr[1]/td[5]/button")).click();
		    
		    //driver.switchTo().defaultContent();
		    
		  
		    
		   
		    //click-on Logout TAB
		    if (myLog.exists()) {
		    	System.out.println("Log Exists, so opening log for appending.");
		    	//logWriter.close();
		    	FileWriter logWriter = new FileWriter(myLog, true);
		    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
		    logWriter.write(logWithNewLine2);
		    logWriter.close();
		    }
			System.out.println("click-on Logout button - Marketing Test PASS");
		    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/a")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.close();
		    driver.quit();
		    
	}
		    

		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			// driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}
