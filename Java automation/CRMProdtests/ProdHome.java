package vollygroupid.Mavenjava;

	import org.testng.Assert;

	//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
	//import org.junit.Test;
	//import org.junit.After;
	import static org.junit.Assert.*;
	import static org.hamcrest.CoreMatchers.is;
	import static org.hamcrest.core.IsNot.not;
    import java.time.Duration;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;

	//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class ProdHome {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

	@Test
	//Set Property

	    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM Prod Home";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log doesnot Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
	      //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");
	      System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			//WebDriver driver = new ChromeDriver();		
			
			//Adding-in Headless Chrome Options
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			options.addArguments("window-size=1280,800");
			//WebDriver driver = new ChromeDriver(options);
			driver = new ChromeDriver(options);
			
			driver.get("https://vollycrm.com/site/login.asp");
		    //driver.manage().window().setSize(new Dimension(1936, 1056));
		    driver.findElement(By.name("UserLogin")).click();
		    driver.findElement(By.name("UserLogin")).sendKeys("ABeck");
		    driver.findElement(By.name("UserPassword")).click();
		    driver.findElement(By.name("UserPassword")).sendKeys("Testpw1234");
		    driver.findElement(By.name("UserCorp")).click();
		    driver.findElement(By.name("UserCorp")).sendKeys("ABC2");

		    
		    //click-on Login-button
			System.out.println("click-on Login button");
		    driver.findElement(By.xpath("/html/body/div/div/div/div[1]/form/div[2]/div[4]/div/input")).click();
		    //driver.findElement(By.cssSelector(".btn")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    Set<String> wh = driver.getWindowHandles();
			System.out.println("this is value of wh: " + wh);
			String currentWindowHandle = driver.getWindowHandle();
			System.out.println("this is value of currentWindowHandle: " + currentWindowHandle);
		    vars.put(currentWindowHandle, waitForWindow(2000));
		    
		    //click-on Home TAB
			System.out.println("click-on Home TAB");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/button")).click();
		    try {
			      Thread.sleep(8000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }

		    //click-on Home TAB
			System.out.println("click-on Home Header icons");
			System.out.println("click-on Lead Summary");
			
		    driver.findElement(By.id("spanBadgeLeadSummary")).click();
		    //driver.switchTo().frame(0);
		    //assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Lead Summary"));
		    //driver.switchTo().defaultContent();
		    driver.findElement(By.id("spanBadgeAlertsSummary")).click();
		    System.out.println("click-on Alert Summary");
		    driver.switchTo().frame(0);
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Alert Summary"));
		    driver.switchTo().defaultContent();
		    driver.findElement(By.id("spanBadgeOrderSummary")).click();
		    System.out.println("click-on Order Summary");
	
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    //assertThat(driver.findElement(By.cssSelector(".site-page-title-content")).getText(), is("Order Summary"));
		    driver.switchTo().defaultContent();
		    driver.findElement(By.cssSelector(".site-header-alert:nth-child(3)")).click();
		    driver.findElement(By.id("spanBadgeCalendar")).click();
		    driver.switchTo().frame(0);
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    assertThat(driver.findElement(By.cssSelector("#spanCalendarWeeklyToday .calendarTodayTomorrow")).getText(), is("Today"));
		    assertThat(driver.findElement(By.cssSelector("tr:nth-child(4) .calendarTodayTomorrow")).getText(), is("Tomorrow"));
		    driver.switchTo().defaultContent();
		    driver.findElement(By.id("imgHeaderProfile")).click();
		    driver.switchTo().frame(0);
		    assertThat(driver.findElement(By.cssSelector("table:nth-child(12) > tbody > tr > td:nth-child(1) > .formTable > caption")).getText(), is("Personalization Settings"));
		    driver.switchTo().defaultContent();
		    
		    //test Header search
			System.out.println("click-on Home Search functionality");
			
			//test search for ARM
			System.out.println("Search for ARM in search selector");
			driver.findElement(By.cssSelector(".site-header-logo img")).click();
		    driver.findElement(By.id("txtHeaderSearch")).sendKeys("ARM");
		    
		    try {
			      Thread.sleep(20000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }   
		    driver.findElement(By.cssSelector(".search-row:nth-child(1) .search-row-info")).click();
		    
		    driver.findElement(By.cssSelector(".fa-caret-down")).click();
		    driver.findElement(By.cssSelector(".search-dropdown-select-option:nth-child(2)")).click();
		    
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //search for test contact
		    System.out.println("Search for Test Search Selector as Contacts");
		    driver.findElement(By.cssSelector(".site-header-logo img")).click();
		    driver.findElement(By.id("txtHeaderSearch")).sendKeys("Test"); 
		  
		    try {
			      Thread.sleep(20000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector(".search-row:nth-child(1) .search-row-info")).click();
		    
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector(".fa-caret-down")).click();
            driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[6]/div/div[2]/div[3]")).click();
		    
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //search for test customer
		    
		    System.out.println("Search for Test Search Selector as Customers");
		    driver.findElement(By.cssSelector(".site-header-logo img")).click();
		    driver.findElement(By.id("txtHeaderSearch")).sendKeys("Customer");
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector(".search-row:nth-child(1) .search-row-info")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector(".fa-caret-down")).click();
		    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[6]/div/div[2]/div[3]")).click();
		    
		    
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    
		    //driver.findElement(By.cssSelector(".search-row:nth-child(1) .search-row-info-top")).click();
		    //driver.findElement(By.cssSelector(".fa-caret-down")).click();
		    //driver.findElement(By.cssSelector(".search-dropdown-select-option:nth-child(3)")).click();
		    
		    
		    //click on partner search
		    System.out.println("Search for Test Search Selector as Partner");
		    
		    driver.findElement(By.cssSelector(".fa-caret-down")).click();
		    driver.findElement(By.cssSelector(".search-dropdown-select-option:nth-child(4)")).click();
		    driver.findElement(By.id("txtHeaderSearch")).sendKeys("Partner");
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.cssSelector(".search-row:nth-child(1) .search-row-info")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.cssSelector(".fa-caret-down")).click();
		    driver.findElement(By.cssSelector(".search-dropdown-select-option:nth-child(6)")).click();
		    try {
		      Thread.sleep(10000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    
		    //click on loans search
		    System.out.println("Search for Test Search Selector as Loans");
		    driver.findElement(By.cssSelector(".site-header-logo img")).click();
		    driver.findElement(By.id("txtHeaderSearch")).sendKeys("2");
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector(".search-row:nth-child(1) .search-row-info")).click();
		    
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector(".fa-caret-down")).click();
		    //driver.findElement(By.cssSelector(".search-dropdown-select-option:nth-child(6)")).click();
		    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[6]/div/div[2]/div[7]")).click();
		    
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    
		  //click on reports search
		    System.out.println("Search for Test Search Selector as Reports");
		    
		    driver.findElement(By.cssSelector(".site-header-logo img")).click();
		    driver.findElement(By.id("txtHeaderSearch")).sendKeys("ARM");
		    
		    try {
			      Thread.sleep(20000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector(".search-row:nth-child(1) .search-row-info")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.findElement(By.cssSelector(".fa-caret-down")).click();
		    driver.findElement(By.cssSelector("div.search-dropdown-select-option:nth-child(8)")).click();
		    
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		  
		    //click on marketing search
		    System.out.println("Search for Test Search Selector as Marketing");
		    driver.findElement(By.id("txtHeaderSearch")).sendKeys("Birthday");
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.cssSelector(".search-row:nth-child(1) .search-row-info")).click();
		    
		    try {
		      Thread.sleep(5000);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    //Marketing search not loading
		    //driver.findElement(By.cssSelector(".search-row:nth-child(1) .search-row-info")).click();
		    
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    //check dashboard calendar
		    System.out.println("check Dashboard Calendar Tile");
		    
		    driver.findElement(By.xpath("//button[@onclick=\'document.getElementById(\"masterIframe\").src=\"Dashboardv2.asp\"\']")).click();
		    driver.switchTo().frame(0);
		    driver.switchTo().defaultContent();
		    
		    System.out.println("click-on Home TAB / My Calendar");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    } 
		    driver.findElement(By.linkText("My Calendar")).click();
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    driver.findElement(By.cssSelector("div:nth-child(1) > .btn")).click();
		    
		    //close add calendar
		    driver.switchTo().defaultContent();
		    System.out.println("click-on close add calendar");
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.findElement(By.id("txtCalEntrySubject")).click();
		    driver.findElement(By.cssSelector(".calendar-modal-footer-row > div:nth-child(1) > button:nth-child(2)")).click();
		    
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    
		    driver.findElement(By.cssSelector(".fa-angle-right")).click();
		    driver.findElement(By.cssSelector(".fa-angle-right")).click();
		    driver.findElement(By.cssSelector("div:nth-child(3) > .btn")).click();
		    
		    
		    //check todays events
		    System.out.println("click-on todays events");
		    //fails here
		    
		    //driver.switchTo().frame(0);
		    driver.switchTo().defaultContent();
		    //driver.findElement(By.xpath("//button[@onclick=\'document.getElementById(\"masterIframe\").src=\"Dashboardv2.asp\"\']")).click();
		    
		    
		    System.out.println("click-on Home TAB / Today");
		    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/button")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    } 
		    driver.findElement(By.linkText("Today")).click();
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    driver.switchTo().frame(0);
		    System.out.println("click-on Sections of Today tile");
		    driver.findElement(By.cssSelector(".le-accordion")).click();
		    
		    //driver.switchTo().defaultContent();
		    
		    
		    driver.findElement(By.xpath("//*[@id=\"section1\"]")).click();
		    driver.findElement(By.id("section2")).click();
		    driver.findElement(By.id("section3")).click();
		    driver.findElement(By.id("section4")).click();
		    driver.findElement(By.id("section5")).click();
		    
		    {
		      WebElement element = driver.findElement(By.cssSelector(".site-page-content"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).clickAndHold().perform();
		    }
		    {
		      WebElement element = driver.findElement(By.cssSelector(".site-page-content"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).perform();
		    }
		    {
		      WebElement element = driver.findElement(By.cssSelector(".site-page-content"));
		      Actions builder = new Actions(driver);
		      builder.moveToElement(element).release().perform();
		    }

		    driver.findElement(By.id("section5")).click();
		    driver.findElement(By.id("section4")).click();
		    driver.findElement(By.id("section3")).click();
		    driver.findElement(By.id("section2")).click();
		    driver.switchTo().defaultContent();
			
		    try {
			      Thread.sleep(10000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    
		    
		    //click-on Logout TAB
		    if (myLog.exists()) {
		    	System.out.println("Log Exists, so opening log for appending.");
		    	//logWriter.close();
		    	FileWriter logWriter = new FileWriter(myLog, true);
		    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
		    logWriter.write(logWithNewLine2);
		    logWriter.close();
		    }
			System.out.println("click-on Logout button - Home Test PASS");
		    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/a")).click();
		    try {
			      Thread.sleep(5000);
			    } catch (InterruptedException e) {
			      e.printStackTrace();
			    }
		    driver.close();
		    driver.quit();
		    
	}
		 

		@AfterTest
		public void tearDownTest() {
			// close browser
			//driver.close();
			// driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}
