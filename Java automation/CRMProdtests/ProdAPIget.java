package vollygroupid.Mavenjava;

	import org.testng.Assert;

import java.io.BufferedReader;

//import com.sun.org.apache.bcel.internal.generic.Select;
	//below libraries from selenium-IDE

	import java.io.File;
	import java.io.FileWriter;
	import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
//import org.junit.Test;
	//import org.junit.After;
	import java.time.Duration;
	import java.time.LocalDateTime;
	import java.util.ArrayList;
	import java.util.Calendar;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
    import java.util.Optional;
    import java.util.Set;
	import java.util.TimeZone;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Dimension;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.NoSuchElementException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebDriver.Window;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;
	//import org.openqa.selenium.devtools.DevTools;
    //import org.openqa.selenium.devtools.v96.network.*;	

//import org.openqa.selenium.WebElement;

	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.FluentWait;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import org.testng.annotations.AfterTest;

	//static WebDriver driver;

	public class ProdAPIget {

		WebDriver driver = null;

		private static final String t = null;
		// private WebDriver driver;
		private Map<String, Object> vars = new HashMap<String, Object>();

		public String waitForWindow(int timeout) {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}
	    private static final String USER_AGENT = "Mozilla/5.0";
	    
	  //tagging API tests - https://api.vollycrm.com/tagging/swagger/index.html
	    
	    private static final String GET_TAGURL = "https://api.vollycrm.com/tagging/tag";
	    
	    private static final String GET_TAGURLB = "https://api.vollycrm.com/tagging/group";
	    
	    private static final String GET_TAGURLC = "https://api.vollycrm.com/tagging/tagGroupAppKey";
	    
	    private static final String GET_TAGURLD = "https://api.vollycrm.com/tagging/map/CMJYB";

	    private static final String GET_TAGURLE = "https://api.vollycrm.com/tagging/map/CMJYB/Contact/10006395";
	    
	    private static final String GET_TAGURLF = "https://api.vollycrm.com/tagging/map/tag/5/CMJYB/Contact";
	    
	    //CM API tests - https://api.vollycrm.com/cm/swagger/index.html
	    //Here�s a swagger page for all the CRM API end points - Tedd
	    
	    private static final String GET_CMURL = "https://api.vollycrm.com/cm/messageque/gettoken";
	    
	    private static final String GET_CMURLB = "https://api.vollycrm.com/cm/integration/pos/updateLOData?corpCode=ABC&corpUserId=10001232&contactId=10006437";
	    
	    private static final String GET_CMURLC = "https://api.vollycrm.com/cm/integration/updateLOData?corpCode=ABC&corpUserId=10001232&contactId=10006437";
	
	    private static final String GET_CMURLD = "https://api.vollycrm.com/cm/listupload/batchInterfaceComment?corpCode=ABC&batchInterfaceId=10001001";
	    
	    private static final String GET_CMURLE = "https://api.vollycrm.com/cm/site/search?CorpUserId=10001232&SearchType=0&SearchText=test&CorpCode=ABC";
	    
	    private static final String GET_CMURLF = "https://api.vollycrm.com/cm/site/recent?CorpUserId=10001220&CorpCode=ABC";
	    
		// JavascriptExecutor js;

		// js = (JavascriptExecutor) driver;

//		public static void main(String[] args) {

		/*
		 * @BeforeTest public void setUp() throws Exception {
		 * System.setProperty("webdriver.chrome.driver",
		 * "C:\\Users\\acronin\\eclipse\\chromedriver.exe"); WebDriver driver = new
		 * ChromeDriver(); }
		 */
	@Test
	//Set Property

    public void test() throws Exception {
		
		String logHeader = "[QA run Test Log]";	
		String testCaseName = "CRM Prod API get";	
	    System.out.println("Starting Test-Case: " + testCaseName); 
		String logWithNewLine0 = logHeader + System.getProperty("line.separator");
		String logWithNewLine1 = testCaseName + System.getProperty("line.separator");		
		LocalDateTime datetime = LocalDateTime.now();
		File myLog = new File("c:\\QATestLog.txt");

		String nuLine = "\r\n";
		//logWriter.write(nuLine);
				
		if (myLog.exists()) {
	        System.out.println("Log Exists, so opening log for appending."); 
			//logWriter.close();
			FileWriter logWriter = new FileWriter(myLog, true);
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// append & write QATestlog.
					//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
					// myWriter.write("Files in Java might be tricky, but it is fun enough!");
					//LocalDateTime datetime = LocalDateTime.now();
					try {
						System.out.println(datetime);
						// logWriter.write(datetime.getHour());
						logWriter.write(logWithNewLine0);
						//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
						//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
						logWriter.write(logWithNewLine1);
						//logWriter.write(nuLine);
						// myWriter.write(dataWithNewLine1);
						// myWriter.write(dataWithNewLine2);
						// myWriter.write(dataWithNewLine3);
						logWriter.close();
						System.out.println("Successfully wrote lines to the log file.");
					} catch (IOException e) {
						System.out.println("An error occurred.");
						e.printStackTrace();
					}
		}
		else {
	        System.out.println("Log does not Exist, so opening a new log for writing."); 
			FileWriter logWriter = new FileWriter(myLog);
			// append & write QATestlog.
			//FileWriter logWriter = new FileWriter("c:\\QATestLog.txt", true);
			// myWriter.write("Files in Java might be tricky, but it is fun enough!");
			//LocalDateTime datetime = LocalDateTime.now();
			try {
				System.out.println(datetime);
				// logWriter.write(datetime.getHour());
				logWriter.write(logWithNewLine0);
				//logWriter.write(datetime.toString() + System.getProperty("line.separator"));
				//logWriter.write(logWithNewLine1 + System.getProperty("line.separator"));
				logWriter.write(logWithNewLine1);
				// myWriter.write(dataWithNewLine1);
				// myWriter.write(dataWithNewLine2);
				// myWriter.write(dataWithNewLine3);
				logWriter.close();
				System.out.println("Successfully wrote lines to the log file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}

		  System.out.println("<<Start-Of-Chrome-Test>> \n");
			//System.setProperty(key, value)
		    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\abeck\\eclipse\\chromedriver.exe");	
		    System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			
			//Adding-in Headless Chrome Options
		    boolean headless = true;
		    //ChromeOptions options = new ChromeOptions();
			//ChromeDriver driver = new ChromeDriver();
			
			WebDriver driver = null;
			if (headless) {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--headless");
				options.addArguments("window-size=1920,1080");
				driver = new ChromeDriver(options);
				driver.manage().window().maximize();
			}
			else {
				driver = new ChromeDriver();
				driver.manage().window().maximize();
			}
			
			//javaguides.net - GET
			System.out.println("Test CRM Tagging GET:\n\n");
			System.out.println("Tag list\n");
			System.out.println("Test GET: "+GET_TAGURL);
			URL obj = new URL(GET_TAGURL);
	        HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
	        httpURLConnection.setRequestMethod("GET");
	        httpURLConnection.setRequestProperty("User-Agent", USER_AGENT);
	        int responseCode = httpURLConnection.getResponseCode();
	        System.out.println("GET Response Code :: " + responseCode);
            
	        if (responseCode == HttpURLConnection.HTTP_OK) { // success
	            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
	            String inputLine;
	            StringBuffer response = new StringBuffer();

	            while ((inputLine = in .readLine()) != null) {
	                response.append(inputLine);
	            } in .close();

	            // print result
	            String APIresponse = response.toString();
	            System.out.println(APIresponse+ "\n");
	        }
	            
	          //javaguides.net - GET
	        System.out.println("Tag ID:\n");
				System.out.println("Test GET: "+GET_TAGURLB);
				URL objb = new URL(GET_TAGURLB);
		        HttpURLConnection httpURLConnectionb = (HttpURLConnection) objb.openConnection();
		        httpURLConnectionb.setRequestMethod("GET");
		        httpURLConnectionb.setRequestProperty("User-Agent", USER_AGENT);
		        int responseCodeb = httpURLConnectionb.getResponseCode();
		        System.out.println("GET Response Code :: " + responseCodeb);
	            
		        if (responseCodeb == HttpURLConnection.HTTP_OK) { // success
		            BufferedReader inb = new BufferedReader(new InputStreamReader(httpURLConnectionb.getInputStream()));
		            String inputLineb;
		            StringBuffer responseb = new StringBuffer();

		            while ((inputLineb = inb .readLine()) != null) {
		                responseb.append(inputLineb);
		            } inb .close();

		            // print result
		            String APIresponseb = responseb.toString();
		            System.out.println(APIresponseb+ "\n");
		        }
			          //javaguides.net - GET
		        System.out.println("Tag Group:\n");
					System.out.println("Test GET: "+GET_TAGURLC);
					URL objc = new URL(GET_TAGURLC);
			        HttpURLConnection httpURLConnectionc = (HttpURLConnection) objc.openConnection();
			        httpURLConnectionc.setRequestMethod("GET");
			        httpURLConnectionc.setRequestProperty("User-Agent", USER_AGENT);
			        int responseCodec = httpURLConnectionc.getResponseCode();
			        System.out.println("GET Response Code :: " + responseCodec);
		            
			        if (responseCodec == HttpURLConnection.HTTP_OK) { // success
			            BufferedReader inc = new BufferedReader(new InputStreamReader(httpURLConnectionc.getInputStream()));
			            String inputLinec;
			            StringBuffer responsec = new StringBuffer();

			            while ((inputLinec = inc .readLine()) != null) {
			                responsec.append(inputLinec);
			            } inc .close();

			            // print result
			            String APIresponsec = responsec.toString();
			            System.out.println(APIresponsec+ "\n");
			        }
			        //javaguides.net - GET
			        System.out.println("Tag Group AppKey:\n");
					System.out.println("Test GET: "+GET_TAGURLD);
					URL objd = new URL(GET_TAGURLD);
			        HttpURLConnection httpURLConnectiond = (HttpURLConnection) objd.openConnection();
			        httpURLConnectiond.setRequestMethod("GET");
			        httpURLConnectiond.setRequestProperty("User-Agent", USER_AGENT);
			        int responseCoded = httpURLConnectiond.getResponseCode();
			        System.out.println("GET Response Code :: " + responseCoded);
		            
			        if (responseCoded == HttpURLConnection.HTTP_OK) { // success
			            BufferedReader ind = new BufferedReader(new InputStreamReader(httpURLConnectiond.getInputStream()));
			            String inputLined;
			            StringBuffer responsed = new StringBuffer();

			            while ((inputLined = ind .readLine()) != null) {
			                responsed.append(inputLined);
			            } ind .close();

			            // print result
			            String APIresponsed = responsed.toString();
			            System.out.println(APIresponsed+ "\n");
			        }
			        
			      //javaguides.net - GET
			        System.out.println("Tags for an object:\n");
					System.out.println("Test for GET: "+GET_TAGURLE);
					URL obje = new URL(GET_TAGURLE);
			        HttpURLConnection httpURLConnectione = (HttpURLConnection) obje.openConnection();
			        httpURLConnectione.setRequestMethod("GET");
			        httpURLConnectione.setRequestProperty("User-Agent", USER_AGENT);
			        int responseCodee = httpURLConnectione.getResponseCode();
			        System.out.println("GET Response Code :: " + responseCodee);
		            
			        if (responseCodee == HttpURLConnection.HTTP_OK) { // success
			            BufferedReader ine = new BufferedReader(new InputStreamReader(httpURLConnectione.getInputStream()));
			            String inputLinee;
			            StringBuffer responsee = new StringBuffer();

			            while ((inputLinee = ine .readLine()) != null) {
			                responsee.append(inputLinee);
			            } ine .close();

			            // print result
			            String APIresponsee = responsee.toString();
			            System.out.println(APIresponsee+ "\n");
			        }
			        //javaguides.net - GET
			        
			        System.out.println("Tags by Object Type:\n");
					System.out.println("Test GET: "+GET_TAGURLF);
					URL objf = new URL(GET_TAGURLF);
			        HttpURLConnection httpURLConnectionf = (HttpURLConnection) objf.openConnection();
			        httpURLConnectionf.setRequestMethod("GET");
			        httpURLConnectionf.setRequestProperty("User-Agent", USER_AGENT);
			        int responseCodef = httpURLConnectionf.getResponseCode();
			        System.out.println("GET Response Code :: " + responseCodef);
		            
			        if (responseCodef == HttpURLConnection.HTTP_OK) { // success
			            BufferedReader inf = new BufferedReader(new InputStreamReader(httpURLConnectionf.getInputStream()));
			            String inputLinef;
			            StringBuffer responsef = new StringBuffer();

			            while ((inputLinef = inf .readLine()) != null) {
			                responsef.append(inputLinef);
			            } inf .close();

			            // print result
			            String APIresponsef = responsef.toString();
			            System.out.println(APIresponsef+ "\n\n");
			        }
			        
			      //CM API gets        
					System.out.println("Test CRM API GET:\n\n");
					
					System.out.println("Message Get Token:\n");
					System.out.println("Test GET: "+GET_CMURL);
					URL cmobj = new URL(GET_CMURL);
			        HttpURLConnection cmhttpURLConnection = (HttpURLConnection) cmobj.openConnection();
			        cmhttpURLConnection.setRequestMethod("GET");
			        cmhttpURLConnection.setRequestProperty("User-Agent", USER_AGENT);
			        int cmresponseCode = httpURLConnection.getResponseCode();
			        System.out.println("GET Response Code :: " + cmresponseCode);
		            
			        if (cmresponseCode == HttpURLConnection.HTTP_OK) { // success
			            BufferedReader cmin = new BufferedReader(new InputStreamReader(cmhttpURLConnection.getInputStream()));
			            String cminputLine;
			            StringBuffer cmresponse = new StringBuffer();

			            while ((cminputLine = cmin .readLine()) != null) {
			                cmresponse.append(cminputLine);
			            } cmin .close();

			            // print result
			            String cmAPIresponse = cmresponse.toString();
			            System.out.println(cmAPIresponse+ "\n");
			        }
			            
			          //javaguides.net - GET
			           
			        System.out.println("POS Update LOData:\n");
						System.out.println("Test GET: "+GET_CMURLB);
						URL cmobjb = new URL(GET_CMURLB);
				        HttpURLConnection cmhttpURLConnectionb = (HttpURLConnection) cmobjb.openConnection();
				        cmhttpURLConnectionb.setRequestMethod("GET");
				        cmhttpURLConnectionb.setRequestProperty("User-Agent", USER_AGENT);
				        int cmresponseCodeb = cmhttpURLConnectionb.getResponseCode();
				        System.out.println("GET Response Code :: " + cmresponseCodeb);
			            
				        if (cmresponseCodeb == HttpURLConnection.HTTP_OK) { // success
				            BufferedReader cminb = new BufferedReader(new InputStreamReader(cmhttpURLConnectionb.getInputStream()));
				            String cminputLineb;
				            StringBuffer cmresponseb = new StringBuffer();

				            while ((cminputLineb = cminb .readLine()) != null) {
				                cmresponseb.append(cminputLineb);
				            } cminb .close();

				            // print result
				            String cmAPIresponseb = cmresponseb.toString();
				            System.out.println(cmAPIresponseb+ "\n");
				        }
				        
					          //javaguides.net - GET
				        System.out.println("UpdateLOData:\n");
							System.out.println("Test GET: "+GET_CMURLC);
							URL cmobjc = new URL(GET_CMURLC);
					        HttpURLConnection cmhttpURLConnectionc = (HttpURLConnection) cmobjc.openConnection();
					        cmhttpURLConnectionc.setRequestMethod("GET");
					        cmhttpURLConnectionc.setRequestProperty("User-Agent", USER_AGENT);
					        int cmresponseCodec = cmhttpURLConnectionc.getResponseCode();
					        System.out.println("GET Response Code :: " + cmresponseCodec);
				            
					        if (cmresponseCodec == HttpURLConnection.HTTP_OK) { // success
					            BufferedReader cminc = new BufferedReader(new InputStreamReader(cmhttpURLConnectionc.getInputStream()));
					            String cminputLinec;
					            StringBuffer cmresponsec = new StringBuffer();

					            while ((cminputLinec = cminc .readLine()) != null) {
					                cmresponsec.append(cminputLinec);
					            } cminc .close();

					            // print result
					            String cmAPIresponsec = cmresponsec.toString();
					            System.out.println(cmAPIresponsec+ "\n");
					        }
					        
					        //javaguides.net - GET
					        System.out.println("Message Get Token:\n");
							System.out.println("Test GET: "+GET_CMURLD);
							URL cmobjd = new URL(GET_CMURLD);
					        HttpURLConnection cmhttpURLConnectiond = (HttpURLConnection) cmobjd.openConnection();
					        cmhttpURLConnectiond.setRequestMethod("GET");
					        cmhttpURLConnectiond.setRequestProperty("User-Agent", USER_AGENT);
					        int cmresponseCoded = cmhttpURLConnectiond.getResponseCode();
					        System.out.println("GET Response Code :: " + cmresponseCoded);
				            
					        if (cmresponseCoded == HttpURLConnection.HTTP_OK) { // success
					            BufferedReader cmind = new BufferedReader(new InputStreamReader(cmhttpURLConnectiond.getInputStream()));
					            String cminputLined;
					            StringBuffer cmresponsed = new StringBuffer();

					            while ((cminputLined = cmind .readLine()) != null) {
					                cmresponsed.append(cminputLined);
					            } cmind .close();

					            // print result
					            String cmAPIresponsed = cmresponsed.toString();
					            System.out.println(cmAPIresponsed+ "\n");
					        }
					        
					      //javaguides.net - GET
					        System.out.println("Site Search:\n");
							System.out.println("Test GET: "+GET_CMURLE);
							URL cmobje = new URL(GET_CMURLE);
					        HttpURLConnection cmhttpURLConnectione = (HttpURLConnection) cmobje.openConnection();
					        cmhttpURLConnectione.setRequestMethod("GET");
					        cmhttpURLConnectione.setRequestProperty("User-Agent", USER_AGENT);
					        int cmresponseCodee = cmhttpURLConnectione.getResponseCode();
					        System.out.println("GET Response Code :: " + cmresponseCodee);
				            
					        if (cmresponseCodee == HttpURLConnection.HTTP_OK) { // success
					            BufferedReader cmine = new BufferedReader(new InputStreamReader(cmhttpURLConnectione.getInputStream()));
					            String cminputLinee;
					            StringBuffer cmresponsee = new StringBuffer();

					            while ((cminputLinee = cmine .readLine()) != null) {
					                cmresponsee.append(cminputLinee);
					            } cmine .close();

					            // print result
					            String cmAPIresponsee = cmresponsee.toString();
					            System.out.println(cmAPIresponsee+ "\n");
					        }
					        
					        System.out.println("Site Recent:\n");
					        System.out.println("Test GET: "+GET_CMURLF);
							URL cmobjf = new URL(GET_CMURLF);
					        HttpURLConnection cmhttpURLConnectionf = (HttpURLConnection) cmobjf.openConnection();
					        cmhttpURLConnectionf.setRequestMethod("GET");
					        cmhttpURLConnectionf.setRequestProperty("User-Agent", USER_AGENT);
					        int cmresponseCodef = cmhttpURLConnectionf.getResponseCode();
					        System.out.println("GET Response Code :: " + cmresponseCodef);
				            
					        if (cmresponseCodef == HttpURLConnection.HTTP_OK) { // success
					            BufferedReader cminf = new BufferedReader(new InputStreamReader(cmhttpURLConnectionf.getInputStream()));
					            String cminputLinef;
					            StringBuffer cmresponsef = new StringBuffer();

					            while ((cminputLinef = cminf .readLine()) != null) {
					                cmresponsef.append(cminputLinef);
					            } cminf .close();

					            // print result
					            String cmAPIresponsef = cmresponsef.toString();
					            System.out.println(cmAPIresponsef+ "\n\n");
					        }
			        
			        
		    System.out.println("API GET Test PASS");
		    
		    if (myLog.exists()) {
		    	System.out.println("Log Exists, so opening log for appending.");
		    	//logWriter.close();
		    	FileWriter logWriter = new FileWriter(myLog, true);
		    String logWithNewLine2 = testCaseName + " Passes" + System.getProperty("line.separator");
		    logWriter.write(logWithNewLine2);
		    logWriter.close();
		    }
		    driver.close();
		    driver.quit();
	}
		    
		@AfterTest
		public void tearDownTest() {;
			// close browser
			//driver.close();
			//driver.quit();
			System.out.println("Test Completed Successfully!");
		}

		/*
		 * @AfterTest public void tearDown() throws Exception { Thread.sleep(5000);
		 * driver.quit(); }
		 */
	}
