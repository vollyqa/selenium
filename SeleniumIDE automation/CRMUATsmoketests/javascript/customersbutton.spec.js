// Generated by Selenium IDE
const { Builder, By, Key, until } = require('selenium-webdriver')
const assert = require('assert')

describe('Customersbutton', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('firefox').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Customersbutton', async function() {
    await driver.get("https://rc.cmuat.loyaltyexpress.com//site/login.asp?PassedUserCorp=ABC2")
    await driver.manage().window().setRect(1437, 905)
    await driver.findElement(By.id("idLogin")).click()
    await driver.findElement(By.id("idLogin")).sendKeys("cgreenwood")
    await driver.findElement(By.id("idPassword")).sendKeys("Testpw123")
    await driver.findElement(By.id("idUserCorp")).sendKeys("ABC2")
    await driver.findElement(By.xpath("//input[@value=\'Log In\']")).click()
    await driver.findElement(By.xpath("//button[@onclick=\'document.getElementById(\"masterIframe\").src=\"ContactSummary.asp?type=customer\"\']")).click()
    await driver.switchTo().frame(0)
    assert(await driver.findElement(By.css("#GroupType")).getText() == "Customers")
    await driver.findElement(By.css("#mycontroller .filteroptions td path")).click()
    await driver.sleep(1000)
    await driver.switchTo().defaultContent()
    await driver.findElement(By.id("txtFirstName")).click()
    vars["ndate"] = await driver.executeScript("return new Date().getDate();")
    await driver.findElement(By.css("#txtFirstName")).sendKeys("Test")
    await driver.findElement(By.css("#txtLastName")).sendKeys("Customervars["ndate"]")
    await driver.findElement(By.css(".text-right > .btn-success:nth-child(1)")).click()
    await driver.close()
  })
})
